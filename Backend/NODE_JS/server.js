var express     =   require("express");
var router      =   express.Router();
var moment     	=   require("moment");
var app         =   module.exports = express();
var geolib     	=   require("geolib");
var bodyParser  =   require("body-parser");
var mongoClient = require('mongodb').MongoClient;
var ObjectId 	= require('mongodb').ObjectID;

//OPENSHIFT_NODEJS_IP=127.8.78.1 / 8080
var ipaddressEnv = process.env.OPENSHIFT_NODEJS_IP || "127.0.0.1";
var portEnv      = process.env.OPENSHIFT_NODEJS_PORT || 8080;
moment.locale('pt-br')

if (typeof ipaddressEnv === "undefined") {
	//  Log errors on OpenShift but continue w/ 127.0.0.1 - this
	//  allows us to run/test the app locally.
	console.warn('No OPENSHIFT_NODEJS_IP var, using 127.0.0.1');
	ipaddressEnv = "127.0.0.1";
};

app.use(bodyParser.urlencoded({"extended" : false}));
app.use(bodyParser.json());

app.use(function(req, res, next) {
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Headers", "X-Requested-With");
        res.header("Access-Control-Allow-Headers", "Content-Type");
        res.header("Access-Control-Allow-Methods", "PUT, GET, POST, DELETE, OPTIONS");
        next();
});

//MONGODB LOCALHOST
//var mongoUrl = 'mongodb://localhost:27017/pociidata'
//var collectionName = 'informes'

//MONGODB OPENSHIFT
//Database: pociidata / User: admin / Password: ClVqRdQ-PXFx
//URL must be in the format mongodb://user:pass@host:port/dbnam
//var mongoUrl = 'admin:ClVqRdQ-PXFx@127.8.78.2:27017/pociidata';
//var mongoUrl = 'admin:ClVqRdQ-PXFx@127.8.78.2:27017/pociidata';

var mongoUrl = 'mongodb://admin:ClVqRdQ-PXFx@127.8.78.2:27017/pociidata';
var collectionName = 'informes';
var db_name = 'pociidata';

//take advantage of openshift env vars when available:
if(process.env.OPENSHIFT_MONGODB_DB_URL){
  mongoUrl = process.env.OPENSHIFT_MONGODB_DB_URL + db_name;
}

app.get("/",function(req,res){
    //res.json({"error" : false,"message" : "Hello World"});
    res.send("Hello World");
});

app.post("/insert",function(request,res){
    var db = mongoClient.connect(mongoUrl, function(err, db) {
		if(err) { return console.dir(err); }
		var mycollection = db.collection(collectionName);
		request.body.created_at = moment(new Date(request.body.created_at), 'dd MMM DD HH:mm:ss ZZ YYYY').format('L')
		mycollection.insert(request.body, function(err2, result) {
          if (err2 != null){ console.log("Erro: " + err2);}
          //else {console.dir(result);}
      });
	});
	res.json({"error" : false,"message" : "Dados inseridos"});
	
});

app.get("/informes",function(req,res){
	var db = mongoClient.connect(mongoUrl, function(err, db) {
		if(err) { return console.dir(err); }
		var mycollection = db.collection(collectionName);
		mycollection.find({}).limit(2).toArray(function (err, docs) { 
			if (err != null){ console.log("Erro: " + err);}
			res.json({"error" : false,"data" : docs});		
		});		
	});
});

app.get("/closest",function(req,res){
		var db = mongoClient.connect(mongoUrl, function(err, db) {
		if(err) { return console.dir(err); }
		var mycollection = db.collection(collectionName);
		
		moment.locale('pt-br');
		var today = moment().format('L');
		//var limite = moment().add(3, 'days').format('L');		
		//Testes
		//var today = '01/09/2016';
		//var limite = '25/09/2016';
		
		//mycollection.find( {"created_at": {"$gte": today, "$lt": limite}}).sort({created_at: -1}).toArray(function (err, docs) { 
		mycollection.find( {"created_at": today}).sort({"timestamp_ms": -1}).toArray(function (err, docs) { 
			// docs is an array of all the documents in mycollection
			//console.log(docs.length);
			if (err != null){ console.log("Erro: " + err);}
			res.json({"error" : false,"data" : docs});		
		});		
	});
});


app.get('/informes/:id', function(req, res) {
	if (req.params.id == -1 || req.params.id  == '-1'){
		res.json({"error" : false,"data" : []});	
	} else {
		//console.log(String(req.params.id));		
		var db = mongoClient.connect(mongoUrl, function(err, db) {
			if(err) { return console.dir(err); }
			var mycollection = db.collection(collectionName);
			mycollection.find().min({"_id":ObjectId(req.params.id)}).limit(2).skip(1).toArray(function (err, docs) { 
				//console.log(docs);
				if (err != null){ console.log("Erro: " + err);}
				res.json({"error" : false,"data" : docs});		
			});	
		});	
	}
});

app.get('/find/:id', function(req, res) {
	if (req.params.id == -1 || req.params.id  == '-1'){
		res.json({"error" : false,"data" : []});	
	} else {
		//console.log(String(req.params.id));		
		var db = mongoClient.connect(mongoUrl, function(err, db) {
			if(err) { return console.dir(err); }
			var mycollection = db.collection(collectionName);
			mycollection.find({"id_str":String(req.params.id)}).limit(2).toArray(function (err, docs) { 
				//console.log(docs);
				if (err != null){ console.log("Erro: " + err);}
				res.json({"error" : false,"data" : docs});		
			});	
		});	
	}
});

/*
app.use('/',router);
app.use('/informes',router);
app.use('/informes/:id',router);
app.use('/find/:id',router);
app.use('/insert',router);
app.use('/closest',router);
*/
app.listen(portEnv, ipaddressEnv);
console.log("Listening to PORT " + portEnv + " on " + ipaddressEnv);