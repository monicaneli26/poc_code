#!/usr/bin/python
# -*- coding: utf-8 -*-
import requests
import simplejson as json

url = "http://localhost:8080/insert"
data = {'nome': 'Alice'}
headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
r = requests.post(url, data=json.dumps(data), headers=headers)
print(r.status_code)

#{'informe': 'RT @g1: Campanha é dominada pela extrema-direita a 6 meses da eleição na França https://t.co/e6d0NaeXpn', 'created_at': 'Thu Oct 27 12:25:06 +0000 2016', 'coordinates': 'OOOOBBOOOOOOOOOBOOO', 'iob_label': 'BBOOOOOOOOOB', 'timestamp_ms': '1477571106539', 'id_str': '791616712898703360', 'topic': {'lng': -43.9314024, 'lat': -19.9431404}}

