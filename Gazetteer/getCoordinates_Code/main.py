#!/usr/bin/python
# -*- coding: utf-8 -*-
import Levenshtein
import unicodedata
import nltk.corpus
import string
import re
import googlemaps
from datetime import datetime
import csv


def geocoding( address, gmaps):
	# Geocoding and address
	geocode_result = gmaps.geocode(address + ", Belo Horizonte, Minas Gerais, Brazil")
	
	if (len(geocode_result) > 0):
		location = geocode_result[0]['geometry']['location']
		latitude, longitude = location['lat'],location['lng']
	else:
		latitude, longitude = -1,-1
	# Look up an address with reverse geocoding
	#reverse_geocode_result = self.gmaps.reverse_geocode((40.714224, -73.961452))

	return (latitude, longitude)

#------------------------------------------------------
# Function readDictionary e obtem coordenadas
# @params: fileName
# @author: Monica
#------------------------------------------------------
def getDictionaryCoordinates( fileName):
	
	gmaps = googlemaps.Client(key='AIzaSyDXIEeHCDshtG3NX7Ky88xqE6oawiuLCLE')
	
	f = open(fileName, 'r')
	f1 = csv.writer(open("EntidadesAnnotadasGazetteer.csv", 'w', newline=''))
	
	# Write CSV Headers
	#f1.writerow(["Address", "lat", "lgn"])

	for line in f:
		address = line
		address = address.replace('\n', "").upper()
		print(address)
		
		#Get the address coordinates
		coordinates = geocoding(address, gmaps)
		f1.writerow([address, coordinates[0], coordinates[1]])

	f.close()
	
	
getDictionaryCoordinates("Entidades2.csv")