
$(document).ready(function() {

	//$('#showMenu').trigger("click");	
	//$(".cadastro").css("display", "none");
	//$(".cadastro").('.active').css("display", "block");
	
	/*$(".showPopupMap").bind('click', function (event) {
		BootstrapDialog.show({
            message: 'Hi Apple!'
        });        
	});*/
	
/*	$('.icon-size').bind("click", function() {
		$('.icon-size').removeClass('active-button');
		$(this).addClass('active-button');
		console.log("hei1");
	});*/
	//console.log(mapFunctions.mapCenterCoords);
	
	function getIOBLabel(expression){
		var iobLabel = [];
		var words = expression.split(" ");
		
		$.each(words, function( index, value ) {
			if (value != ""){
				if (iobLabel.length == 0) iobLabel.push("B");
				else iobLabel.push("I");
			}
		});
		
		return iobLabel;
	}
	
	$('#addGeoRef').bind("click", function() {
		
		if ($(".geoLabel").length > 0){
			var oldLabel = $('#labelIOB').val();
			size = oldLabel.length;		
			var idList = [];
			
			var newLabel = [];
			for (var i=0; i < size; i++) {   
				letter = oldLabel.charAt(i);                
				newLabel.push(letter);
				//console.log(letter);
			}
			//console.log(newLabel);
			//console.log(newLabel.length);
			var geoName = "";
			var names = [];
			var expressionIds = [];
			var labelsVector = [];
			var label_ids = [];
			var idItem = -1;
			var iobLabel = [];
			$(".geoLabel").each(function() {
					idList.push(this.id);
					if (idItem == -1){
						idItem = parseInt(this.id);									
					} else if (idItem+1 != parseInt(this.id)){						
						names.push(geoName);						
						iobLabel = getIOBLabel(geoName);
						labelsVector.push([label_ids, iobLabel]);
						label_ids = [];
						geoName = "";	
						idItem = -1;			
					}
					geoName = geoName + " " + this.value;
					idItem = parseInt(this.id);	
					label_ids.push(parseInt(this.id));
					
					//console.log(geoName);
					
					$(this).removeClass('geoLabel');
					$(this).addClass('geoLabelSet');
			});		
			
			names.push(geoName);			
			iobLabel = getIOBLabel(geoName);
			labelsVector.push([label_ids, iobLabel]);
			console.log(labelsVector);
			console.log(names);
			
			$.each(labelsVector, function( index, value ) {				
				$.each(value[0], function(index2, value2) {
					newLabel[value2] = 	value[1][index2];
				});
				//console.log(newLabel);	
			});	
			
			var str = ""
			$.each(newLabel, function(index, value) {                    
				str = str + "" + value;
			});
			$('#labelIOB').val(str);
			console.log($('#labelIOB').val());	
			//console.log(geoName);	
			//var geoClass = geoName.split(" ")[0];
			
			$.each(names, function( index, value ) {
				//Adicionar referencia Geográfica à lista				
				var $input = $('<div class="btn btn-default list-group-item '+value+'" value = "'+value+'">'+value+'<span class="glyphicon glyphicon-remove left-space remove-geo" aria-hidden="true"></span></div>');
				$input.appendTo($(".list-group"));					
			});
			
			$('.noneGeo').addClass('hidden');
			$('.remove-geo').bind("click", function() {
				var item = $(this).closest("div").attr("value");
				var elements = item.split(" ");
				//console.log(elements);
				
				var oldLabel = $('#labelIOB').val();
				size = oldLabel.length;		
				var idList = [];
				
				var newLabel = [];
				for (var i=0; i < size; i++) {   
					letter = oldLabel.charAt(i);                
					newLabel.push(letter);
					//console.log(letter);
				}						
				
				$.each(elements, function(index, value) { 
					//console.log(value);
					if (value != ""){
						$('.'+value).remove();
											
						$(".geoLabelSet").each(function() {
							if (this.value == value){
								//console.log(this.id);
								newLabel[this.id] = 'O';
								$(this).removeClass('geoLabelSet');
								$(this).addClass('nonGeoLabel');
							}
						});
					}
				});
				
				var str = ""
				$.each(newLabel, function(index, value) {                    
					str = str + "" + value;
				});
				$('#labelIOB').val(str);
				console.log($('#labelIOB').val());	
				
				if ($('.remove-geo').length <= 0){
					$('.noneGeo').removeClass('hidden');
				}
				//e.stopPropagation();
			});
		}
	});
	
	$('.wordAnnotation').bind("click", function() {
		targetId = this.id;
		
		if ($(this).hasClass('geoLabelSet') == false){
			//Geo
			if ($(this).hasClass('nonGeoLabel')){
				$(this).removeClass('nonGeoLabel');
				$(this).addClass('geoLabel');	
			
				
			//Non Geo
			} else {
				$(this).removeClass('geoLabel');
				$(this).addClass('nonGeoLabel');
			}			
		}
		$(this).blur(); 
	});
	
	function confirmfrmSubmit(){
		var agree=confirm("Are you sure you wish to continue?");

		if (agree) return true ;
		else return false ;
	} 
	
	function isEmpty( inputStr ) { if ( null == inputStr || "" == inputStr ) { return true; } return false; }

	function limparFiltros(targeturl, currentNav, divAtual){
		var regiaoSelected = $('#filterSelectRegion').val();
		var statusSelected = $('#filterStatus').val();
		var from = $('#datetimepicker1').val();
		var to = $('#datetimepicker2').val();
		var selectedValue = null;
		$('#limparFiltro').addClass("hidden");
		$('#loading-spinner').removeClass("hidden");		
		
		if (isEmpty(regiaoSelected) && isEmpty(statusSelected) && isEmpty(from) && isEmpty(to)) {
			$('#loading-spinner').addClass("hidden");
			return;
		}
		
		$.ajax({
			type: 'post',
			url: targeturl,
			data: {regiao: selectedValue, tipo: currentNav},
			beforeSend: function(xhr) {
				xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
			},
			success: function(response) {
				//console.log("Sucesso");
				//console.log(response);
				if (response) {
					$(divAtual).html(response.content);	
					$('#filterOptions').html("");
					$('#loading-spinner').addClass("hidden");				
				}
				rebingEvents();		
			},
			error: function(e) {
				//console.log("falha");
				rebingEvents();
				$('#loading-spinner').addClass("hidden");
				alert("An error occurred: " + e.responseText.message);
				console.log(e);
			}
		});		
	}

	$('#filtrar').bind("click", function() {
		var regiaoSelected = $('#filterSelectRegion').val();
		var statusSelected = $('#filterStatus').val();
		var from = $('#datetimepicker1').val();
		var to = $('#datetimepicker2').val();
		var targeturl = $('#filter-form').attr('rel');
		$('#limparFiltro').addClass("hidden");
		$('#loading-spinner').removeClass("hidden");
		//$('#filterButton').addClass("hidden");
		
		if (isEmpty(regiaoSelected) && isEmpty(statusSelected) && isEmpty(from) && isEmpty(to)) {
			$('#loading-spinner').addClass("hidden");
			return;
		}
		
		$.ajax({
			type: 'post',
			url: targeturl,
			data: {regiao: regiaoSelected, status: statusSelected, from: from, to: to, tipo: "Restrição"},
			beforeSend: function(xhr) {
				xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
			},
			success: function(response) {
				//console.log("Sucesso");
				//console.log(response);
				if (response) {
					$('#user-restricoes-list').html(response.content);	
					$('#loading-spinner').addClass("hidden");
					$('#filterOptions').html("<button id='limparFiltro' type='button' class='btn btn-default controles-filter'><span class= 'glyphicon glyphicon-remove' aria-hidden='true'></span> Limpar Filtro</button>");
				}
				rebingEvents();
				$('#limparFiltro').bind("click", function() {					
					limparFiltros(targeturl, "Restrição", '#user-restricoes-list');					
					$('#filterSelectRegion').val('');
					$('#filterStatus').val('');
					$('#datetimepicker1').val('');
					$('#datetimepicker2').val('');
				});				
			},
			error: function(e) {
				//console.log("falha");
				rebingEvents();
				$('#loading-spinner').addClass("hidden");
				alert("An error occurred: " + e.responseText.message);
				console.log(e);
			}
		});
	});


	function limparFiltrosPontos(targeturl, currentNav, divAtual){
		var regiaoSelected = $('#filterSelectRegionPontos').val();
		var statusSelected = $('#filterStatusPontos').val();
		var from = $('#datetimepicker3').val();
		var to = $('#datetimepicker4').val();
		var selectedValue = null;
		$('#limparFiltroPontos').addClass("hidden");
		$('#loading-spinner-pontos').removeClass("hidden");		
		
		if (isEmpty(regiaoSelected) && isEmpty(statusSelected) && isEmpty(from) && isEmpty(to)) {
			$('#loading-spinner-pontos').addClass("hidden");
			return;
		}
		
		$.ajax({
			type: 'post',
			url: targeturl,
			data: {regiao: selectedValue, tipo: currentNav},
			beforeSend: function(xhr) {
				xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
			},
			success: function(response) {
				//console.log(response);
				if (response) {
					$(divAtual).html(response.content);	
					$('#filterOptionsPontos').html("");
					$('#loading-spinner-pontos').addClass("hidden");				
				}		
				rebingEvents();
			},
			error: function(e) {
				rebingEvents();
				$('#loading-spinner-pontos').addClass("hidden");
				alert("An error occurred: " + e.responseText.message);
				console.log(e);
			}
		});		
	}

	$('#filtrarPontos').bind("click", function() {
		var regiaoSelected = $('#filterSelectRegionPontos').val();
		var statusSelected = $('#filterStatusPontos').val();
		var from = $('#datetimepicker3').val();
		var to = $('#datetimepicker4').val();
		var targeturl = $('#filter-form-pontos').attr('rel');
		$('#limparFiltroPontos').addClass("hidden");
		$('#loading-spinner-pontos').removeClass("hidden");
		
		if (isEmpty(regiaoSelected) && isEmpty(statusSelected) && isEmpty(from) && isEmpty(to)) {
			$('#loading-spinner-pontos').addClass("hidden");
			return;
		}
		
		$.ajax({
			type: 'post',
			url: targeturl,
			data: {regiao: regiaoSelected, status: statusSelected, from: from, to: to, tipo: "Pontos"},
			beforeSend: function(xhr) {
				xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
			},
			success: function(response) {
				//console.log(response);
				if (response) {
					$('#user-pontos-list').html(response.content);	
					$('#loading-spinner-pontos').addClass("hidden");
					$('#filterOptionsPontos').html("<button id='limparFiltroPontos' type='button' class='btn btn-default controles-filter'><span class= 'glyphicon glyphicon-remove' aria-hidden='true'></span> Limpar Filtro</button>");
				}
				rebingEvents();
				$('#limparFiltroPontos').bind("click", function() {					
					limparFiltrosPontos(targeturl, "Pontos", '#user-pontos-list');					
					$('#filterSelectRegionPontos').val('');
					$('#filterStatusPontos').val('');
					$('#datetimepicker3').val('');
					$('#datetimepicker4').val('');
				});				
			},
			error: function(e) {
				rebingEvents();
				$('#loading-spinner-pontos').addClass("hidden");
				alert("An error occurred: " + e.responseText.message);
				console.log(e);
			}
		});
	});

	function limparFiltrosInformes(targeturl, currentNav, divAtual){
		var regiaoSelected = $('#filterSelectRegionInformes').val();
		var statusSelected = $('#filterStatusInformes').val();
		var from = $('#datetimepicker5').val();
		var to = $('#datetimepicker6').val();
		var selectedValue = null;
		$('#limparFiltroInformes').addClass("hidden");
		$('#loading-spinner-informes').removeClass("hidden");		
		
		if (isEmpty(regiaoSelected) && isEmpty(statusSelected) && isEmpty(from) && isEmpty(to)) {
			$('#loading-spinner-informes').addClass("hidden");
			return;
		}
		
		$.ajax({
			type: 'post',
			url: targeturl,
			data: {regiao: selectedValue, tipo: currentNav},
			beforeSend: function(xhr) {
				xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
			},
			success: function(response) {
				//console.log(response);
				if (response) {
					$(divAtual).html(response.content);	
					$('#filterOptionsInformes').html("");
					$('#loading-spinner-informes').addClass("hidden");				
				}		
				rebingEvents();
			},
			error: function(e) {
				rebingEvents();
				$('#loading-spinner-informes').addClass("hidden");
				alert("An error occurred: " + e.responseText.message);
				console.log(e);
			}
		});		
	}

	$('#filtrarInformes').bind("click", function() {
		var regiaoSelected = $('#filterSelectRegionInformes').val();
		var statusSelected = $('#filterStatusInformes').val();
		var from = $('#datetimepicker5').val();
		var to = $('#datetimepicker6').val();
		var targeturl = $('#filter-form-informes').attr('rel');
		$('#limparFiltroInformes').addClass("hidden");
		$('#loading-spinner-informes').removeClass("hidden");
		
		if (isEmpty(regiaoSelected) && isEmpty(statusSelected) && isEmpty(from) && isEmpty(to)) {
			$('#loading-spinner-informes').addClass("hidden");
			return;
		}
		
		$.ajax({
			type: 'post',
			url: targeturl,
			data: {regiao: regiaoSelected, status: statusSelected, from: from, to: to, tipo: "Informes"},
			beforeSend: function(xhr) {
				xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
			},
			success: function(response) {
				//console.log(response);
				if (response) {
					$('#user-informes-list').html(response.content);	
					$('#loading-spinner-informes').addClass("hidden");
					$('#filterOptionsInformes').html("<button id='limparFiltroInformes' type='button' class='btn btn-default controles-filter'><span class= 'glyphicon glyphicon-remove' aria-hidden='true'></span> Limpar Filtro</button>");
				}
				rebingEvents();
				$('#limparFiltroInformes').bind("click", function() {					
					limparFiltrosInformes(targeturl, "Informes", '#user-informes-list');					
					$('#filterSelectRegionInformes').val('');
					$('#filterStatusInformes').val('');
					$('#datetimepicker5').val('');
					$('#datetimepicker6').val('');
				});				
			},
			error: function(e) {
				rebingEvents();
				$('#loading-spinner-informes').addClass("hidden");
				alert("An error occurred: " + e.responseText.message);
				console.log(e);
			}
		});
	});
	
	
	$('#datetimepicker1').datetimepicker({
                    locale: 'pt'
	});
	$('#datetimepicker2').datetimepicker({
                    locale: 'pt'
	});
 	$('#datetimepicker3').datetimepicker({
                    locale: 'pt'
	});
	$('#datetimepicker4').datetimepicker({
                    locale: 'pt'
	});
 	$('#datetimepicker5').datetimepicker({
                    locale: 'pt'
	});
	$('#datetimepicker6').datetimepicker({
                    locale: 'pt'
	});
	$('#dataEHora').datetimepicker({
                    locale: 'pt',
                    inline: true,
					sideBySide: true,
					showClose: true
	});
	if ($('#dataEHora').data("DateTimePicker") != undefined){
		$('#dataEHora').data("DateTimePicker").hide();
		$('#dataEHora').data("DateTimePicker").date(null);
	}
	
	$(".seeOnTheMap").bind('click', function(event) {
		var coordinates = $(this).attr('rel');
		coordinates = coordinates.split('_');
		//{lat: -34.397, lng: 150.644},
		var coordenadaObj = new google.maps.LatLng(parseFloat(coordinates[0]), parseFloat(coordinates[1]));
		//console.log(coordenadaObj);
		var myMapOptions = {
			scrollwheel:true,
			zoom: 14,
			//center: {lat: parseFloat(coordinates[0])+0.01, lng: parseFloat(coordinates[1])-0.015},
			center: new google.maps.LatLng(parseFloat(coordinates[0])+0.019, parseFloat(coordinates[1])-0.025),
			mapTypeControl: true,
			mapTypeControlOptions: {
			style: google.maps.MapTypeControlStyle.DROPDOWN_MENU,
			//position: google.maps.ControlPosition.RIGHT_TOP
			},
			navigationControl: true,
			navigationControlOptions: {
			style: google.maps.NavigationControlStyle.SMALL,
			//position: google.maps.ControlPosition.LEFT_CENTER
			},
			mapTypeId: google.maps.MapTypeId.ROADMAP
		};
		
		var mapa_instancia = new google.maps.Map(document.getElementById("map_preview"), myMapOptions);
		var marker = new google.maps.Marker({
							position: coordenadaObj,
							title: 'Novo ponto de carga e descarga'
							//map: mapa_instancia
		});
		marker.setMap(mapa_instancia);
		
		google.maps.event.addListenerOnce(mapa_instancia, 'idle', function() {
			google.maps.event.trigger(mapa_instancia, 'resize');
		});	
		
		google.maps.event.trigger(mapa_instancia, 'resize');
		mapa_instancia.setZoom( mapa_instancia.getZoom() );
	});
	
	$(".approving-item").bind('click', function(event) {
		var linkId = this.id;
		var stringArray = linkId.split('-');
		var idItem = stringArray[1];
		var urlAction = $(this).attr('rel');
		//$('#divItem-'+idItem).addClass('jquery-waiting-base-container');
		
		$.ajax({
			type: 'post',
			url: urlAction,
			data: {id: idItem},
			beforeSend: function(xhr) {
				xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
			},
			success: function(response) {
				//console.log(response);
				if (response) {					
					$("#"+linkId).addClass("hidden");
					$("#reprove-"+idItem).removeClass("hidden");
					$("#divItem-"+idItem+" > .news-item").removeClass("red-background");
					$("#divItem-"+idItem+" > .news-item").addClass("green-background");
					//console.log($(linkId));
					//console.log($("#reprove-"+idItem));
				}
				//$('#divItem-'+idItem).removeClass('jquery-waiting-base-container');
			},
			error: function(e) {
				//alert("An error occurred: " + e.responseText.message);
				console.log(e);
				//$('#divItem-'+idItem).removeClass('jquery-waiting-base-container');
			}
		});			
	});
	
	$(".reproving-item").bind('click', function(event) {
		var linkId = this.id;
		var stringArray = linkId.split('-');
		var idItem = stringArray[1];
		var urlAction = $(this).attr('rel');
		//$('#divItem-'+idItem).addClass('jquery-waiting-base-container');
		
		$.ajax({
			type: 'post',
			url: urlAction,
			data: {id: idItem},
			beforeSend: function(xhr) {
				xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
			},
			success: function(response) {
				//console.log(response);
				if (response) {					
					$("#"+linkId).addClass("hidden");
					$("#approve-"+idItem).removeClass("hidden");
					$("#divItem-"+idItem+" > .news-item").removeClass("green-background");
					$("#divItem-"+idItem+" > .news-item").addClass("red-background");
					//console.log($(linkId));
					//console.log($("#reprove-"+idItem));
				}
				//$('#divItem-'+idItem).removeClass('jquery-waiting-base-container');
			},
			error: function(e) {
				//alert("An error occurred: " + e.responseText.message);
				console.log(e);
				//$('#divItem-'+idItem).removeClass('jquery-waiting-base-container');
			}
		});	
	});
	
	function rebingEvents(){
		$(".seeOnTheMap").bind('click', function(event) {
			var coordinates = $(this).attr('rel');
			coordinates = coordinates.split('_');
			//{lat: -34.397, lng: 150.644},
			var coordenadaObj = new google.maps.LatLng(parseFloat(coordinates[0]), parseFloat(coordinates[1]));
			//console.log(coordenadaObj);
			var myMapOptions = {
				scrollwheel:true,
				zoom: 14,
				//center: {lat: parseFloat(coordinates[0])+0.01, lng: parseFloat(coordinates[1])-0.015},
				center: new google.maps.LatLng(parseFloat(coordinates[0])+0.019, parseFloat(coordinates[1])-0.025),
				mapTypeControl: true,
				mapTypeControlOptions: {
				style: google.maps.MapTypeControlStyle.DROPDOWN_MENU,
				//position: google.maps.ControlPosition.RIGHT_TOP
				},
				navigationControl: true,
				navigationControlOptions: {
				style: google.maps.NavigationControlStyle.SMALL,
				//position: google.maps.ControlPosition.LEFT_CENTER
				},
				mapTypeId: google.maps.MapTypeId.ROADMAP
			};
			
			var mapa_instancia = new google.maps.Map(document.getElementById("map_preview"), myMapOptions);
			var marker = new google.maps.Marker({
								position: coordenadaObj,
								title: 'Novo ponto de carga e descarga'
								//map: mapa_instancia
			});
			marker.setMap(mapa_instancia);
			
			google.maps.event.addListenerOnce(mapa_instancia, 'idle', function() {
				google.maps.event.trigger(mapa_instancia, 'resize');
			});	
			
			google.maps.event.trigger(mapa_instancia, 'resize');
			mapa_instancia.setZoom( mapa_instancia.getZoom() );
		});
		
		$(".approving-item").bind('click', function(event) {
			var linkId = this.id;
			var stringArray = linkId.split('-');
			var idItem = stringArray[1];
			var urlAction = $(this).attr('rel');
			//$('#divItem-'+idItem).addClass('jquery-waiting-base-container');
			
			$.ajax({
				type: 'post',
				url: urlAction,
				data: {id: idItem},
				beforeSend: function(xhr) {
					xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
				},
				success: function(response) {
					//console.log(response);
					if (response) {					
						$("#"+linkId).addClass("hidden");
						$("#reprove-"+idItem).removeClass("hidden");
						$("#divItem-"+idItem+" > .news-item").removeClass("red-background");
						$("#divItem-"+idItem+" > .news-item").addClass("green-background");
						//console.log($(linkId));
						//console.log($("#reprove-"+idItem));
					}
					//$('#divItem-'+idItem).removeClass('jquery-waiting-base-container');
				},
				error: function(e) {
					//alert("An error occurred: " + e.responseText.message);
					console.log(e);
					//$('#divItem-'+idItem).removeClass('jquery-waiting-base-container');
				}
			});			
		});
		
		$(".reproving-item").bind('click', function(event) {
			var linkId = this.id;
			var stringArray = linkId.split('-');
			var idItem = stringArray[1];
			var urlAction = $(this).attr('rel');
			//$('#divItem-'+idItem).addClass('jquery-waiting-base-container');
			
			$.ajax({
				type: 'post',
				url: urlAction,
				data: {id: idItem},
				beforeSend: function(xhr) {
					xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
				},
				success: function(response) {
					//console.log(response);
					if (response) {					
						$("#"+linkId).addClass("hidden");
						$("#approve-"+idItem).removeClass("hidden");
						$("#divItem-"+idItem+" > .news-item").removeClass("green-background");
						$("#divItem-"+idItem+" > .news-item").addClass("red-background");
						//console.log($(linkId));
						//console.log($("#reprove-"+idItem));
					}
					//$('#divItem-'+idItem).removeClass('jquery-waiting-base-container');
				},
				error: function(e) {
					//alert("An error occurred: " + e.responseText.message);
					console.log(e);
					//$('#divItem-'+idItem).removeClass('jquery-waiting-base-container');
				}
			});	
		});
				
	}
	
	
	$(".div-visibilidade").addClass("hidden");
	
	$("#carga-list").bind('click', function(event) {
		$(".div-visibilidade").addClass("hidden");
		$('#carga-list').addClass("active");
		$('#cargaEdescarga-listagem').removeClass("hidden");	
	});
	
	$("#restricao-list").bind('click', function(event) {
		$(".div-visibilidade").addClass("hidden");
		$('#restricao-list').addClass("active");
		$('#restricoesVeiculares-listagem').removeClass("hidden");	
	});	
	
	$("#informes-list").bind('click', function(event) {
		$(".div-visibilidade").addClass("hidden");
		$('#informes-list').addClass("active");
		$('#informes-listagem').removeClass("hidden");	
	});
	
	$('#carga-add').bind('click', function(event) {
		$(".cadastro").removeClass("active");
		$('#carga-add').addClass("active");	
		$('.cadastro-div').addClass("hidden");
		$('#cargaDescarga-form').removeClass("hidden");
	});
	
	$('#restricao-add').bind('click', function(event) {
		$(".cadastro").removeClass("active");
		$('#restricao-add').addClass("active");	
		$('.cadastro-div').addClass("hidden")
		$('#restricoes-form').removeClass("hidden");
	});

	$('#informes-add').bind('click', function(event) {
		$(".cadastro").removeClass("active");
		$('#informes-add').addClass("active");	
		$('.cadastro-div').addClass("hidden")
		$('#informes-form').removeClass("hidden");
	});	
	
	// Prevent closing the sign in form on click
	$("#dropdown-login").bind('click', function(event) {
	   event.stopPropagation();
	});
	
	function unsetTabs(){
		$(".visibility").hide();	
		$(".navtab").removeClass("active");
	}


	function saveRestricoes(dados){
		var urlAction = $('#saveRestricoes').attr('rel');

        $.ajax({
            type: 'post',
            url: urlAction,
            data: {vetor: dados},
            beforeSend: function(xhr) {
                xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
            },
            success: function(response) {
				//console.log(response);
                if (response) {					
					//$(divId).html(response.content);	
                }
            },
            error: function(e) {
                //alert("An error occurred: " + e.responseText.message);
                console.log(e);
            }
        });
	}
	
	function addLayer(url, divId, destino, urlAction){
      //  var destino = $('#searchBox').val();
       //var urlAction = $('#restricoes').attr('rel');
		//alert(url);
		//console.log(divId);
		//divId = divId.replace('#', '');
		//console.log(divId);
        $.ajax({
            type: 'post',
            url: urlAction,
            data: {url: url, destino: destino},
            beforeSend: function(xhr) {
                xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
            },
            success: function(response) {
				//console.log(response);
                if (response) {		
					//console.log($(divId).html());		
					//document.getElementById(divId).innerHTML = response.content;	
					$(divId).html(response.content);	
                }
            },
            error: function(e) {
                alert("An error occurred: " + e.responseText.message);
                console.log(e);
            }
        });
	}

	function moveToLocation(lat, lng){
		var center = new google.maps.LatLng(lat, lng);
		// using global variable:
		map.panTo(center)
	}

    function getInformes_old() {
		var urlInformes = $('#getinformes').attr('rel');
		var destino = "";
		//console.log(urlInformes);
		if($("button.search.active").val() != undefined){
			destino = $("button.search.active").val();
		}
		
        $.ajax({
            type: 'post',
            url: urlInformes,
            data: {destino: destino},
            beforeSend: function(xhr) {
                xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
            },
            success: function(response) {
				console.log(response);
                if (response) {									
					//$('#informes-area').replaceWith(response.content);	
					document.getElementById('informes-area').innerHTML = response.content;
					$('#loading').css("display", "none");
					
					$("#selectRegion").bind("change", function() {
							$(".moreInformes").attr("rel", "20");
							$('#loading').css("display", "block");
							var selectedValue = $(this).val();
							var targeturl = $(this).attr('rel');
							$.ajax({
								type: 'post',
								url: targeturl,
								data: {regiao: selectedValue},
								beforeSend: function(xhr) {
									xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
								},
								success: function(response) {
									$('#loading').css("display", "none");
									//console.log(response.content);
									if (response) {
										$('#informes-list').html(response.content);	
									}
								},
								error: function(e) {
									alert("An error occurred: " + e.responseText.message);
									console.log(e);
								}
							});
							
							unsetTabs();
							$("#nav3").addClass("active");
							$("#informes-area").css("display", "block");
					});
					
					$('div.moreInformes').bind("mouseover", function(){
						getNextBunchNews();
					});
					
					$('.icon-size').bind("click", function() {
						$('.icon-size').removeClass('active-button');
						$(this).addClass('active-button');
						console.log("hei-x");
					});
					
					$('.informes-item2').bind("click", function() {
						coordinates = $(this).attr('rel').slit('/');
						console.log(coordinates);
					});							
					
					if (destino != ""){
						$('.dropdown option[value='+destino+']').attr('selected','selected');
					}
                }
            },
            error: function(e) {
                alert("An error occurred: " + e.responseText.message);
                console.log(e);
            }
        });	
               	
	}

/*MAPS FUNCTIONS*/
	var makersList = {'Incident': {}, 'Alert':{}, 'Notification':{}};
	var infoWindowsList = {'infoWindows': []};
	var markerCluster1;
	var markerCluster2;
	var markerCluster3;
	var mapCenterCoords = {"lat": 0, "lng": 0};
	var markersFiltro= "All";
	
	map_canvas.addListener('bounds_changed', function() {
		var latLng = map_canvas.getCenter();
		
		//console.log(Math.abs( latLng.lat() - mapCenterCoords["lat"] ));
		//console.log(Math.abs( latLng.lng() - mapCenterCoords["lng"]  ));
		//var coord2 = new google.maps.LatLng(mapCenterCoords["lat"], mapCenterCoords["lng"]);
		//$(".moreInformes2").attr("rel", "");
		
		if ( Math.abs( latLng.lat()-mapCenterCoords["lat"] ) > 0.01 || Math.abs( latLng.lng() - mapCenterCoords["lng"]  ) > 0.01 ){
			//console.log(latLng.lat() + " " + latLng.lng());
			console.log("center changed");
			//return;
			mapCenterCoords["lat"] = latLng.lat() ;
			mapCenterCoords["lng"] = latLng.lng() ;
			$('#informes-list').html("");
			getInformes();
		}
	});	

	function removeKml(){
		ctaLayer.setMap(null);
	}
	
	var input = /** @type {!HTMLInputElement} */(document.getElementById('search-ruas-input'));

		//var types = document.getElementById('type-selector');
		//map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
		//map.controls[google.maps.ControlPosition.TOP_LEFT].push(types);

		var autocomplete = new google.maps.places.Autocomplete(input);
		autocomplete.bindTo('bounds', map_canvas);
		var marker = new google.maps.Marker({
			map: map_canvas,
			anchorPoint: new google.maps.Point(0, -29)
		});
		
		$('#search-ruas-input').bind('change', function (){
				if (this.value == ""){
					infowindow.close();
					marker.setVisible(false);
				}
		});
		var infowindow = new google.maps.InfoWindow();
		  		
		autocomplete.addListener('place_changed', function() {
			infowindow.close();
			marker.setVisible(false);
			var place = autocomplete.getPlace();
			// If the place has a geometry, then present it on a map.
			if (place.geometry.viewport) {
			  map_canvas.fitBounds(place.geometry.viewport);
			} else {
			  map_canvas.setCenter(place.geometry.location);
			  map_canvas.setZoom(17);  // Why 17? Because it looks good.
			}

			marker.setIcon(/** @type {google.maps.Icon} */({
			  url: place.icon,
			  size: new google.maps.Size(71, 71),
			  origin: new google.maps.Point(0, 0),
			  anchor: new google.maps.Point(17, 34),
			  scaledSize: new google.maps.Size(35, 35)
			}));
			marker.setPosition(place.geometry.location);
			marker.setVisible(true);

			var address = '';
			if (place.address_components) {
			  address = [
				(place.address_components[0] && place.address_components[0].short_name || ''),
				(place.address_components[1] && place.address_components[1].short_name || ''),
				(place.address_components[2] && place.address_components[2].short_name || '')
			  ].join(' ');
			}

			infowindow.setContent('<div><strong>' + place.name + '</strong><br>' + address);
			infowindow.open(map_canvas, marker);	
			google.maps.event.addListener(marker,'click', (function(marker,infowindow){ 				
					return function() {							
						infowindow.open(map_canvas,marker);
						event.preventDefault();
					};
			})(marker,infowindow)); 		
		});

		//var infowindow = new google.maps.InfoWindow();
		//var marker = new google.maps.Marker({
		//map: map,
		//anchorPoint: new google.maps.Point(0, -29)

	google.maps.event.addListener(map_canvas, "rightclick", function(event) {
		var lat = event.latLng.lat();
		var lng = event.latLng.lng();
		// populate yor box/field with lat, lng
		console.log("(" + lat + ", " + lng);
	});	

	/*google.maps.event.addListener(map_canvas, "click", function(event) {
		for (var i = 0; i < infoWindowsList['infoWindows'].length; i++ ) { 
		 infoWindowsList['infoWindows'][i].close();
		}
		//infowindow.close();
	});*/
		
	function addMarker(id, titulo, conteudo, data, lati, longi){
		
		//var markers = []
		//markers[0].setMap(map);
		//var myLatLng = {lat: -25.363, lng: 131.044};
		
		var myLatLng = {lat: parseFloat(lati), lng: parseFloat(longi)};
		//console.log("Marker added " );
		//console.log(myLatLng );
		
		var imageIcon = "";
		var tituloInfoWindow = "";
		
		//Icone e título
		if (titulo == "Alert"){
			tituloInfoWindow = "Alerta";
			imageIcon = "/sureway3/img/icons/alert.png";
		} else if (titulo == "Incident"){
			tituloInfoWindow = "Incidente";
			imageIcon = "/sureway3/img/icons/incident.png";
		} else {
			tituloInfoWindow = "Notificação";
			imageIcon = "/sureway3/img/icons/notification.png";
		}
		
		if ( makersList[titulo][String(id)] == undefined ){
			console.log("Marker added");
			makersList[titulo][String(id)] = new google.maps.Marker({
				position: myLatLng,
				map: map_canvas,
				title: tituloInfoWindow,
				icon: imageIcon
			});	
			//console.log(makersList[titulo]);

			var contentString = '<div id="content">'+
				'<div id="siteNotice">'+
				'</div>'+
				'<h2 id="firstHeading" class="firstHeading">'+tituloInfoWindow+'</h2>'+
				'<div id="bodyContent">'+
				'<p>'+conteudo+'</p>'+
				'<p>Fonte: <a href="#">@OficialBHTRANS</a><br>'+
				'('+ data +')</p>'+
				'</div>'+
				'</div>';

			infoWindowsList['infoWindows'].push(new google.maps.InfoWindow({ content: contentString }));

			//var marker = makersList[titulo][makersList[titulo].length-1];
			var marker = makersList[titulo][String(id)];
			var infoWindow = infoWindowsList['infoWindows'][infoWindowsList['infoWindows'].length-1];
			
			/*google.maps.event.addListener(marker,'mouseover', (function(marker,infoWindow){ 
					return function() {
					   infoWindow.open(map_canvas,marker);
					};
			})(marker,infoWindow)); 
			
			google.maps.event.addListener(marker,'mouseout', (function(marker,infoWindow){ 
					return function() {
					   infoWindow.close();
					};
			})(marker,infoWindow));	*/	
			
			var infoWindow = infoWindowsList['infoWindows'][infoWindowsList['infoWindows'].length-1];
			var lista = infoWindowsList['infoWindows'];
			google.maps.event.addListener(marker,'click', (function(marker,infoWindow, lista){ 				
					return function() {		
						for (var i = 0; i < lista.length; i++ ) {  lista[i].close(); }	
						//map_canvas.setCenter(marker.getPosition());		
						infoWindow.open(map_canvas,marker);
						event.preventDefault();
					};
			})(marker,infoWindow,lista)); 
		}	
	}

	function addAllMarkers(lista){
		
		//console.log("Markers " + lista);
		for (i=0; i < lista.length; i++) { 
			if (lista[i]["display"] == true) {
				id = lista[i]["_id"];
				lat = lista[i]["lat"];
				longi = lista[i]["lng"];
				conteudo = lista[i]["Tweet"];
				categoria = lista[i]["type1"];
				dataT = lista[i]["createdAt"];
				//console.log({lat, longi});
				addMarker(id, categoria, conteudo, dataT, lat, longi);
			}
		}
		
		//var alerts = Object.keys(makersList["Alert"]).map(function(key){ return makersList["Alert"][key]; });
		//var incidents = Object.keys(makersList["Incident"]).map(function(key){ return makersList["Incident"][key]; });
		//var notifications = Object.keys(makersList["Notification"]).map(function(key){ return makersList["Notification"][key]; });
		//markerCluster1 = new MarkerClusterer(map_canvas, alerts);
		//markerCluster2 = new MarkerClusterer(map_canvas, incidents);
		//markerCluster3 = new MarkerClusterer(map_canvas, notifications);
	}

	
	function hideMarkers(tipo){

		if (tipo == "Incident" || tipo == "All"){
			for(var key in makersList["Incident"]){
				makersList["Incident"][key].setMap(null);
			}
			$('.incident-inform').addClass('hidden');
		} 
		
		if (tipo == "Alert" || tipo == "All"){
			for(var key in makersList["Alert"]){
				makersList["Alert"][key].setMap(null);
			}
			$('.alerta-inform').addClass('hidden');
		} 
		
		if (tipo == "Notification" || tipo == "All"){
			for(var key in makersList["Notification"]){
				makersList["Notification"][key].setMap(null);
			}
			$('.notificacao-inform').addClass('hidden');
		}
	}
	
	function showMarkers(tipo){
		$('.incident-inform').addClass('hidden');
		$('.alerta-inform').addClass('hidden');
		$('.notificacao-inform').addClass('hidden');
		
		if (tipo == "Incident" || tipo == "All"){
			for(var key in makersList["Incident"]){
				makersList["Incident"][key].setMap(map_canvas);
			}
			$('.incident-inform').removeClass('hidden');
		} 
		
		if (tipo == "Alert" || tipo == "All"){
			for(var key in makersList["Alert"]){
				makersList["Alert"][key].setMap(map_canvas);
			}
			$('.alerta-inform').removeClass('hidden');
		} 
		if (tipo == "Notification" || tipo == "All"){
			for(var key in makersList["Notification"]){
				makersList["Notification"][key].setMap(map_canvas);
			}
			$('.notificacao-inform').removeClass('hidden');
		}		
	}
	
	function bindingEvents() {
		$("#selectRegion").bind("change", function() {
				$(".moreInformes").attr("rel", "20");
				$('#loading').css("display", "block");
				var selectedValue = $(this).val();
				var targeturl = $(this).attr('rel');
				$.ajax({
					type: 'post',
					url: targeturl,
					data: {regiao: selectedValue},
					beforeSend: function(xhr) {
						xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
					},
					success: function(response) {
						$('#loading').css("display", "none");
						//console.log(response.content);
						if (response) {
							$('#informes-list').html(response.content);	
						}
					},
					error: function(e) {
						alert("An error occurred: " + e.responseText.message);
						console.log(e);
					}
				});
				
				unsetTabs();
				$("#nav3").addClass("active");
				$("#informes-area").css("display", "block");
		});

		$('div.moreInformes').bind("mouseover", function(){
			getNextBunchNews();
		});

		//Markers controlers
		$('#show-all-markers').bind("click", function() {
			$('.icon-size').removeClass('active-button');
			$(this).addClass('active-button');
			//hideMarkers("All");
			//console.log("all markers");
			showMarkers("All");
			markersFiltro = "All";
		});

		$('#show-only-alerts').bind("click", function() {
			$('.icon-size').removeClass('active-button');
			$(this).addClass('active-button');
			hideMarkers("Incident");
			hideMarkers("Notification");
			//hideMarkers("All");
			//console.log("Alerts");
			showMarkers("Alert");
			markersFiltro = "Alert";
		});

		$('#show-only-incidents').bind("click", function() {
			$('.icon-size').removeClass('active-button');
			$(this).addClass('active-button');
			hideMarkers("Notification");
			hideMarkers("Alert");
			//hideMarkers("All");
			//console.log("Incidents");
			showMarkers("Incident");
			markersFiltro = "Incident";
		});

		$('#show-only-notifications').bind("click", function() {
			$('.icon-size').removeClass('active-button');
			$(this).addClass('active-button');
			hideMarkers("Alert");
			hideMarkers("Incident");
			//hideMarkers("All");
			//console.log("Notifications");
			showMarkers("Notification");
			markersFiltro = "Notification";
		});							

		/*$('.icon-size').bind("click", function() {
			$('.icon-size').removeClass('active-button');
			$(this).addClass('active-button');
			console.log("hei");
		});*/

		$('.informes-item2').bind("click", function() {
			//console.log($(this).attr('rel'));
			var coordinates = $(this).attr('rel').split('/');
			var id = $(this)[0].id;
			//console.log(this);
			//console.log($(this));
			//console.log($(this)[0].id);
			
			//var coord = {lat: coordinates[0], lng: coordinates[1]};
			//console.log({lat: coordinates[0], lng: coordinates[1]});
			//map_canvas.setCenter({lat: parseFloat(coordinates[0]), lng: parseFloat(coordinates[1])});
			
			//Identificando o tipo de informe
			var tipo= "";
			if ($(this).hasClass('notificacao-inform')){tipo= "Notification";}
			else if ($(this).hasClass('alerta-inform')){tipo= "Alert";}
			else {tipo= "Incident";}
			//console.log(tipo);
			//console.log(makersList);
			var marker = makersList[tipo][id];	
			//console.log(marker);
			if (marker != undefined) new google.maps.event.trigger( marker, 'click', function (){event.preventDefault(); return;} );
		});							

		/*if (destino != ""){
			$('.dropdown option[value='+destino+']').attr('selected','selected');
		}*/
	} 
	 
	
    function getInformes() {
		removeKml();
		
		console.log("Informes function get");
		
		//hideMarkers("All");
		//infoWindowsList = {'infoWindows': []};
		//makersList = {'Incident': {}, 'Alert':{}, 'Notification':{}};
		
		var urlInformes = $('#getinformes').attr('rel');
		var destino = "";
		//console.log(urlInformes);
		if($("button.search.active").val() != undefined){
			destino = $("button.search.active").val();
		}
		//var dfd = $.Deferred();
		informes.getInformsList(map_canvas, function(informsListHTML) {
			//console.log("GOT: "+ informsListHTML);
			//console.log(result);
			//return;
			//var informsListHTML, listaInformes = result[0], result[1];
			
			if (informsListHTML != undefined){				
				//$('#informes-area').replaceWith(response.content);	
				console.log("Markers num: " + informes.getInformsArray().length);
				
				if (informes.getInformsArray().length != undefined) addAllMarkers(informes.getInformsArray());
				document.getElementById('informes-area').innerHTML = informsListHTML;
				$('#loading').css("display", "none");
				bindingEvents();
				var alerts = Object.keys(makersList["Alert"]).map(function(key){ return makersList["Alert"][key]; });
				var incidents = Object.keys(makersList["Incident"]).map(function(key){ return makersList["Incident"][key]; });
				var notifications = Object.keys(makersList["Notification"]).map(function(key){ return makersList["Notification"][key]; });
				console.log(alerts.length);
				console.log(incidents.length);
				console.log(notifications.length);
				showMarkers(markersFiltro);
				console.log(">>>>" + markersFiltro);
			}
		});
	     	
	}
	

	function searchDestination(destino, urlAction){
        $.ajax({
            type: 'post',
            url: urlAction,
            data: {destino: destino},
            beforeSend: function(xhr) {
                xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
            },
            success: function(response) {
				//console.log(response);
                if (response) {					
					$("#restricoes-area").html(response.content);	
                }
            },
            error: function(e) {
                alert("An error occurred: " + e.responseText.message);
                console.log(e);
            }
        });		
	}
 
	function getNextBunchNews() {
		//console.log("Get More");

		var urlInformes = $('#moreNews').attr('rel');
		//var destino = "";
		var nextbunch = $(".moreInformes2").attr("rel");
		//console.log(nextbunch);
		/*if($("button.search.active").val() != undefined){
			destino = $("button.search.active").val();
		} else if ($("#selectRegion").val() != "" || $("#selectRegion").val() != undefined){
			destino = $("#selectRegion").val();
		}*/
		if(nextbunch != -1 || nextbunch != '-1'){
			$('.sem-mais-informes').css("display", "none");
			$('div.moreInformes').css("display", "none");
			$("#informesLoading").css("display", "block");
			//console.log("antes");
			//$this->response->header(['Access-Control-Allow-Header' => 'X-DEBUGKIT-ID']);
			var request = new XMLHttpRequest();
			request.open('GET', "http://localhost:8080/informes/"+nextbunch, true);
			request.onload = function () {
				//console.log("depois");
				//console.log(request);
				/*var teste = request.responseText;
				teste = teste["data"];
				console.log(teste);
				console.log(JSON.stringify(teste));
				console.log("Ok");*/

				var dbResponse = JSON.stringify(JSON.parse(request.responseText)["data"]);
				//var dbResponse = request.responseText;
				//dbResponse = JSON.parse(dbResponse)["data"];
				//dbResponse = "'"+dbResponse+"'";
				var arrayResponse = JSON.parse(request.responseText)["data"];
				//console.log(arrayResponse);
				addAllMarkers(JSON.parse(request.responseText)["data"]);
				if (arrayResponse.length > 0 && arrayResponse[arrayResponse.length-1]["_id"] != undefined){
					nextbunch = arrayResponse[arrayResponse.length-1]["_id"];
				} else {
					nextbunch = -1;
				}
				
				dbResponse = getClosestInformes(dbResponse);
				
				//console.log(nextbunch);
				//console.log(dbResponse);
				//console.log(JSON.parse(dbResponse)["data"]);
				//console.log(dbResponse.data);
				//console.log(dbResponse["data"]);

				//dbResponse = '[{"Tweet": " Trânsito bom na Amazonas e há trechos maior volume mas fluindo principalmente sentido bairro bom Via Expressa próximo Metro Gameleira ","createdAt": "2016-07-20T18:06:12.078Z","objectId": "00nFi5Wagw","resp1": "OOOBOOOOOOOOOOOBIOBI","resp2": "OOOBOOOOOOOOOOOBIOBI","resp3": "OOOBOOOOOOOOOOOBIOBI","status": true,"type1": "Notification","type2": "Notification","type3": "Notification","updatedAt": "2016-08-26T12:01:59.627Z","user1": "578fb11f77bea","user2": "57b4d7be3058a","user3": "579df7d7a1f31", "lat": "-19.95", "lng": "-43.95"}]';

				if (dbResponse != undefined){		
				
					$.ajax({
						type: 'post',
						url: urlInformes,
						data: {dados: dbResponse, nextbunch: nextbunch},
						beforeSend: function(xhr) {
							xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
						},
						success: function(response) {
							//console.log(response);
							if (response) {									
								//$('#informes-area').replaceWith(response.content);	
								//document.getElementById('informes-area').append(response.content);
								$('#informes-list').append(response.content);
								 $("#informesLoading").css("display", "none");
								 $(".moreInformes2").css("display", "block");
								
								/*$("#selectRegion").bind("change", function() {
										$(".moreInformes").attr("rel", '20');
										$('#loading').css("display", "block");
										var selectedValue = $(this).val();
										var targeturl = $(this).attr('rel');
										$.ajax({
											type: 'post',
											url: targeturl,
											data: {regiao: selectedValue},
											beforeSend: function(xhr) {
												xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
											},
											success: function(response) {
												$('#loading').css("display", "none");
												//console.log(response.content);
												if (response) {
													$('#informes-list').html(response.content);	
												}
												$('div.moreInformes').css("display", "block");
											},
											error: function(e) {
												alert("An error occurred: " + e.responseText.message);
												console.log(e);
											}
										});
										
										unsetTabs();
										$("#nav3").addClass("active");
										$("#informes-area").css("display", "block");
								});
								*/
								
								$('.informes-item2').bind("click", function() {
									//console.log($(this).attr('rel'));
									var coordinates = $(this).attr('rel').split('/');
									var id = $(this)[0].id;
									//console.log(this);
									//console.log($(this));
									//console.log($(this)[0].id);
									
									//var coord = {lat: coordinates[0], lng: coordinates[1]};
									//console.log({lat: coordinates[0], lng: coordinates[1]});
									//map_canvas.setCenter({lat: parseFloat(coordinates[0]), lng: parseFloat(coordinates[1])});
									
									//Identificando o tipo de informe
									var tipo= "";
									if ($(this).hasClass('notificacao-inform')){tipo= "Notification";}
									else if ($(this).hasClass('alerta-inform')){tipo= "Alert";}
									else {tipo= "Incident";}
									//console.log(tipo);
									//console.log(makersList);
									var marker = makersList[tipo][id];	
									//console.log(marker);
									new google.maps.event.trigger( marker, 'click' );
								});									
								
								$(".moreInformes2").attr("rel", nextbunch);
								//console.log($(".moreInformes2").attr("rel"));
								/*if (destino != ""){
									$('.dropdown option[value='+destino+']').attr('selected','selected');
								}*/
							}
						},
						error: function(e) {
							alert("An error occurred: " + e.responseText.message);
							console.log(e);
						}
					});
				
				}	
			};
			request.onerror = function () {
				console.log(request.responseText);
			};
			request.send();
		}else {
			$('.sem-mais-informes').css("display", "block");
			$('div.moreInformes').css("display", "none");
		}
	}
	 
 
	function getNextBunchNews_old() {
	
		$('div.moreInformes').css("display", "none");
		$("#informesLoading").css("display", "block");
		var urlInformes = $('#moreNews').attr('rel');
		var destino = "";
		var nextbunch = parseInt($(".moreInformes").attr("rel"));
		
		if($("button.search.active").val() != undefined){
			destino = $("button.search.active").val();
		} else if ($("#selectRegion").val() != "" || $("#selectRegion").val() != undefined){
			destino = $("#selectRegion").val();
		}
		
        $.ajax({
            type: 'post',
            url: urlInformes,
            data: {destino: destino, nextbunch: nextbunch},
            beforeSend: function(xhr) {
                xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
            },
            success: function(response) {
				console.log(response);
                if (response) {									
					//$('#informes-area').replaceWith(response.content);	
					//document.getElementById('informes-area').append(response.content);
					$('#informes-list').append(response.content);
					 $("#informesLoading").css("display", "none");
					 $(".moreInformes").css("display", "block");
					
					/*$("#selectRegion").bind("change", function() {
							$(".moreInformes").attr("rel", '20');
							$('#loading').css("display", "block");
							var selectedValue = $(this).val();
							var targeturl = $(this).attr('rel');
							$.ajax({
								type: 'post',
								url: targeturl,
								data: {regiao: selectedValue},
								beforeSend: function(xhr) {
									xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
								},
								success: function(response) {
									$('#loading').css("display", "none");
									//console.log(response.content);
									if (response) {
										$('#informes-list').html(response.content);	
									}
									$('div.moreInformes').css("display", "block");
								},
								error: function(e) {
									alert("An error occurred: " + e.responseText.message);
									console.log(e);
								}
							});
							
							unsetTabs();
							$("#nav3").addClass("active");
							$("#informes-area").css("display", "block");
					});
					*/
					
					$(".moreInformes").attr("rel", (nextbunch+20).toString());
					/*if (destino != ""){
						$('.dropdown option[value='+destino+']').attr('selected','selected');
					}*/
                }
            },
            error: function(e) {
                alert("An error occurred: " + e.responseText.message);
                console.log(e);
            }
        });
	}
	
	unsetTabs();
	$("#nav1").addClass("active");
	$("#restricoes-area").css("display", "block");
	
	$("#nav1").bind('click', function(event){
		unsetTabs();
		$("#nav1").addClass("active");
		$("#restricoes-area").css("display", "block");
		var url = "http://sureway.com.br/webroot/kmls/restricao_veicular_bh.kml";
		var destino = $('#searchBox').val();
		if($("button.search.active").val() != undefined){
			destino = $("button.search.active").val();
		}
		//alert(destino);
		var urlAction = $('#restricoes').attr('rel');
		addLayer(url, "#restricoes-area", destino, urlAction);
		/*$('html, body').animate({
			scrollTop: $("#restricoes-area").offset().top
		}, 500);*/
    });

    $("#nav2").bind('click', function(event){
		unsetTabs();
		$("#restricoes-area").css("display", "block");
		$("#nav2").addClass("active");
		var url = "http://sureway.com.br/webroot/kmls/carga_descarga_bh_novo.kml";
		var destino = $('#searchBox').val();
		if($("button.search.active").val() != undefined){
			destino = $("button.search.active").val();
		}
		//alert(destino);
		var urlAction = $('#restricoes').attr('rel');
		addLayer(url, "#restricoes-area", destino, urlAction)
		/*$('html, body').animate({
			scrollTop: $("#restricoes-area").offset().top
		}, 10);*/
    });

    $("#nav3").bind('click', function(event){
		unsetTabs();
		$("#nav3").addClass("active");
		$("#restricoes-area").css("display", "block");
		$("#informes-area").css("display", "block");
	/*	$('html, body').animate({
			scrollTop: $("#informes-area").offset().top
		}, 10);*/
		$('#loading').css("display", "block");
		getInformes();
    });
    
    $("#nav4").bind('click', function(event){
		unsetTabs();
		$('#loading').css("display", "none");	
		$("#nav4").addClass("active");
		$("#get-app").css("display", "block");
		/*$('html, body').animate({
			scrollTop: $("#get-app").offset().top
		}, 10);	*/			
    }); 
 
	$("#bh-search").click(function( event ) {
		$("#searchBox").val("");
		$(".search").removeClass("active");
		$("#bh-search").addClass("active");		
		$(".navtab.active").trigger("click");
		var destino = "Belo Horizonte";
		var urlAction = $('#restricoes').attr('rel');
        var url = "http://sureway.com.br/webroot/kmls/restricao_veicular_bh.kml";
        addLayer(url, "#restricoes-area", destino, urlAction)
        $(".label-regiao").html(destino);
	});
	
	$("#rj-search").click(function( event ) {
		$("#searchBox").val("");
		$(".search").removeClass("active");
		$("#rj-search").addClass("active");		
		$(".navtab.active").trigger("click");
		var destino = "Rio de Janeiro";
		var urlAction = $('#restricoes').attr('rel');
        var url = "";
        addLayer(url, "#restricoes-area", destino, urlAction)
		//searchDestination(destino, urlAction);
		 $(".label-regiao").html(destino);		
	});

	$("#sp-search").click(function( event ) {
		$("#searchBox").val("");
		$(".search").removeClass("active");
		$("#sp-search").addClass("active");		
		$(".navtab.active").trigger("click");
		var destino = "São Paulo";
        var urlAction = $('#restricoes').attr('rel');
        var url = "";
		addLayer(url, "#restricoes-area", destino, urlAction)	
		$(".label-regiao").html(destino);
	});	
});
