/**
 * MODULE PATTERN: templates
 */ 
var templates = (function () {
	
	var getEntities = function (tweet, labelIOB){
		//tweet = "Caminhão com defeito na Contorno com Praça Clemente Faria via liberada"
		//labelIOB = "OOOOBOBIIOO";
		var novoTweet = tweet;
		var wordList = novoTweet.split(" ");
		var entities = labelIOB.match(/BI*/g);
		var posicoes = [];
		
		wordList.forEach(function(currentValue, index) {
			if (currentValue == "") wordList.splice(index, 1);
		});
		
		//console.log(entities);
		//console.log(novoTweet);
		//console.log(wordList.length);
		//console.log(labelIOB);
		//console.log(labelIOB.length);
		
		entities.forEach(function(currentValue, index) {
			posicoes.push([labelIOB.indexOf(currentValue), labelIOB.indexOf(currentValue)+currentValue.length]);			
			labelIOB = labelIOB.replace(currentValue, Array(currentValue.length+1).join('x'));
		});
		
		//console.log(posicoes);
		//console.log("label: " + labelIOB);
		
		var entitiesNames = [];
		//console.log(wordList);
		
		posicoes.forEach(function(currentValue, index) {
			//console.log([currentValue[0], currentValue[1]]);
			entitiesNames.push(wordList.slice(currentValue[0], currentValue[1]).join(" "));
		});
		
		//console.log(entitiesNames);
		entitiesNames.forEach(function(currentValue, index) {
			novoTweet = novoTweet.replace(currentValue, "<span style='color:blue'>"+currentValue+"</span>");
		});
		//console.log(novoTweet);
		return (novoTweet)
	}
	
	var informsAreaTemplate = function(lista){
		var htmlString = "\
			<div>\
				<div id='filter-region' style='margin-bottom: 15px;' class='filtro'>\
					<div class='btn-group' role='group' aria-label='...'>\
					  <button id='show-all-markers' type='button' class='icon-size btn btn-default'><span>Todos</span></button>\
					  <button id='show-only-alerts' type='button' class='icon-size btn btn-default'><span><img src='/sureway3/img/icons/alert.png'/></span></button>\
					  <button id='show-only-incidents' type='button' class='icon-size btn btn-default'><span><img src='/sureway3/img/icons/incident.png'/></span></button>\
					  <button id='show-only-notifications' type='button' class='icon-size btn btn-default'><span><img src='/sureway3/img/icons/notification.png'/></span></button>\
					</div>\
			</div>\
		";
		//getEntities();
		htmlString = htmlString + "<div id='informes-list' class = 'informes-list2' >"; 
		
		for (var i=0; i < lista.length; i++){
			var classe = "";
			
			//Show if it is inside map bounds
			if ( lista[i]["display"] == false) classe = "hidden ";
		
			//Type
			if ( lista[i]['type1'] == "Incident" ) classe = classe+" informes-item2 incident-inform";
			else if ( lista[i]['type1'] == "Alert" ) classe = classe+" informes-item2 alerta-inform";
			else classe = classe+" informes-item2 notificacao-inform";
			var newTweet = getEntities(lista[i]["Tweet"], lista[i]["resp1"]);
			
			htmlString = htmlString + "\
				<div id='"+lista[i]['_id']+"' class='"+classe+"' rel='"+lista[i]['lat']+"/"+lista[i]['lng']+"'>\
					<small style='float: right;'>"+lista[i]['createdAt']+"</small>\
					<strong style='text-transform: uppercase;'>"+lista[i]['type1']+"</strong>\
					<p>"+ newTweet+"</p>\
					<p><small>Fonte: @OficialBHTRANS</small></p>\
				</div>\
			";
			//console.log(htmlString);
			
		}
		htmlString = htmlString + "</div>";
		htmlString = htmlString + "";
		return (htmlString);
	};
	
	// Public API
	return {
		informsAreaTemplate: informsAreaTemplate,
		getEntities: getEntities
	};
	
})();
 
 
/**
 * MODULE PATTERN: MapFunctions
 */
var mapFunctions = (function () {
	var makersList = {'Incident': {}, 'Alert':{}, 'Notification':{}};
	var infoWindowsList = {'infoWindows': []};
	var markerCluster1;
	var markerCluster2;
	var markerCluster3;
	var mapCenterCoords = {"lat": 0, "lng": 0};
	
	// Public API
	return {
		makersList 	    : makersList, 
		infoWindowsList : infoWindowsList,
		markerCluster1  : markerCluster1,
		markerCluster2  : markerCluster2,
		markerCluster3  : markerCluster3,
		mapCenterCoords : mapCenterCoords
	};
	
})();		
		
/**
 * MODULE PATTERN: Informes
 */
var informes = (function() {
	//Atributos
	var privateThing = "secret";
	var publicThing = "not secret";
	var informsArray = [];
	
	//Funcoes
	var changePrivateThing = function() {
			privateThing = "super secret";
	};
	 
	var sayPrivateThing = function() {
		console.log( privateThing );
		changePrivateThing();
	};
	
	var setInformsList = function (informListToShow) {
		informsArray = [];
		for (var i=0; i < informListToShow.length; i++){
			informsArray.push(informListToShow[i]);
		}
		//console.log(informsArray);
	}
	
	var getClosestInformes = function (listaJson, map_canvas){
		//var newList = [];		
		//console.log(map_canvas.getBounds());
		//var center = new google.maps.LatLng(mapCenterCoords["lat"] , mapCenterCoords["lng"]);
		//var coord2 = new google.maps.LatLng(-19.911302814363676, -43.9068603515625);
		//var coord2 = new google.maps.LatLng(-19.92304420120293,  -43.90853404998779);
		
		//var lista = JSON.parse(listaJson);
		var lista = listaJson;
		//console.log("Closest>>" + lista);
		//console.log(map_canvas.getZoom());
		
		for (var i = 0; i < lista.length; i++){
			/*var coord2 = new google.maps.LatLng(parseFloat(lista[i]["lat"]), parseFloat(lista[i]["lng"]));		
			var distance = google.maps.geometry.spherical.computeDistanceBetween(center, coord2);
			console.log(distance);	
			console.log("Total: " + 3000*map_canvas.getZoom());
			if (distance <= 3000*map_canvas.getZoom()){
				console.log("Dentro");
				newList.push(lista[i]);
			}	*/
			
			if( map_canvas.getBounds().contains(new google.maps.LatLng(lista[i]["lat"], lista[i]["lng"])) ){
				// code for showing your object, associated with markers[i]
				console.log("Inside");
				lista[i]["display"] = true;					
				
			} else {
				console.log("Outsite");
				lista[i]["display"] = false;	
			}
			//console.log(lista[i]["display"]);
		}
		//console.log("show:: " + lista.length);
		return (lista);
	}
	
	var getInformsArray = function () {
		return (informsArray)
	};
	
	var getInformsList = function (map_canvas, _callback) {
		var request = new XMLHttpRequest();
		//request.open('GET', "http://localhost:8080/informes", true);
		request.open('GET', "http://localhost:8080/closest", true);
		request.onload = function () {
			
			var dbResponse = JSON.parse(request.responseText)["data"];			
			var informListToShow = getClosestInformes(dbResponse, map_canvas);
			
			//Get HTML template
			var informesHTML = templates.informsAreaTemplate(informListToShow);
			setInformsList(informListToShow);

			_callback(informesHTML);  
			return (informesHTML);					
		
		};//end Onload
		request.onerror = function () {
			console.log(request.responseText);
		};
		request.send();
	};
	
	// Public API
	return {
		informsArray: informsArray,
		getClosestInformes: getClosestInformes,
		getInformsArray: getInformsArray,
		getInformsList: getInformsList		
	};
	
})();

	
