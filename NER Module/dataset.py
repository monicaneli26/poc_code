#!/usr/bin/python
# -*- coding: utf-8 -*-

from __future__ import division
import csv
import re
import operator
import numpy
import matplotlib.pyplot as plt
from nlp import NLP
from dicionario import Dictionary
from rules import Rules
from postagging import POStagging


class Dataset:

	geoRefNum = 0							# Number of geo-refs manually annotated
	upperCaseTerms = 0						# Number of terms starting with upper-case letters
	uniGramsGeoRefs = 0						# Number of 1-grams with geo evidence
	biGramsGeoRefs = 0						# Number of 2-grams with geo evidence
	triGramsGeoRefs = 0						# Number of 3-grams with geo evidence
	uniGramsRecognized = 0					# Number of 1-grams recognized by the dictionary
	biGramsRecognized = 0  					# Number of 2-grams recognized by the dictionary
	triGramsRecognized = 0					# Number of 3-grams recognized by the dictionary
	nGramRecognizedBruteForce = 0
	maxNGramFound = 0
	bruteForceLabel = None
	porcentageUpperCase = [0, 0, 0]
	porcentageEntityUpperCase = [0, 0, 0]
	porcentageExpressionsRecognized = [0, 0, 0]
	porcentageTermsGeoEvidence = [0, 0, 0]
	totalNumInstances = 2861
	minSupportValue = 1
	nlp = None
	dictionary = None
	rules = None
	posTagging = None
	confusionMatrixFB 	= {'II': 0, 'IO': 0, 'IB': 0, 'OI': 0, 'OO': 0, 'OB': 0, 'BI': 0, 'BO': 0, 'BB': 0}		#IOB vs IOB
	confusionMatrixFBR	= {'II': 0, 'IO': 0, 'IB': 0, 'OI': 0, 'OO': 0, 'OB': 0, 'BI': 0, 'BO': 0, 'BB': 0}		#IOB vs IOB
	cmRules0 = {'II': 0, 'IO': 0, 'IB': 0, 'OI': 0, 'OO': 0, 'OB': 0, 'BI': 0, 'BO': 0, 'BB': 0}		#IOB vs IOB
	cmRules1 = {'II': 0, 'IO': 0, 'IB': 0, 'OI': 0, 'OO': 0, 'OB': 0, 'BI': 0, 'BO': 0, 'BB': 0}		#IOB vs IOB
	cmRules2 = {'II': 0, 'IO': 0, 'IB': 0, 'OI': 0, 'OO': 0, 'OB': 0, 'BI': 0, 'BO': 0, 'BB': 0}		#IOB vs IOB
	cmRules3 = {'II': 0, 'IO': 0, 'IB': 0, 'OI': 0, 'OO': 0, 'OB': 0, 'BI': 0, 'BO': 0, 'BB': 0}		#IOB vs IOB
	cmRules4 = {'II': 0, 'IO': 0, 'IB': 0, 'OI': 0, 'OO': 0, 'OB': 0, 'BI': 0, 'BO': 0, 'BB': 0}		#IOB vs IOB
	entitiesFile = csv.writer(open("trainingEntities.csv", 'w', newline=''))
	entitiesByDicFile = csv.writer(open("entitiesByDicFile.csv", 'w', newline=''))
	entitiesByDicRulesFile = csv.writer(open("entitiesByDicRulesFile.csv", 'w', newline=''))
		
	#~ def buildDataset(self, gazetteer):
		#~ self.nlp = NLP()
		#~ self.rules = Rules()
		#~ self.dictionary = Dictionary(gazetteer)
		#~ self.training(fileNameTraining)
		#~ self.posTagging = POStagging()
		#~ self.generateRules()
	
	def initiateModules (self, gazetteer):
		self.nlp = NLP()
		self.rules = Rules(self.posTagging)
		self.dictionary = Dictionary(gazetteer)
		
	def trainingModules(self, foldsSet, nFolds, exceptFoldIndex):
		self.dictionaryTraining(foldsSet, nFolds, exceptFoldIndex)
		self.rules.generateRules()
	
	#~ @classmethod
	#~ def testingMethodConstructor(self, streetsFileName, regionsFileName, fileNameTraining):
		#~ self.dictionary = Dictionary([streetsFileName, regionsFileName])
		#~ self.nlp = NLP()
		#~ #self.training(fileNameTraining)

	def initializeAtt(self):
		self.geoRefNum = 0								# Number of geo-refs manually annotated
		self.upperCaseTerms = 0							# Number of terms starting with upper-case letters
		self.uniGramsGeoRefs = 0						# Number of 1-grams with geo evidence
		self.biGramsGeoRefs = 0							# Number of 2-grams with geo evidence
		self.triGramsGeoRefs = 0						# Number of 3-grams with geo evidence
		self.uniGramsRecognized = 0						# Number of 1-grams recognized by the dictionary
		self.biGramsRecognized = 0  					# Number of 2-grams recognized by the dictionary
		self.triGramsRecognized = 0						# Number of 3-grams recognized by the dictionary
		self.nGramRecognizedBruteForce = 0
		self.maxNGramFound = 0
		self.bruteForceLabel = None

	#~ def extractEntities(self, fileNameTest):
		#~ self.readDataset(fileNameTest)
		#~ #self.printAttributes()
		#~ #self.testAndOperationIOB()
		#~ #self.writeDataset()


	#~ def writeDataset(self):
		#~ f1 = csv.writer(open("datasetFinal.csv", 'w', newline=''))
		#~ # Write CSV Headers
		#~ f1.writerow(["Label", "Texto", "tweet"])
		#~ #f1.writerow([])


	#~ def readDataset(self, fileNameTest):
		#~ f1 = csv.writer(open("datasetFinal.csv", 'w', newline=''))
		#~ # Write CSV Headers
		#~ f1.writerow(["Label", "Texto", "tweet", "upperCaseTerms", "uniGramsGeoRefs", "biGramsGeoRefs", "triGramsGeoRefs", "uniGramsRecognized", "biGramsRecognized", "triGramsRecognized", "nGramRecognizedBruteForce", "maxNGramFound"])
#~ 
		#~ with open(fileNameTest, 'rb') as csvfile:
			#~ spamreader = csv.reader(csvfile, delimiter=',', quotechar='|')
			#~ for row in spamreader:
				#~ #print row
				#~ #self.countGeoReferences(row[0])
				#~ sentenceTokens = self.nlp.tokenization(row[1])
				#~ self.countUpperCaseTerms(sentenceTokens)
				#~ self.countNGramasRecognized(sentenceTokens)
				#~ self.bruteForceAnnotation(sentenceTokens)
				#~ f1.writerow([self.bruteForceLabel, row[1], row[2], self.upperCaseTerms, self.uniGramsGeoRefs, self.biGramsGeoRefs, self.triGramsGeoRefs, self.uniGramsRecognized, self.biGramsRecognized, self.triGramsRecognized, self.nGramRecognizedBruteForce, self.maxNGramFound])
				#~ self.initializeAtt()
			#~ csvfile.close()

	#~ def extractEntitiesRow(self, row):
		#~ #print row
		#~ #print row[1]
		#~ self.countGeoReferences(row[0])
		#~ sentenceTokens = self.nlp.tokenization(row[1])
		#~ self.countUpperCaseTerms(sentenceTokens)
		#~ self.countNGramasRecognized(sentenceTokens)
		#~ self.printAttributes()

	#~ def countGeoReferences(self, patternLabel):
		#~ #print "<GEO-REFERENCES Module>"
		#~ occurrences = re.findall('(BI*)', patternLabel)
		#~ self.geoRefNum.append(len(occurrences))
		#~ #print patternLabel
		#~ #print occurrences
		#~ #return occurrences


	#------------------------------------------------------
	# Function countUpperCaseTerms
	# @params:
	# @author: Monica
	#------------------------------------------------------
	def countUpperCaseTerms(self, sentenceTokens):
		#print "<UPPER-CASE Module>"
		#print sentenceTokens
		termList = self.nlp.extractUpperCaseTerms(sentenceTokens)
		self.upperCaseTerms=len(termList)
		#print len(termList)
		return len(termList)


	#------------------------------------------------------
	# Function
	# @params:
	# @author: Monica
	#------------------------------------------------------
	def andOperationIOB(self, label1, label2):
		#print "\n---------"
		#print str(len(label1)) + "  " + str(len(label2))
		if (len(label1) > len(label2)):
			aux = label1
			label1 = label2
			label2 = aux
		labelFinal = ""
		#IOB = ['B', 'O']
		#print  enumerate(label1)
		for idx, letter in enumerate(label1):


			#print (letter == 'O' and label2[idx] == 'B') or (letter == 'B' and label2[idx] == 'O')
			#print (letter == 'I' and label2[idx] == 'B') or (letter == 'B' and label2[idx] == 'I')
			#print (letter == 'O' and label2[idx] == 'I') or (letter == 'I' and label2[idx] == 'O')

			#B vs O = B
			if ((letter == 'O' and label2[idx] == 'B') or (letter == 'B' and label2[idx] == 'O')):
				#print "1antes: " + labelFinal
				labelFinal=labelFinal + 'B'
				#print "1dpis: " + labelFinal

			#B vs I = I
			elif ((letter == 'I' and label2[idx] == 'B') or (letter == 'B' and label2[idx] == 'I')):
				#print "2antes: " + labelFinal
				labelFinal=labelFinal + 'I'
				#print "2dpois: " + labelFinal

			#O vs I = I
			elif ((letter == 'O' and label2[idx] == 'I') or (letter == 'I' and label2[idx] == 'O')):
				#print "3antes: " + labelFinal
				labelFinal=labelFinal + 'I'
				#print "3dpois: " + labelFinal

			elif (letter == label2[idx]): labelFinal=labelFinal + letter

			#print letter + " [" + str(idx) + "] " +  label2[idx] + " === " + labelFinal

		#print labelFinal
		if(len(label2) > len(label1)):
			#print "Subs: " + label2[len(label2)-(len(label2)-len(label1)):]
			#print len(label2)-(len(label2)-len(label1))
			labelFinal=labelFinal + label2[len(label2)-(len(label2)-len(label1)):]

		return labelFinal


	#------------------------------------------------------
	# Function
	# @params:
	# @author: Monica
	#------------------------------------------------------
	def testAndOperationIOB(self):
		print ("B vs O == " + self.andOperationIOB("B", "O"))
		print ("B vs I == " + self.andOperationIOB("B", "I"))
		print ("O vs I == " + self.andOperationIOB("O", "I"))
		print ("BO vs OI == " + self.andOperationIOB("BO", "OI"))
		print ("BIO vs IIOO == " + self.andOperationIOB("BIO", "IIOO"))


	#------------------------------------------------------
	# Function
	# @params:
	# @author: Monica
	#------------------------------------------------------
	#def bruteForceAnnotation(self, sentenceTokens):
	def dictionaryNER(self, sentenceTokens):
		startingIndex = 1
		labelFinal = ""

		#print('Tokens::: ' + str(sentenceTokens))
		for token in sentenceTokens:
			nGram = []
			nGram.append(token)
			#print ('Token ::: '+token)
			
			#Numeric
			if (not token.isnumeric()):
				matched = self.dictionary.verifyAddress(token)
			else: matched = None
			
			labelPrevious = ""
			#print (token + " <::> " + str(matched))

			#Token Recognized
			if (matched != None and len(matched)>=1):
				self.nGramRecognizedBruteForce = self.nGramRecognizedBruteForce + 1
				labelPrevious = 'B'

				#Get the bigger nGram found
				if (len(nGram) > self.maxNGramFound):
					self.maxNGramFound = len(nGram)
			
			#Token not recognized
			else: labelPrevious = 'O'
			
			#print (token + " <:::> " + labelPrevious)

			#Appending tokens to get a new expression
			for idx in range(startingIndex, len(sentenceTokens)):
				labelIter = ""
				nGram.append(sentenceTokens[idx])
				matched = self.dictionary.verifyAddress(" ".join(nGram))
				#print (" ".join(nGram) + " " + str([matched]))

				#Expression recognized
				if (matched != None and len(matched)>=1):
					self.nGramRecognizedBruteForce = self.nGramRecognizedBruteForce + 1
					labelIter = 'B'
					for i in range(len(nGram)-1): labelIter = labelIter + 'I'

					#if the recognized n-gram is bigger than the max found
					if (len(nGram) > self.maxNGramFound):
						self.maxNGramFound = len(nGram)
				else:
					for i in range(len(nGram)): labelIter = labelIter + 'O'
				
				#print (str(" ".join(nGram)) + " <:::> " + labelIter)
				#print (labelPrevious + " vs " + labelIter + " == " + self.andOperationIOB(labelPrevious, labelIter))
				labelPrevious = self.andOperationIOB(labelPrevious, labelIter)

			#print ("\n---------\n")
			if (startingIndex >= 2):
				##print  "subst[" + str(startingIndex-1) +":] " + labelFinal[startingIndex-1:] + " vs " + labelPrevious + " == " + self.andOperationIOB(labelFinal[startingIndex-1:], labelPrevious)

				labelPrevious = self.andOperationIOB(labelFinal[startingIndex-1:], labelPrevious)

				##labelFinal[startingIndex-1:] = labelFinal[startingIndex-1:].replace(labelFinal[startingIndex-1:], labelPrevious, 1)
				labelFinal = labelFinal[:startingIndex-1] + labelPrevious
			else:
				labelFinal = labelPrevious
			#print ("\nFinal: " +labelFinal + "\n")
			startingIndex = startingIndex + 1
		#print (">>>> Final:: " +labelFinal)
		self.bruteForceLabel = labelFinal
		return self.bruteForceLabel
	
	
	def countGeoEvidence(self, sentenceTokens):
		numTermsGeoEvidence = 0
		for term in sentenceTokens:
			if (self.nlp.hasGeoEvidenceToken(term)):
				numTermsGeoEvidence += 1
		return numTermsGeoEvidence
				

	#------------------------------------------------------
	# Function
	# @params:
	# @author: Monica
	#------------------------------------------------------
	def training(self, fileNameTrainning):
		print ("\nDICTIONARY TRAINING.....\n")
		numLinesRead = 0
		
		with open(fileNameTrainning, 'r') as csvfile:
			spamreader = csv.reader(csvfile, delimiter=',', quotechar='|')
			#cont = 0
			
			for row in spamreader:
				#print ""
				#print (row[1])
				sentenceTokens = self.nlp.tokenization(row[1])
				numUpperCase = self.countUpperCaseTerms(sentenceTokens)
				self.porcentageUpperCase[0] = self.porcentageUpperCase[0] + (numUpperCase/len(sentenceTokens))			
				
				numGeoEvidenceTerms = self.countGeoEvidence(sentenceTokens)
				self.porcentageTermsGeoEvidence[0] += numGeoEvidenceTerms/len(sentenceTokens)			
				
				#print row[1]
				entities = self.extractAnnotatedEntities(row[0], sentenceTokens)
				
				numRecognized = 0
				#Verifing terms on dictionary
				for entity in entities:
					#print ("Entity : " + str(entity).decode('utf-8'))
					if (self.dictionary.verifyAddress(str(entity.decode('utf-8'))) != None):
						numRecognized = numRecognized + 1
					else:
						self.addToDictionary([entity])
				if (len(entities) > 0):
					self.porcentageExpressionsRecognized[0] += numRecognized/len(entities)
				numLinesRead+= 1
				#print entities
				#if (cont == 5): break
				#cont = cont + 1
				#break
				
			csvfile.close()
		
		#total of lines read
		self.porcentageTermsGeoEvidence[1] = numLinesRead
		self.porcentageUpperCase[1] = numLinesRead
		self.porcentageExpressionsRecognized[1] = numLinesRead
		
		self.dictionary.sortKeys()
		print ("-----------------------------------------------\n")


	#------------------------------------------------------
	# Function
	# @params:
	# @author: Monica
	#------------------------------------------------------
	def dictionaryTraining(self, foldsSet, nFolds, exceptFoldIndex):
		print ("\nDICTIONARY TRAINING......")
		numLinesRead = 0
		
		for index,fold in enumerate(foldsSet):
			if (index != exceptFoldIndex):
				for row in fold:
					#print (row[1])
					sentenceTokens = self.nlp.tokenization(row[1])
					entities = self.extractAnnotatedEntities(row[0], sentenceTokens)
			
					#Verifing terms on dictionary
					for entity in entities:
						#print ("Entity Vrfy: " + str(entity.decode('utf-8')))
						if (self.dictionary.verifyAddress(str(entity.decode('utf-8'))) == None):
							#print ("Entity ADDED ANTES: " + str(entity))
							#toAdd = str(entity.decode('utf-8'))
							#print ("Entity ADDED: " + str(entity.decode('utf-8')))
							self.addToDictionary([str(entity.decode('utf-8'))])
					
		
		self.dictionary.dictionaryInfo()
		#print ("-----------------------------------------------\n")

#~ 
	#~ def calculateProbabilities(self):
		#~ self.porcentageTermsGeoEvidence[2] = self.porcentageTermsGeoEvidence[0]/self.porcentageTermsGeoEvidence[1]
		#~ self.porcentageUpperCase[2] = self.porcentageUpperCase[0] / self.porcentageUpperCase[1]
		#~ self.porcentageExpressionsRecognized[2] = self.porcentageExpressionsRecognized[0] / self.porcentageExpressionsRecognized[1]
#~ 
		#~ print ("Probabilities: [GEO] " + str(self.porcentageTermsGeoEvidence)+ ", [UPPER] " +str(self.porcentageUpperCase) + ", [RECOG] " + str(self.porcentageExpressionsRecognized))
		#~ 
	#------------------------------------------------------
	# Function
	# @params:
	# @author: Monica
	#------------------------------------------------------
	def extractAnnotatedEntities(self, patternLabel, sentenceTokens):
		occurrences = re.findall('(BI*)', patternLabel)
		entities = []
		newPattern = patternLabel
		indices = []
		#print occurrences
		#print patternLabel
		for indx, occurrence in enumerate(occurrences):
			indexStart = newPattern.find(occurrences[indx])
			indices.append([indexStart, indexStart+len(occurrences[indx])])
			subs= ""
			for i in range(len(occurrences[indx])): subs= subs+'X'
			newPattern = newPattern.replace(occurrences[indx], subs, 1)
			#print "Index: " + str(indx)+ " " + str(occurrences[indx])
			#print newPattern
		#print indices
		termo = []
		for i,idx in enumerate(indices):
			#print idx
			for position in range(idx[0], idx[1]):
				#print str(idx[0]) + " " + str(idx[1])
				#try:
				#print position
				#print len(sentenceTokens)
				#print sentenceTokens[position-1]
				#print sentenceTokens[position]
				#if (position == len(sentenceTokens)): position = position -1
				termo.append(sentenceTokens[position])
				#except Exception as e: 
				#	print e
					
			#print (" ".join(termo))
			entities.append(self.nlp.tokenEncodeDecode(" ".join(termo)).upper())
			self.rules.addNGram(termo, occurrences[i])
			termo = []
		return entities

	#------------------------------------------------------
	# Function
	# @params:
	# @author: Monica
	#------------------------------------------------------
	def addToDictionary(self, entities):
		for entity in entities: 
			self.dictionary.addToDictionary(entity)
			self.entitiesFile.writerow([entity])

	#------------------------------------------------------
	# Function
	# @params:
	# @author: Monica
	#------------------------------------------------------
	def printAttributes(self):
		print ("DATASET ENTITIES: " + str(self.geoRefNum)	)
		print ("UPPERCASE TERMS: " + str(self.upperCaseTerms))
		print ("[GEO] 1-GRAMS: " + str(self.uniGramsGeoRefs) )
		print ("[GEO] 2-GRAMS: " + str(self.biGramsGeoRefs)  )
		print ("[GEO] 3-GRAMS: " + str(self.triGramsGeoRefs) )
		print ("1-GRAMS: " + str(self.uniGramsRecognized)    )
		print ("2-GRAMS: " + str(self.biGramsRecognized)     )
		print ("3-GRAMS: " + str(self.triGramsRecognized)    )
		print ("BRUTE FORCE N-GRAMS RECOGNIZED: " + str(self.nGramRecognizedBruteForce))
		print ("MAX N-GRAM RECOGNIZED BY BF: " + str(self.maxNGramFound)               )
		print ("LABEL: " + str(self.bruteForceLabel))

	#------------------------------------------------------
	# Function
	# @params:
	# @author: Monica
	#------------------------------------------------------
	def checkHitsRate(self, predictedLabelFB, currentLabelFB, predictedLabelFBR, currentLabelFBR ):
		
		if (len(predictedLabelFB) != len(currentLabelFB) or len(predictedLabelFBR) != len(currentLabelFBR)): 
			print (">>>>>>>>>>>>>>>>>>>Erro labels sizes FB(" + str(len(predictedLabelFB)) + " " + str(len(currentLabelFB)) + ")")
			print (">>>>>>>>>>>>>>>>>>>Erro labels sizes FBR(" + str(len(predictedLabelFBR)) + " " + str(len(currentLabelFBR)) + ")")
			return
		
		for i,item in enumerate(currentLabelFB):
			self.confusionMatrixFB[item+predictedLabelFB[i]] +=  1
			
		for i,item in enumerate(currentLabelFBR):
			self.confusionMatrixFBR[item+predictedLabelFBR[i]] +=  1
	
	#------------------------------------------------------
	# Function
	# @params:
	# @author: Monica
	#------------------------------------------------------
	def checkHitsRateRules(self, labels, currentLabel):
		
		for label in labels:
			if (len(label) != len(currentLabel)): 
				print (">>>>>>>>>>>>>>>>>>>Erro labels sizes Rules(" + str(len(label)) + " " + str(len(currentLabel)) + ")")
				return
		
		label = labels[0]
		for i,item in enumerate(currentLabel):
			self.cmRules0[item+label[i]] +=  1
			
		label = labels[1]
		for i,item in enumerate(currentLabel):
			self.cmRules1[item+label[i]] +=  1
			
		label = labels[2]
		for i,item in enumerate(currentLabel):
			self.cmRules2[item+label[i]] +=  1
		
		label = labels[3]
		for i,item in enumerate(currentLabel):
			self.cmRules3[item+label[i]] +=  1
		
		label = labels[4]
		for i,item in enumerate(currentLabel):
			self.cmRules4[item+label[i]] +=  1		
			
	#~ def readDataset2(self, fileNameTest):
		#~ f1 = csv.writer(open("labels.csv", 'w', newline=''))
		#~ # Write CSV Headers
		#~ #f1.writerow(["Texto", "LabelFB", "Label1", "label2", "label3", "label4", "label5", "label6"])
		#~ f1.writerow(["Texto", "LabelFB", "Label1", "label2", "label3", "label4", "label5"])
		#~ 
		#~ self.rules.rulesInfo()
#~ 
		#~ with open(fileNameTest, 'r', encoding='utf-8') as csvfile:
			#~ spamreader = csv.reader(csvfile, delimiter=',', quotechar='|')
			#~ for row in spamreader:
				#~ #print row
				#~ #self.countGeoReferences(row[0])
				#~ sentenceTokens = self.nlp.tokenization(row[1])
				#~ #self.countUpperCaseTerms(sentenceTokens)
				#~ #self.countNGramasRecognized(sentenceTokens)
				#~ self.dictionaryNER(sentenceTokens)
				#~ labels = self.rulesNER(row)
				#~ #f1.writerow([row[1], self.bruteForceLabel, labels[0], labels[1], labels[2], labels[3], labels[4], labels[5]])
				#~ f1.writerow([row[1], self.bruteForceLabel, labels[0], labels[1], labels[2], labels[3], labels[4]])
				#~ #self.initializeAtt()
			#~ csvfile.close()	

	#~ def readDataset3(self, fileNameTest):
		#~ f1 = csv.writer(open("labelsAccuracy.csv", 'w', newline=''))
		#~ # Write CSV Headers
		#~ f1.writerow(["Texto", "LabelFB", "Label1"])
		#~ 
		#~ self.rules.rulesInfo()
		#~ self.rules.printRules()
#~ 
		#~ with open(fileNameTest, 'r', encoding='utf-8') as csvfile:
			#~ spamreader = csv.reader(csvfile, delimiter=',', quotechar='|')
			#~ for row in spamreader:
				#~ print('--'*80)
				#~ print (row[1])
				#~ #self.countGeoReferences(row[0])
				#~ sentenceTokens = self.nlp.tokenization(row[1])
				#~ print (sentenceTokens)
				#~ #self.countUpperCaseTerms(sentenceTokens)
				#~ #self.countNGramasRecognized(sentenceTokens)
				#~ self.dictionaryNER(sentenceTokens)
				#~ label = self.dictionaryRulesNER(sentenceTokens)
				#~ self.checkHitsRate(self.bruteForceLabel, row[0], label, row[0])
				#~ f1.writerow([row[1], self.bruteForceLabel, label])
				#~ #self.initializeAtt()
			#~ csvfile.close()
		#~ #teste = self.nlp.extractNGramas4("Avenida Amazonas", 2)
		#~ #print teste
		#~ #print self.posTagging.posTaggingNGram(teste[0])
		#~ print (self.confusionMatrixFB)
		#~ print (self.confusionMatrixFBR)

	def getLabelGeo(self, labelPrevious, nGramSize, isGeo):
		labelSize = len(labelPrevious)
		if (isGeo):
			#print "Label= "+str(labelSize)+"-- ngram= "+str(nGramSize)
			if (labelSize == 0): 
				labelPrevious = labelPrevious + 'B'
				for i in range(nGramSize-1): labelPrevious = labelPrevious + 'I'
				
			elif (labelPrevious[labelSize-1] == 'B'): 
				for i in range(nGramSize): labelPrevious = labelPrevious + 'I'
			elif (labelPrevious[labelSize-1] == 'I'): 
				for i in range(nGramSize): labelPrevious = labelPrevious + 'I'
			else: 
				labelPrevious = labelPrevious + 'B'
				for i in range(nGramSize-1): labelPrevious = labelPrevious + 'I'
		else:
			for i in range(nGramSize): labelPrevious = labelPrevious + 'O'
		return labelPrevious

	def shiftOp(self, labelFinal, labelPrevious, labelItem, index):
		#print  labelFinal[index-1:index]+ "[" + labelFinal[index:] + "] vs [" + labelItem + "] == " + labelFinal[:index] + "[" + self.andOperationIOB(labelFinal[index:], labelItem) + "]"

		labelPrevious = self.andOperationIOB(labelFinal[index:], labelItem)
		labelFinal = labelFinal[:index] + labelPrevious
		#print "-> " + labelFinal
		return labelFinal

	def classifyNgram(self, n, sentenceTokens):
		nGrams = self.nlp.extractNGrams(n, sentenceTokens)
		labelPrevious = ""	
		labelFinal = ""	
		for index, ngram in enumerate(nGrams):
			labelItem = ""
			if (self.rules.isThereRule(ngram) != None): 
				labelItem = self.getLabelGeo(labelItem, n, True)
			else: labelItem = self.getLabelGeo(labelItem, n, False) 
			
			if (index > 0):
				labelFinal = self.shiftOp(labelFinal, labelPrevious, labelItem,  index)
			else: 
				labelFinal = labelItem
				#print labelFinal
			
		#print "Final " + labelFinal + "\n\n"
		return labelFinal

	#def classifyBasedOnRules2(self, row):
	def rulesNER(self, row):
		labels = []
		labelItem = ""
		sentenceTokens = self.nlp.tokenization(row[1])
		#print row[1]
		#print "1 -------------------------------------------------------------"	
		#1-grams
		for token in sentenceTokens:
			if (self.rules.isThereRule(token) != None): 
				labelItem = self.getLabelGeo(labelItem, 1, True)
			else: labelItem = self.getLabelGeo(labelItem, 1, False)
		#print labelItem + "\n\n"
		labels.append(labelItem)		

		#print "2-------------------------------------------------------------"	
		# 2-grams
		labelFinal = self.classifyNgram(2, sentenceTokens)
		labels.append(labelFinal)
	
		#return
		#print "3-------------------------------------------------------------"	
		# 3-grams
		labelFinal = self.classifyNgram(3, sentenceTokens)
		labels.append(labelFinal)

		#print "4-------------------------------------------------------------"	
		# 4-grams
		labelFinal = self.classifyNgram(4, sentenceTokens)
		labels.append(labelFinal)

		#print "5-------------------------------------------------------------"	
		# 5-grams
		labelFinal = self.classifyNgram(5, sentenceTokens)
		labels.append(labelFinal)	
		
		#print "6-------------------------------------------------------------"				
		# 6-grams
		#labelFinal = self.classifyNgram(6, sentenceTokens)
		#labels.append(labelFinal)

		#print "-------------------------------------------------------------"				
		return labels
	
	#~ def classifyBasedOnRules(self, fileNameTest):
		#~ with open(fileNameTest, 'r', encoding='utf-8') as csvfile:
			#~ spamreader = csv.reader(csvfile, delimiter=',', quotechar='|')
			#~ for row in spamreader:
				#~ sentenceTokens = self.nlp.tokenization(row[1])
				#~ print ("1 -------------------------------------------------------------")	
				#~ #1-grams
				#~ for token in sentenceTokens:
					#~ if (self.isThereRule(token) != None): print (token)
				#~ 
				#~ print ("2-------------------------------------------------------------")	
				#~ # 2-grams
				#~ bigrams = self.nlp.extractNGrams(2, sentenceTokens)
				#~ for bigram in bigrams:
					#~ if (self.isThereRule(bigram) != None): print (bigram)
				#~ 
				#~ print ("3-------------------------------------------------------------")
				#~ # 3-grams
				#~ trigrams = self.nlp.extractNGrams(3, sentenceTokens)
				#~ for trigram in trigrams:
					#~ if (self.isThereRule(trigram) != None): print (trigram)
				#~ 
				#~ print ("4-------------------------------------------------------------")
				#~ # 4-grams
				#~ fourgrams = self.nlp.extractNGrams(4, sentenceTokens)
				#~ for fourgram in fourgrams:
					#~ if (self.isThereRule(fourgram) != None): print (fourgram)
				#~ 
				#~ print ("5-------------------------------------------------------------")
				#~ # 5-grams
				#~ fivegrams = self.nlp.extractNGrams(5, sentenceTokens)
				#~ for fivegram in fivegrams:
					#~ if (self.isThereRule(fivegram) != None): print (fivegram)	
				#~ print ("6-------------------------------------------------------------")			
				#~ # 6-grams
				#~ sixgrams = self.nlp.extractNGrams(6, sentenceTokens)
				#~ for sixgram in sixgrams:
					#~ if (self.isThereRule(sixgram) != None): print (sixgram)
				#~ 
				#~ print ("-------------------------------------------------------------")				
			#~ csvfile.close()
	#~ 
	def isGeographic(self, nGram):
		isGeo = False
	
		#print ("isGeo:: " +  " ".join(nGram))
		#Check dictionary
		matched = self.dictionary.verifyAddress(" ".join(nGram))
		if (matched != None): 
			isGeo = True
		
		#Check rules
		else:					
			support = self.rules.isThereRule(nGram)
			if (support != None and support > 80): 
				isGeo = True
						
		return isGeo
		
	#def bruteForceClassification(self, sentenceTokens):
	def dictionaryRulesNER(self, sentenceTokens):
		labelFinal = ""
		#
		#print "----------------------------------------"
		#print sentenceTokens
		#print "----------------------------------------"
		for index,token in enumerate(sentenceTokens):
			nGram = []
			nGram.append(token)
			labelPrevious = ""

			#If there is rule or dictionary entry
			if (self.isGeographic(nGram)): labelPrevious =self.getLabelGeo(labelPrevious, len(nGram), True)	
			else: labelPrevious = self.getLabelGeo(labelPrevious, len(nGram), False)
			
			#print(str(" ".join(nGram)) + " ::: " + labelPrevious)
			
			for idx in range(index+1, len(sentenceTokens)):
				labelIter = ""
				nGram.append(sentenceTokens[idx])
				#print nGram
				
				#if recognized
				if (self.isGeographic(nGram)): labelIter =self.getLabelGeo(labelIter, len(nGram), True)
				else: labelIter = self.getLabelGeo(labelIter, len(nGram), False)

				teste = labelPrevious
				labelPrevious = self.andOperationIOB(labelPrevious, labelIter)
				
				#print (str(" ".join(nGram)) +" <::> "+ labelIter + " and " + teste + " == " + labelPrevious)

			if (index > 0):
				#print labelFinal[:index] + "[" + labelFinal[index:] + "] and " + labelPrevious + " == " + labelFinal[:index] + "[" + self.andOperationIOB(labelFinal[index:], labelPrevious) + "]"
				labelPrevious = self.andOperationIOB(labelFinal[index:], labelPrevious)
				
				#lastLetter = labelFinal[:index][len(labelFinal[:index])-1]
				#print "teste: " + labelFinal[:index][len(labelFinal[:index])-1] + " vs " + 
				#if (lastLetter == labelPrevious[0]):
					
				#elif ((lastLetter == 'B' or lastLetter == 'I') and labelPrevious[0] == 'B'):
				#	letter = lastLetter
				#	while(letter != 'B')					
				
				labelFinal = labelFinal[:index] + labelPrevious
			else:				
				labelFinal = labelPrevious

			#print ("\n---------\nFinal: " + labelFinal)
			
			
		#print ("\n\n>>>>>Final: " + labelFinal)
		return labelFinal
		
	def evaluationMetrics(self, matrix):
		print ("="*80)
		print()
		print("TOTAL DE ENTIDADES: "+str(matrix.sum()))
	
		#accuracy = (TN + TP)/(TN+TP+FN+FP) = (Number of correct assessments)/Number of all assessments)
		if (matrix.sum() == 0):accuracy =0
		else :accuracy = matrix.diagonal().sum() / matrix.sum()
		print ("ACURÁCIA: \t\t\t%0.2f" % (accuracy))
		
		#Recall, r , is defined as the fraction of the class 
		#objects correctly classified.
		recall = []
		for i,row in  enumerate(matrix):
			if (row.sum() == 0): recall.append(0)
			else :recall.append(row[i]/(row.sum()))
		print ("RECALL POR LABEL(IOB) \t\t" +str(recall))
		#print (numpy.array(recall))
		
		#Precision, p, is defined as the fraction
		#of objects correctly classified from all objects attributed to the class.
		precision = []
		for i,row in  enumerate(matrix):
			column = matrix[:,i]
			if (column.sum() == 0):precision.append(0)
			else : precision.append(row[i]/(column.sum()))
		print ("PRECISION POR LABEL(IOB) \t" +str(precision))
		#print (numpy.array(precision))
		
		#F1
		#For a given class, c j , its F 1 score is mathematically defined as:
		#F1(c_j) = 2p_j *r_j/(p_j + r_j)
		f1 = []
		for i,row in  enumerate(matrix):
			if ((precision[i] + recall[i]) == 0): f1.append(0)
			else: f1.append((2*precision[i]*recall[i])/(precision[i] + recall[i]))
		print ("F1 POR LABEL(IOB) \t\t" +str(f1)+"\n")
		
		classes = ['I', 'O', 'B']
		print ("class\t\tPrecision\tRecall\tF1\tSupport")
		for i,row in enumerate(matrix):			
			print ("%s\t\t%0.2f\t\t%0.2f\t%0.2f\t%d" % (classes[i], precision[i], recall[i], f1[i], matrix[i,:].sum()))		
		print ("Média/Total\t%0.2f\t\t%0.2f\t%0.2f\t%d" %(numpy.array(precision).sum()/3, numpy.array(recall).sum()/3, numpy.array(f1).sum()/3, matrix.sum()))
	
		print ("="*80)
		print()
	
	def getAccuracyRules(self, accuraciesRules):
		m1 = self.getMatrix(self.cmRules0)
		if (m1.sum() == 0):accuracy =0
		else :accuracy = m1.diagonal().sum() / m1.sum()
		accuraciesRules[0].append(accuracy)
		
		m1 = self.getMatrix(self.cmRules1)
		if (m1.sum() == 0):accuracy =0
		else :accuracy = m1.diagonal().sum() / m1.sum()
		accuraciesRules[1].append(accuracy)
		
		m1 = self.getMatrix(self.cmRules2)
		if (m1.sum() == 0):accuracy =0
		else :accuracy = m1.diagonal().sum() / m1.sum()
		accuraciesRules[2].append(accuracy)
	
		m1 = self.getMatrix(self.cmRules3)
		if (m1.sum() == 0):accuracy =0
		else :accuracy = m1.diagonal().sum() / m1.sum()
		accuraciesRules[3].append(accuracy)
		
		m1 = self.getMatrix(self.cmRules4)
		if (m1.sum() == 0):accuracy =0
		else :accuracy = m1.diagonal().sum() / m1.sum()
		accuraciesRules[4].append(accuracy)		
		return accuraciesRules
		
	def plot_confusion_matrix(self, cm, title='Matriz de Confusão', cmap=plt.cm.Blues):
		target_names = ["I", "O", "B"]
		plt.imshow(cm, interpolation='nearest', cmap=cmap)
		plt.title(title)
		plt.colorbar()
		tick_marks = numpy.arange(len(target_names))
		plt.xticks(tick_marks, target_names, rotation=45)
		plt.yticks(tick_marks, target_names)
		plt.tight_layout()
		plt.ylabel('Label Correto')
		plt.xlabel('Label Predito')	
		
	def generateImageConfusionMatrix(self, matrix, name='CM_image.png'):
		plt.figure()
		self.plot_confusion_matrix(numpy.array(matrix))
		plt.savefig(name)
		
	def printAccuracies(self, accuraciesRules, accuraciesFB, accuraciesFBR):
		print("\n----------------------------------------------------------\n")
		print ("ACURÁCIA E DESVIO PADRÃO")
		print("\n----------------------------------------------------------\n")	
			
		print("DICTIONARY:: Accuracy Mean %0.2f \ Std %0.2f\n" %(numpy.array(accuraciesFB).mean(), numpy.array(accuraciesFB).std()))
		print(numpy.array(accuraciesFB))


		print("\n\nDICTIONARY AND RULES:: Accuracy Mean %0.2f \ Std %0.2f\n" %(numpy.array(accuraciesFBR).mean(), numpy.array(accuraciesFBR).std()))
		print(numpy.array(accuraciesFBR))
		
		for i,row in enumerate(accuraciesRules):
			v = numpy.array(row)			
			print("\nRULES %d-GRAM:: Accuracy Mean %0.2f \ Std %0.2f" %(i+1, v.mean(), v.std()))
			print("\n"+str(v))
		print()	
		
	def getMatrix(self, dictContagem):
		matrix = numpy.array([[0.0, 0.0, 0.0],[0.0, 0.0, 0.0],[0.0, 0.0, 0.0]])
		matrix[0][0] = dictContagem['II']
		matrix[0][1] = dictContagem['IO']
		matrix[0][2] = dictContagem['IB']
		               
		matrix[1][0] = dictContagem['OI']
		matrix[1][1] = dictContagem['OO']
		matrix[1][2] = dictContagem['OB']
		              
		matrix[2][0] = dictContagem['BI']
		matrix[2][1] = dictContagem['BO']
		matrix[2][2] = dictContagem['BB']
		return matrix	
	
	def evaluationCM(self):
		numpy.set_printoptions(precision=3)
		numpy.set_printoptions(suppress=True)
		
		#Normalize results
		cm = self.getMatrix(self.confusionMatrixFB)
		cmFBR = self.getMatrix(self.confusionMatrixFBR)
		cmR0 = self.getMatrix(self.cmRules0)
		cmR1 = self.getMatrix(self.cmRules1)
		cmR2 = self.getMatrix(self.cmRules2)
		cmR3 = self.getMatrix(self.cmRules3)
		cmR4 = self.getMatrix(self.cmRules4)	
		
		print("\n----------------------------------------------------------")
		print ("MATRIZES DE CONFUSÃO - SEM NORMALIZAÇÃO ")
		print("----------------------------------------------------------\n")
		
		print("\n::DICTIONARY::\n")
		print (cm.astype(int))
		
		print("\n\n::DICTIONARY AND RULES::\n")
		print (cmFBR.astype(int))
	
		print("\n\n::RULES::\n")
		print (cmR0.astype(int))
		print()
		print (cmR1.astype(int))
		print()
		print (cmR2.astype(int))
		print()
		print (cmR3.astype(int))
		print()
		print (cmR4.astype(int))
		print()
				
		#Geração de imagens de matrizes de confusão sem normalização			
		self.generateImageConfusionMatrix(cm, 'FB_CM.png')
		self.generateImageConfusionMatrix(cmFBR, 'FBR_CM.png')
		self.generateImageConfusionMatrix(cmR0, 'R0_CM.png')
		self.generateImageConfusionMatrix(cmR1, 'R1_CM.png')
		self.generateImageConfusionMatrix(cmR2, 'R2_CM.png')
		self.generateImageConfusionMatrix(cmR3, 'R3_CM.png')
		self.generateImageConfusionMatrix(cmR4, 'R4_CM.png')
		
		print("\n----------------------------------------------------------")
		print ("TABELAS DE AVALIAÇÃO ")
		print("----------------------------------------------------------\n")
		
		print("\n::DICTIONARY::\n")
		self.evaluationMetrics(cm)
		
		print("\n\n::DICTIONARY AND RULES::\n")
		self.evaluationMetrics(cmFBR)
		
		print("\n\n::RULES::\n")
		print("\n 1-GRAM\n")
		self.evaluationMetrics(cmR0)
		
		print("\n 2-GRAM\n")
		self.evaluationMetrics(cmR1)
		
		print("\n 3-GRAM\n")
		self.evaluationMetrics(cmR2)
		
		print("\n 4-GRAM\n")
		self.evaluationMetrics(cmR3)
		
		print("\n 5-GRAM\n")
		self.evaluationMetrics(cmR4)
		print()
		
		#Normalizacao pelo numero de amostras de cada classe
		for i in range(0, 3):
			rowSum = cm[i][0]+cm[i][1]+cm[i][2]
			rowSum1 = cmFBR[i][0]+cmFBR[i][1]+cmFBR[i][2]
			for j in range(0, 3):
				cm[i][j] = cm[i][j]/rowSum
				cmFBR[i][j] = cmFBR[i][j]/rowSum1
				
		#Normalizacao pelo numero de amostras de cada classe RULES
		for i in range(0, 3):
			rowSum0 = cmR0[i][0]+cmR0[i][1]+cmR0[i][2]
			rowSum1 = cmR1[i][0]+cmR1[i][1]+cmR1[i][2]
			rowSum2 = cmR2[i][0]+cmR2[i][1]+cmR2[i][2]
			rowSum3 = cmR3[i][0]+cmR3[i][1]+cmR3[i][2]
			rowSum4 = cmR4[i][0]+cmR4[i][1]+cmR4[i][2]

			for j in range(0, 3):
				if (rowSum0 != 0): cmR0[i][j] = cmR0[i][j]/rowSum0
				if (rowSum1 != 0): cmR1[i][j] = cmR1[i][j]/rowSum1
				if (rowSum2 != 0): cmR2[i][j] = cmR2[i][j]/rowSum2
				if (rowSum3 != 0): cmR3[i][j] = cmR3[i][j]/rowSum3
				if (rowSum4 != 0): cmR4[i][j] = cmR4[i][j]/rowSum4
						
		print("\n----------------------------------------------------------")
		print ("MATRIZES DE CONFUSÃO - NORMALIZADAS ")
		print("----------------------------------------------------------\n")
		
		print("\n::DICTIONARY::\n")
		print (cm)
		
		print("\n\n::DICTIONARY AND RULES::\n")
		print (cmFBR)
		
		print("\n\n::RULES::\n")
		
		print("\n 1-GRAM\n")
		print (cmR0)
		
		print("\n 2-GRAM\n")
		print (cmR1)
		
		print("\n 3-GRAM\n")
		print (cmR2)
		
		print("\n 4-GRAM\n")
		print (cmR3)
		
		print("\n 5-GRAM\n")
		print (cmR4)
		print ()
		
		#Geração de imagens de matrizes de confusao normalizadas
		self.generateImageConfusionMatrix(cm,   'FB_CM_Normalized.png' )
		self.generateImageConfusionMatrix(cmFBR,'FBR_CM_Normalized.png')
		self.generateImageConfusionMatrix(cmR0, 'R0_CM_Normalized.png')
		self.generateImageConfusionMatrix(cmR1, 'R1_CM_Normalized.png')
		self.generateImageConfusionMatrix(cmR2, 'R2_CM_Normalized.png')
		self.generateImageConfusionMatrix(cmR3, 'R3_CM_Normalized.png')
		self.generateImageConfusionMatrix(cmR4, 'R4_CM_Normalized.png')
	
	def generateCloudDicFile(self, sentenceTokens):
		entities = self.extractAnnotatedEntities(self.bruteForceLabel, sentenceTokens)
		for entity in entities: 
			self.entitiesByDicFile.writerow([entity])
	
	def generateCloudDicRulesFile(self, labelFBClass, sentenceTokens):
		entities = self.extractAnnotatedEntities(labelFBClass, sentenceTokens)
		for entity in entities: 
			self.entitiesByDicRulesFile.writerow([entity])
	
	def crossValidation(self, fileInstances, nFolds, gazetteer):
		foldsSize 	= self.totalNumInstances//nFolds
		lastFold 	= 0
		foldNum 	= 0
		foldsSet 	= []		
		fold 		= []
	
		self.posTagging = POStagging()
		
		#Dividindo as instancias em folds
		with open(fileInstances, 'r', encoding='utf-8') as csvfile:
			spamreader = csv.reader(csvfile, delimiter=',', quotechar='|')
			for i,row in enumerate(spamreader):
				if (foldNum != nFolds and (lastFold == 0 or lastFold >= foldsSize)):
					if (len(fold) > 0):	
						foldsSet.append(fold)
					fold = []
					lastFold = 0
					foldNum += 1							
				fold.append(row)
				lastFold += 1
			csvfile.close()		
		#Last fold
		foldsSet.append(fold)
		fold = []
		
		print ("\n\n> NÚMERO DE FOLDS:: " + str(len(foldsSet)))
		for index,item in enumerate(foldsSet): print (" %d Fold: %d instâncias" % (index, len(item)))	
		print()
		
		accuraciesFB = []
		accuraciesFBR = []
		accuraciesRules = [[], [], [], [], []]
		
		f1 = csv.writer(open("outputToCloud.csv", 'w', newline=''))
		
		# Write CSV Headers
		#f1.writerow(["Texto", "LabelFB", "label", "upperCaseTerms"])
		
		#CrossValidation tests for each fold
		for foldIndex in range(0, nFolds): 
			print("="*80)
			print("FOLD "+str(foldIndex))
			print("="*80)
			
			#Reset Dictionary(only gazzetteer entries) and Rules to its initial state	
			self.initiateModules(gazetteer)
			
			#Training with nFolds-1 folds except foldIndex (Dicionary and Rules using only some folds)
			self.trainingModules(foldsSet, nFolds, foldIndex)		
			#self.rules.rulesInfo()	
			
			#Testing with one of the folds			
			for row in foldsSet[foldIndex]:
				sentenceTokens = self.nlp.tokenization(row[1])
				annotatedLabel = row[0]
				preProcessedInforme = row[1]
				
				if(len(sentenceTokens) != len(annotatedLabel)): print (">>> ERRO: Tokens= " + str(len(sentenceTokens)) + " label= " + str(len(annotatedLabel))+" " + preProcessedInforme)
				
				#Dictionary strategy
				self.dictionaryNER(sentenceTokens)
				self.generateCloudDicFile(sentenceTokens)
				
				#Dictionary and Rules Strategy
				labelFBClass = self.dictionaryRulesNER(sentenceTokens)
				self.generateCloudDicRulesFile(labelFBClass, sentenceTokens)
				self.checkHitsRate(self.bruteForceLabel, annotatedLabel, labelFBClass, annotatedLabel)			
				
				#Rules strategy
				labels = self.rulesNER(row)
				
				#Excecao n-grama com n > que o num de tokens
				for i,label in enumerate(labels): 
					if (len(label) == 0): labels[i] = 'O'*len(sentenceTokens)
				
				f1.writerow([preProcessedInforme, self.bruteForceLabel, labelFBClass, labels[0], labels[1], labels[2], labels[3], labels[4], annotatedLabel])
				self.checkHitsRateRules(labels, annotatedLabel)				
			
			# Confusion Matrix Dictionary
			m1 = self.getMatrix(self.confusionMatrixFB)
			accuracy = m1.diagonal().sum() / m1.sum()
			accuraciesFB.append(accuracy)
			
			# Confusion Matrix Dictionary And Rules
			m2 = self.getMatrix(self.confusionMatrixFBR)
			accuracy = m2.diagonal().sum() / m2.sum()
			accuraciesFBR.append(accuracy)
			
			# Confusion Matrix Rules
			accuraciesRules = self.getAccuracyRules(accuraciesRules)

		#Printing Results
		print("\n----------------------------------------------------------")
		print ("CROSS-VALIDATION RESULTS ")
		print("----------------------------------------------------------\n")
		print ("[DICTIONARY] \t\t" + str(self.confusionMatrixFB))
		print ("[DICTIONARY + RULES] " + str(self.confusionMatrixFBR) + "\n")			
		
		#Accuracies and standart deviation
		self.printAccuracies(accuraciesRules, accuraciesFB, accuraciesFBR)
		self.evaluationCM()
		
		
	def testingFunctions(self, gazetteer):
		#informe = "Trânsito está lento na Raja sentido centro entre Josafá Belo e Contorno" #OOOOBOOOBIIB
		#informe = "Veículo com problema mecânico na Vilarinho com Edgar Torres Venda Nova sentido bairro" #OOOOOBOBIBIOO
		informe = "Trânsito bom na Avenida Pedro II no Elevado D Helena Greco na Avenida Bias Fortes na Praça Raul Soares" #OOOBIIOBIIIOBIIOBII		
		
		tokens = informe.split(" ")	
		self.posTagging = POStagging()
		self.initiateModules(gazetteer)
		
		#Add to dictionary test
		#self.dictionary.addToDictionary("Elevado do Ponteio")
		#self.dictionary.addToDictionary("Praça D Helena Greco")
		#self.dictionary.addToDictionary("Praça Sete")
		#self.dictionary.addToDictionary("Praça do Independencia")
		#print(self.dictionary.verifyAddress("Belo e"))
		
		print (tokens)
		#self.dictionaryNER(tokens)
		self.dictionaryRulesNER(tokens)	

	#~ def testingRules(self, gazetteer):
		#~ self.nlp = NLP()
		#~ posTagging = POStagging()
		#~ self.rules = Rules(posTagging)
		#~ self.rules.generateRules()
		#~ self.extractAnnotatedEntities("OOOBIOBIIIOB", "Trânsito intenso na Pedro II com Viaduto Dona Helena Greco sentido bairro".split(" "))
		#~ self.rules.testingRules()
		
	
	#~ def findPattern(self, nGram):
		#~ #get uppercase pattern
		#~ uppercasePattern = self.getUppercasePattern(nGram)
		#~ #uppercasePattern = 0
#~ 
		#~ #get geo evicence terms
		#~ geoEvidencePattern = self.nlp.extractGeoEvidenceTokens(nGram)
#~ 
		#~ #get pos-tagging pattern
		#~ postaggingPattern = self.posTagging.posTaggingNGram(nGram)
		#~ 
		#~ uppercaseRule = {'u': uppercasePattern}			
		#~ posTaggingRule = {'p': [item[1] for item in postaggingPattern]}
		#~ if (len(geoEvidencePattern) > 0 ): geoEvidenceRule = {'g': geoEvidencePattern}
		#~ else: geoEvidenceRule = None
		#~ 
		#~ return (uppercaseRule, geoEvidenceRule, posTaggingRule)
		#~ 
	#~ def getUppercasePattern(self, nGram):
		#~ pattern = ""
		#~ cont = 0
		#~ #print nGram
		#~ for token in nGram:
			#~ if (token[0].isupper()):
				#~ pattern = pattern+'S'
				#~ cont += 1
			#~ else:
				#~ pattern = pattern+'N'
		#~ 
		#~ #return pattern
		#~ #print cont/len(nGram)
		#~ return cont/len(nGram)
	#~ 
	#~ def generateRules(self):		
		#~ #C1 candidates
		#~ c1_uppercase = {}
		#~ c1_geoEvidence = {}
		#~ c1_posTagging = {}
		#~ for ngram in self.rules.getNGramsSet():
			#~ uppercaseRule, geoEvidenceRule, posTaggingRule = self.findPattern(ngram[0])
			#~ 
			#~ #uppercaseRule = {'u': uppercasePattern}
			#~ #geoEvidenceRule = {'g': geoEvidencePattern}
			#~ #posTaggingRule = {'p': [item[1] for item in postaggingPattern]}
			#~ 
			#~ #add C1 candidates with support						
			#~ if (not (str(uppercaseRule) in c1_uppercase)): c1_uppercase[str(uppercaseRule)] = 1
			#~ else: c1_uppercase[str(uppercaseRule)] += 1				
			#~ 
			#~ if ((geoEvidenceRule != None) and not (str(geoEvidenceRule) in c1_geoEvidence)): c1_geoEvidence[str(geoEvidenceRule)] = 1
			#~ elif (geoEvidenceRule != None): c1_geoEvidence[str(geoEvidenceRule)] += 1				
			#~ 
			#~ if (not (str(posTaggingRule) in c1_posTagging)): c1_posTagging[str(posTaggingRule)] = 1
			#~ else: c1_posTagging[str(posTaggingRule)] += 1
			#~ 
		#~ #print c1
		#~ #print sorted(c1, key=itemgetter(1))
		#~ #eval (str(geoEvidencePattern)) == array
		#~ #print "\n\nSOrted\n"
		#~ sortedC1_uppercase = sorted(c1_uppercase.items(), key=operator.itemgetter(1), reverse=True)
		#~ sortedC1_geoEvidence = sorted(c1_geoEvidence.items(), key=operator.itemgetter(1), reverse=True)
		#~ sortedC1_posTagging = sorted(c1_posTagging.items(), key=operator.itemgetter(1), reverse=True)
		#~ 
		#~ #print sortedC1_uppercase
		#~ #print "\n\n"
		#~ #print sortedC1_posTagging
		#~ #print "\n\n"
		#~ #print sortedC1_geoEvidence
		#~ #print "\n\n"
		#~ 
		#~ #print "\n\ntestes\n"
		#~ #print self.posTagging.posTaggingNGram(self.nlp.extractNGramas2("na avenida Rio"))
		#~ #print self.posTagging.posTaggingNGram(self.nlp.extractNGramas2("Em Belo Horizonte na cidade aberta perto do viaduto"))
		#~ #print self.posTagging.posTaggingNGram(self.nlp.extractNGramas2("Rua Nossa Senhora do Carmo"))
		#~ #print self.posTagging.posTaggingNGram(self.nlp.extractNGramas2("no rio perto de casa"))
		#~ #print self.posTagging.posTaggingNGram(self.nlp.extractNGramas2("trânsito lento na direção da avenida Brasil"))
		#~ 
		#~ #Poda
		#~ l1_frequent = {}
		#~ for uppercaseCandidateRule in sortedC1_uppercase:
			#~ 
			#~ #print uppercaseCandidateRule[0]
			#~ #print eval(uppercaseCandidateRule[0])
			#~ 
			#~ if (uppercaseCandidateRule[1] > self.minSupportValue):						
				#~ for posTaggingCandidateRule in sortedC1_posTagging:
					#~ if (posTaggingCandidateRule[1] > self.minSupportValue):
						#~ #Uppercase + POS-tagging						
						#~ rulePattern = str([eval(uppercaseCandidateRule[0]), eval(posTaggingCandidateRule[0])])
						#~ l1_frequent[rulePattern] = 0
						#~ for geoEvidenceCandidateRule in sortedC1_geoEvidence:
							#~ if (geoEvidenceCandidateRule[1] > self.minSupportValue):
								#~ #Uppercase + POS-tagging + GeoEvidence terms					
								#~ rulePattern = str([eval(uppercaseCandidateRule[0]), eval(posTaggingCandidateRule[0]), eval(geoEvidenceCandidateRule[0])])
								#~ #print rulePattern
								#~ l1_frequent[rulePattern] = 0
							#~ else: break
					#~ 
					#~ else: break				
			#~ else: break
#~ 
#~ 
		#~ #scan again - count support
		#~ for ngram in self.rules.getNGramsSet():
			#~ uppercaseRule, geoEvidenceRule, posTaggingRule = self.findPattern(ngram[0])
			#~ 
			#~ #uppercaseRule = {'u': uppercasePattern}			
			#~ #posTaggingRule = {'p': [item[1] for item in postaggingPattern]}
			#~ #geoEvidenceRule = {'g': geoEvidencePattern}
			#~ 
			#~ if (geoEvidenceRule != None): 
				#~ rulePattern = str([uppercaseRule, posTaggingRule, geoEvidenceRule])
			#~ else:
				#~ rulePattern = str([uppercaseRule, posTaggingRule])
				#~ 
			#~ #print rulePattern			
			#~ if (rulePattern in l1_frequent): l1_frequent[rulePattern] += 1
			#~ 
		#~ sortedL1 = sorted(l1_frequent.items(), key=operator.itemgetter(1), reverse=False)
		#~ #print sortedL1
		#~ 
		#~ #poda e adição das regras finais encontradas
		#~ for rule in sortedL1:
			#~ if (rule[1] > self.minSupportValue): self.rules.addRule(rule[0], rule[1])
		#~ print ("\nRules Num: " + str(self.rules.rulesNum()) + "\n\n")
#~ 
	#~ def isThereRule(self, nGram):
		#~ uppercaseRule, geoEvidenceRule, posTaggingRule = self.findPattern(nGram)
		#~ 
		#~ if (geoEvidenceRule != None): 
			#~ rulePattern = str([uppercaseRule, posTaggingRule, geoEvidenceRule])
		#~ else:
			#~ rulePattern = str([uppercaseRule, posTaggingRule])
		#~ 
		#~ support = self.rules.selectRule(rulePattern)
		#~ return support
		
	#~ #------------------------------------------------------
	#~ # Function countNGramasRecognized
	#~ # @params:
	#~ # @author: Monica
	#~ #------------------------------------------------------
	#~ def countNGramasRecognizedTeste(self, sentenceTokens):
		#~ #print "<N-GRAMAS Module>"
		#~ termsRecognized = 0
		#~ results = []
		#~ # Search 1-grams in the dicionary
		#~ ngramsGeoList = []
		#~ print ("GEO Evidence: ")
		#~ for grams in sentenceTokens:
			#~ #if (self.nlp.hasGeoEvidenceToken(self.nlp.tokenEncodeDecode(grams))):
			#~ if (self.nlp.hasGeoEvidenceToken(grams)):
				#~ ngramsGeoList.append(grams)
				#~ #print "".join(grams)
				#~ #print grams
#~ 
		#~ results.append(sentenceTokens)
		#~ results.append(ngramsGeoList)
#~ 
#~ 
		#~ #print results
#~ 
		#~ for nGram in results[0]:
			#~ matched = self.dictionary.verifyAddress(nGram)
			#~ if (matched != None and len(matched) >= 1): termsRecognized = termsRecognized + 1
#~ 
			#~ #print nGram
			#~ #print matched
			#~ print ("N-GRAM '" + nGram + "' MATCHES " + str(matched))
#~ 
		#~ self.uniGramsRecognized=termsRecognized
#~ 
		#~ print ("--------")
		#~ print ("1-GRAMS RECOGNIZED: " + str(termsRecognized))
		#~ print ("--------")
		#~ termsRecognized = 0
#~ 
		#~ #print results[1]
#~ 
		#~ # n-gramas with geo evidence
		#~ for nGram in results[1]:
			#~ matched = self.dictionary.verifyAddress(nGram)
			#~ if (matched != None and len(matched) >= 1): termsRecognized = termsRecognized + 1
#~ 
			#~ #print nGram
			#~ #print matched
			#~ print ("N-GRAM '" + nGram + "' MATCHES " + str(matched))
#~ 
		#~ self.uniGramsGeoRefs=termsRecognized
#~ 
		#~ print ("--------")
		#~ print ("[GEO] 1-GRAMS RECOGNIZED: " + str(termsRecognized))
		#~ print ("--------")
		#~ termsRecognized = 0
#~ 
		#~ # Search 2-grams in the dicionary
		#~ results = self.nlp.extractNGramas(2, sentenceTokens)
		#~ #print results
		#~ for nGram in results[0]:
			#~ matched = self.dictionary.verifyAddress(nGram)
			#~ if (matched != None and len(matched) >= 1): termsRecognized = termsRecognized + 1
#~ 
			#~ #print nGram
			#~ #print matched
			#~ print ("N-GRAM '" + nGram + "' MATCHES " + str(matched))
#~ 
		#~ self.biGramsRecognized=termsRecognized
#~ 
		#~ print ("--------")
		#~ print ("2-GRAMS RECOGNIZED: " + str(termsRecognized))
		#~ print ("--------")
		#~ termsRecognized = 0
#~ 
		#~ #print results[1]
		#~ # n-gramas with geo evidence
		#~ for nGram in results[1]:
			#~ matched = self.dictionary.verifyAddress(nGram)
			#~ if (matched != None and len(matched) >= 1): termsRecognized = termsRecognized + 1
#~ 
			#~ #print nGram
			#~ #print matched
			#~ print ("N-GRAM '" + nGram + "' MATCHES " + str(matched))
#~ 
		#~ self.biGramsGeoRefs=termsRecognized
#~ 
		#~ print ("--------")
		#~ print ("[GEO] 2-GRAMS RECOGNIZED: " + str(termsRecognized))
		#~ print ("--------")
		#~ termsRecognized = 0
#~ 
		#~ # Search 3-grams in the dicionary
		#~ results = self.nlp.extractNGramas(3, sentenceTokens)
		#~ #print results
		#~ for nGram in results[0]:
			#~ matched = self.dictionary.verifyAddress(nGram)
			#~ if (matched != None and len(matched) >= 1): termsRecognized = termsRecognized + 1
#~ 
			#~ #print nGram
			#~ #print matched
			#~ print ("N-GRAM '" + nGram + "' MATCHES " + str(matched))
#~ 
		#~ self.triGramsRecognized=termsRecognized
#~ 
		#~ print ("--------")
		#~ print ("3-GRAMS RECOGNIZED: " + str(termsRecognized))
		#~ print ("--------")
		#~ termsRecognized = 0
#~ 
		#~ # n-gramas with geo evidence
		#~ for nGram in results[1]:
			#~ matched = self.dictionary.verifyAddress(nGram)
			#~ if (matched != None and len(matched) >= 1): termsRecognized = termsRecognized + 1
#~ 
			#~ #print nGram
			#~ #print matched
			#~ print ("N-GRAM '" + nGram + "' MATCHES " + str(matched))
#~ 
		#~ self.triGramsGeoRefs=termsRecognized
#~ 
		#~ print ("--------")
		#~ print ("[GEO] 3-GRAMS RECOGNIZED: " + str(termsRecognized))
		#~ print ("--------")
		#~ termsRecognized = 0

	
	#~ def countNGramasRecognized(self, sentenceTokens):
		#~ #print "<N-GRAMAS Module>"
		#~ termsRecognized = 0
		#~ results = []
		#~ # Search 1-grams in the dicionary
		#~ ngramsGeoList = []
		#~ for grams in sentenceTokens:
			#~ if (self.nlp.hasGeoEvidenceToken(grams)):
				#~ ngramsGeoList.append(grams)
		#~ results.append(sentenceTokens)
		#~ results.append(ngramsGeoList)
#~ 
		#~ for nGram in results[0]:
			#~ matched = self.dictionary.verifyAddress(nGram)
			#~ if (matched != None and len(matched) >= 1): termsRecognized = termsRecognized + 1
		#~ self.uniGramsRecognized=termsRecognized
		#~ termsRecognized = 0
#~ 
		#~ # n-gramas with geo evidence
		#~ for nGram in results[1]:
			#~ matched = self.dictionary.verifyAddress(nGram)
			#~ if (matched != None and len(matched) >= 1): termsRecognized = termsRecognized + 1
		#~ self.uniGramsGeoRefs=termsRecognized
		#~ termsRecognized = 0
#~ 
		#~ # Search 2-grams in the dicionary
		#~ results = self.nlp.extractNGramas(2, sentenceTokens)
		#~ for nGram in results[0]:
			#~ matched = self.dictionary.verifyAddress(nGram)
			#~ if (matched != None and len(matched) >= 1): termsRecognized = termsRecognized + 1
		#~ self.biGramsRecognized=termsRecognized
		#~ termsRecognized = 0
#~ 
		#~ # n-gramas with geo evidence
		#~ for nGram in results[1]:
			#~ matched = self.dictionary.verifyAddress(nGram)
			#~ if (matched != None and len(matched) >= 1): termsRecognized = termsRecognized + 1
		#~ self.biGramsGeoRefs=termsRecognized
		#~ termsRecognized = 0
#~ 
		#~ # Search 3-grams in the dicionary
		#~ results = self.nlp.extractNGramas(3, sentenceTokens)
		#~ for nGram in results[0]:
			#~ matched = self.dictionary.verifyAddress(nGram)
			#~ if (matched != None and len(matched) >= 1): termsRecognized = termsRecognized + 1
		#~ self.triGramsRecognized=termsRecognized
		#~ termsRecognized = 0
#~ 
		#~ # n-gramas with geo evidence
		#~ for nGram in results[1]:
			#~ matched = self.dictionary.verifyAddress(nGram)
			#~ if (matched != None and len(matched) >= 1): termsRecognized = termsRecognized + 1
		#~ self.triGramsGeoRefs=termsRecognized
		#~ termsRecognized = 0


		