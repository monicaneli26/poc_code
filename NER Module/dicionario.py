#!/usr/bin/python
# -*- coding: utf-8 -*-
import Levenshtein
import unicodedata
import nltk.corpus
import string
import re
import googlemaps
from datetime import datetime
import csv

class Dictionary:
	
	#attributes
	dicionario = {}
	sortedKeys = None
	stopwords = None
	gmaps = None
	keyWordsEncoded = ['avenida', 'rua', 'bairro', 'rodovia', 'elevado', 'praca', 'praça']
	
	#parameters
	matchesLimitNumber = 3
	distanciaMax = 1
	minimumTermsNumber = 2
	minumumUppercaseTerms = 0.5
	minumumStopWordsTerms = 0.5
	minimumWordSize = 4
	
	def __init__(self, gazetteer):
		
		#Get Coordinates
		#self.getDictionaryCoordinates(streetsFile)
		#self.getDictionaryCoordinates(regionsFile)
		#return
		
		self.dicionario = {}
		self.sortedKeys = None
		self.stopwords = None
		self.stopwords = nltk.corpus.stopwords.words('portuguese')
		
		self.readGazetteer(gazetteer)					# Gazetteer.csv	
		self.dictionaryInfo()		

	#------------------------------------------------------
	# Function
	# @params:
	# @author: Monica
	#------------------------------------------------------
	def dictionaryInfo(self):
		vals = 0
		self.sortedKeys = sorted(self.dicionario)
		print ("\n---------DICTIONARY PRE-PROCESSING--------")
		print ("Dictionary keys: " + str(len(self.sortedKeys)))
		for key in self.sortedKeys:
			vals = vals + len(self.dicionario[key])
		print ("Dictionary values: " + str(vals))
		print ("-----------------------------------------------\n")

	#------------------------------------------------------
	# Function extractKey
	# @params: address
	# @author: Monica
	#------------------------------------------------------
	def extractKey(self, address):
		words = str(address).split(' ')
		key = ''
		for word in words: key = key + word[0]
		return key


	def geocoding(self, address):
		# Geocoding and address
		geocode_result = self.gmaps.geocode(address + ", Belo Horizonte, Minas Gerais, Brazil")
		
		if (len(geocode_result) > 0):
			location = geocode_result[0]['geometry']['location']
			latitude, longitude = location['lat'],location['lng']
		else:
			latitude, longitude = -1,-1
		# Look up an address with reverse geocoding
		#reverse_geocode_result = self.gmaps.reverse_geocode((40.714224, -73.961452))

		return (latitude, longitude)

	#------------------------------------------------------
	# Function addToDictionary and get the coordinates
	# @params: key, address
	# @author: Monica
	#------------------------------------------------------
	def addToDictionary(self, address):
		#Remover termos de categoria de endereço
		address = self.removeAddressCategory(address)
		
		#TODO: get coordinates
		coordinates = {"lat": -1, "lng": -1}
		
		#print ("Address result:: " + str(address))
		if (address != "" and address != " "):
			#print ("Address result1:: >" + str(address)+"<")	
			
			#Starts with stopwords
			addressTokens = address.split(" ")
			if (addressTokens[0] in self.stopwords):
				for index,word in enumerate(addressTokens):
					if (not word in self.stopwords):				
						newVersionOfAddress = addressTokens[index: len(addressTokens)]
						self.addToDictionary(" ".join(newVersionOfAddress))
						break
			
			#Extract the entry key
			key = self.extractKey(address)
			
			#print("Add To Dict:: " + str([key, address]) + "\n")
			
			if (not(key in self.dicionario)):
				self.dicionario[key] = []
				
				#Add entry (address
				self.dicionario[key].append([address, coordinates])
			else:
				if (not (address in self.dicionario[key])):
					self.dicionario[key].append([address, coordinates])
			
	#------------------------------------------------------
	# Function addToDictionary
	# @params: key, address
	# @author: Monica
	#------------------------------------------------------
	def addToGazetteer(self, address, coords):
		#Remover termos de categoria de endereço
		address = self.removeAddressCategory(address)
		
		#print ("Address result:: " + str(address))
		if (address != "" and address != " "):
		
			#Extract the entry key
			key = self.extractKey(address)
			
			if (not(key in self.dicionario)):
				self.dicionario[key] = []
				
				#Add entry (address
				self.dicionario[key].append([address, coords])
			else:
				if (not (address in self.dicionario[key])):
					self.dicionario[key].append([address, coords])			
				
	#------------------------------------------------------
	# Function sortedDictValues3
	# @params: dicionario
	# @author: http://code.activestate.com/recipes/52306-to-sort-a-dictionary/
	#------------------------------------------------------
	#~ def sortedDictValues3(self, adict):
		#~ keys = adict.keys()
		#~ keys.sort()
		#~ return map(adict.get, keys)

	#------------------------------------------------------
	# Function readDictionary
	# @params: fileName
	# @author: Monica
	#------------------------------------------------------
	#~ def readDictionary(self, fileName):
		#~ f = open(fileName, 'r')
#~ 
		#~ for line in f:
			#~ address = line
			#~ address = address.replace('\n', "")
			#~ 
			#~ if (len(address) == 2):
				#~ address2= str(address[0] + " " + address[1])
				#~ key = self.extractKey(address2)
				#~ self.addToDictionary(key, address2)
#~ 
			#~ key = self.extractKey(address)
			#~ self.addToDictionary(key, address)
#~ 
		#~ f.close()

	#------------------------------------------------------
	# Function readDictionary
	# @params: fileName
	# @author: Monica
	#------------------------------------------------------
	def readGazetteer(self, fileName):
		f = open(fileName, 'r')

		for line in f:
			entry = line
			entry = entry.replace('\n', "")
			entry = entry.split(",")
			#print (entry)
			
			address= str(entry[0])
			coords = {"lat": float(entry[1]), "lng": float(entry[2])}			
			self.addToGazetteer( address, coords)

		f.close()

	#------------------------------------------------------
	# Function readDictionary e obtem coordenadas
	# @params: fileName
	# @author: Monica
	#------------------------------------------------------
	def getDictionaryCoordinates(self, fileName):
		
		# Replace the API key below with a valid API key.
		self.gmaps = googlemaps.Client(key='AIzaSyDXIEeHCDshtG3NX7Ky88xqE6oawiuLCLE')
		
		f = open(fileName, 'r')
		f1 = csv.writer(open("gazetteer.csv", 'w', newline=''))
		
		# Write CSV Headers
		f1.writerow(["Address", "lat", "lgn"])

		for line in f:
			address = line
			address = address.replace('\n', "")
			print(address)
			
			#Get the address coordinates
			coordinates = self.geocoding(address)
			f1.writerow([address, coordinates[0], coordinates[1]])

		f.close()

	#------------------------------------------------------
	# Function getDistance
	# Description: returns the entries matched and the indices of distances 
	# @params:
	# @author: Monica
	#------------------------------------------------------
	def getDistance(self, addressItem, dicionaryEntry, addressesMatched):
		indices = []
		
		#print ("OBJ:: "+str(dicionaryEntry))
		
		#Original version of dictionary entry
		if (type(dicionaryEntry[1]) is not list):
			address = dicionaryEntry[0]
			addressEntry = dicionaryEntry[0]
			coordinate = dicionaryEntry[1]			
		else:
			#Abreviated version of dictionary entry
			address = dicionaryEntry[0]
		
			#Original Dictionary entry
			addressEntry = dicionaryEntry[1][0]
			coordinate = dicionaryEntry[1][1]
		
				
		distancia = Levenshtein.distance(str(addressItem), str(address))
		#print(addressItem + " <> " + address + " ==  " + str(distancia))
		
		if (distancia >= 0 and distancia <= self.distanciaMax):
			if (not distancia in addressesMatched):
				addressesMatched[distancia] = []

			#Adiciona a entrada do dicionário que casou
			if (not address in addressesMatched[distancia]):
				indices.append(distancia)
				indices.append(len(addressesMatched[distancia]))
				#print (coordinate)
				#if (len(coordinate) == 2 and not str(coordinate[0]).isnumeric()):
				#	address = coordinate[0]
				#	coordinate = coordinate[1]
				addressesMatched[distancia].append([addressEntry, coordinate])
				
				#print ("Entry::" + str(dicionaryEntry))
				#print ("Distance::: "+str(addressesMatched))
				#print ("address::: "+str(address))
				#print ("coords::: "+str(coordinate))

		return indices


	#------------------------------------------------------
	# Function
	# @params:
	# @author: Monica
	#------------------------------------------------------
	def verifyCompleteAddress(self, key, addressToVerify, addressesMatched):
		#vetor de distancias matched
		indices = []
		matched = False
		
		#Verificando endereço sem abreviações
		if (key in self.dicionario):
			#casamento de cadeias
			for dicionaryEntry in self.dicionario[key]:
				indices = self.getDistance(addressToVerify, dicionaryEntry, addressesMatched)
				if (len(indices) > 0):
					matched = True
					if (len(addressesMatched) >= self.matchesLimitNumber): break
		return matched	
		
	#------------------------------------------------------
	# Function
	# @params:
	# @author: Monica
	#------------------------------------------------------
	def verifyAbbreviatedAddress(self, key, addressToVerify, addressesMatched):
		#vetor de distancias matched
		indices = []
		matched = False
		isAbbreviated = False
		#print ('Key: ' + key)
		
		words = str(addressToVerify).split(" ")
		for word in words:
			if (len(word) == 1): isAbbreviated = True
		if(not isAbbreviated): return matched
			
		
		#casamento de cadeias
		for dicionaryEntry in self.dicionario[key]:			
			addressEntry = dicionaryEntry[0]
			coordinate = dicionaryEntry[1]
			
			words = str(addressToVerify).split(" ")
			dicionarioAddress = str(addressEntry).split(" ")
			abbreviatedAddressEntry = addressEntry

			#Abreviando o Endereço do Dicionário de acordo com o padrão
			i = 0
			for word in words:
				if (len(word) == 1):
						abbreviatedAddressEntry = str(abbreviatedAddressEntry).replace(dicionarioAddress[i], dicionarioAddress[i][0])
				i = i + 1			
			
			#print ("--Abreviated: " + abbreviatedAddressEntry)
			indices = self.getDistance(addressToVerify, [abbreviatedAddressEntry, [addressEntry, coordinate]], addressesMatched)
			if (len(indices) > 0):
				matched = True
				distance = indices[0]
				numberOfMatches = indices[1]				
				addressesMatched[distance][numberOfMatches] = dicionaryEntry
				if (len(addressesMatched) >= self.matchesLimitNumber): break
		
		return matched

	#------------------------------------------------------
	# Function
	# @params:
	# @author: Monica
	#------------------------------------------------------
	def verifyApproximatedAddress(self, key, addressToVerify, addressesMatched):
		#vetor de distancias matched
		indices = []
		matched = False
		increment = True
		
		#Verificando endereço sem abreviações
		for keyEntry in self.dicionario.keys():
			if (keyEntry[0] == key):			
				#casamento de cadeias
				for dicionaryEntry in self.dicionario[keyEntry]:
					addressEntry = dicionaryEntry[0]
					coordinate = dicionaryEntry[1]
					#print ("Entry2 COord::" + str(coordinate))

					words = str(addressToVerify).split(" ")
					dicionarioAddress = str(addressEntry).split(" ")
					abbreviatedAddressEntry = addressEntry 
					
					if (len(words) >= len(dicionarioAddress)):
						expression0 = words
						expression1 = dicionarioAddress
					else:
						expression0 = dicionarioAddress
						expression1 = words
									
					#Abreviando o Endereço do Dicionário de acordo com o padrão
					i = 0
					for term in expression0:
						increment = True
						if (i < len(expression1)):
							#abreviar
							if (expression1[i][0] == term[0] and len(expression1[i]) != len(term)):
								abbreviatedAddressEntry = str(abbreviatedAddressEntry).replace(dicionarioAddress[i], dicionarioAddress[i][0])
							
							#Remove middle word
							elif (expression1[i][0] != term[0] or Levenshtein.distance(str(expression1[i]), str(term)) > 0):
								#print (expression1[i][0] + " === " + term[0])
								abbreviatedAddressEntry = str(abbreviatedAddressEntry).replace(dicionarioAddress[i], "")
								increment = False
							
							#print (expression1[i] + " vs  "+ term)
							#if (addressEntry == 'NOSSA SENHORA DO CARMO'): print ("Abreviated ::: " + abbreviatedAddressEntry)
							#print(term[0] + " vs " +expression1[i][0] + " dist: " + str(Levenshtein.distance(str(term), str(expression1[i]))) )
						if (increment): i = i + 1
					
					abbreviatedAddressEntry = re.sub('\s+', ' ', abbreviatedAddressEntry).strip()
					#if (addressEntry == 'NOSSA SENHORA DO CARMO'): print ("Abreviated <:::> " + abbreviatedAddressEntry)
					#print ("\nAbbrevi: "+str(abbreviatedAddressEntry))
					#print ("addressEntry: "+str(addressEntry))
					#print ("coordinate: "+str(coordinate))
					#print ("entry: "+str(dicionaryEntry))
					indices = self.getDistance(addressToVerify, [abbreviatedAddressEntry, [addressEntry, coordinate]], addressesMatched)
					if (len(indices) > 0):
						matched = True
						#if (len(addressesMatched) >= self.matchesLimitNumber): break
		return matched			
		
		
	#------------------------------------------------------
	# Function
	# @params:
	# @author: Monica
	#------------------------------------------------------
	def verifyAddressPart(self, key, addressToVerify, addressesMatched):
		indices = []
		matched = False
		results = {}
		distancia = 0
		
		#Obtendo a lista de endereços que começam com a palavra (endereço a ser verificado)
		for keyEntry in self.dicionario.keys():
			if (keyEntry[0] == key):
				
				for dicEntry in self.dicionario[keyEntry]:
					addressEntry = dicEntry[0]
					#coordinate = dicEntry[1]
					#print ("Entry2 COord::" + str(coordinate))

					words = str(addressToVerify).split(" ")
					if (words[0] != " " and words[0] != ""):
						
						#Substring matched
						position = addressEntry.find(words[0])
						if (position != -1):
								
							#Ordenado pela menor distancia de edição	
							distancia = Levenshtein.distance(str(addressEntry), str(addressToVerify))							
							if (not distancia in results): results[distancia] = []
											
							#Adiciona a entrada do dicionário que casou
							if (not addressToVerify in results[distancia]):
								indices.append(distancia)														
								results[distancia].append(dicEntry)								
								#addressesMatched[position].append(dicEntry)
								#indices.append(len(addressesMatched[position]))
											
					else: print ("Erro: empty string\n")
				
		if (len(indices) > 0):
			matched = True
			addressesMatched[0] = []
			
			for tupla in list(results.values()):
				for element in tupla:
					addressesMatched[0].append(element)
			#distance = indices[0]
			#numberOfMatches = indices[1]				
			#addressesMatched[distance][numberOfMatches] = dicionaryEntry
			#if (len(addressesMatched) >= self.matchesLimitNumber): break	
			
		return matched
	
	#------------------------------------------------------
	# Function
	# @params:
	# @author: Monica
	#------------------------------------------------------
	def verifyTermsPresence(self, key, addressToVerify, addressesMatched):
		keyWords = ['avenida', 'rua', 'bairro', 'rodovia']
		termsToSearchFor = []
		results = {}
		indices = []
		matched = False
		
		#Select the relevant terms to search for
		for word in addressToVerify.split(" "):
			if (not word.lower() in self.stopwords and not word.lower() in keyWords and len(word) > self.minimumWordSize): 
				termsToSearchFor.append(word)
	
		#Select all entries with one of the relevant terms on it		
		for keyEntry in self.dicionario.keys():
			
			for termItem in termsToSearchFor:
				
				#Obtendo a lista de endereços que possam ter o termo através da hash key gerada pelas primeiras letras do ender
				if (termItem[0] in keyEntry):
					
					for dicEntry in self.dicionario[keyEntry]:
						addressEntry = dicEntry[0]
						
						#Substring matched
						position = addressEntry.find(termItem)
						if (position != -1 ):
							
							#Ordenado pela distancia de edição entre os endereços
							distancia = Levenshtein.distance(str(addressEntry), str(addressToVerify))
							if (not distancia in results): results[distancia] = []
							
							#Adiciona a entrada do dicionário que casou
							if (not addressToVerify in results[distancia]):
								indices.append(distancia)														
								results[distancia].append(dicEntry)
		
		#Gerando lista de resultados
		if (len(indices) > 0):
				matched = True
				addressesMatched[0] = []
				
				for tupla in list(results.values()):
					for element in tupla:
						addressesMatched[0].append(element)
				
		return matched
	

	def addressTermsFilter(self, address):
		words = address.split(" ")
		newAddress = []	
		
		#Avoid evaluating numbers
		if (not words[0].isnumeric()):
			
			#Do not evaluate only stopwords
			onlyStopWords = True
			for word in words:
				if (not word in self.stopwords): onlyStopWords = False
			
			if (not onlyStopWords):
							
				#print("words::: "+str(words) + "\n")
				#Termos geográficos
				#keyWords = ['avenida', 'rua', 'bairro', 'praça', 'praca', 'trincheira', 'rodovia', 'via', 'estrada']
				#keyWordsEncoded = ['avenida', 'rua', 'bairro', 'rodovia']
				
				#keyWordsEncoded = ['avenida', 'rua', 'bairro', 'rodovia', 'elevado', 'praca', 'praça']
				
				#keyWordsEncoded = []
				#for key in keyWords: keyWordsEncoded.append(key.decode('utf8'))
				#for key in keyWords: keyWordsEncoded.append(key)		
				#print (keyWordsEncoded)
				#print ("Original address: >" + address +"<")

				#Removing address category terms
				for index, token in enumerate(words):
					if (token != "" and token != " "): 
						if (token.lower() not in self.keyWordsEncoded): 
							newAddress = words[index: len(words)]
							break
					
		#print (	newAddress)
		#print ("New address without category terms: >"+" ".join(newAddress)+"<\n")
		
		#print (self.remover_acentos(" ".join(newAddress)))
		#Remover acentos e cedilhas
		return (self.remover_acentos(" ".join(newAddress)))

	def removeAddressCategory(self, address):
		newAddress = []
		words = address.split(" ")

		#Termos geográficos
		#keyWords = ['avenida', 'rua', 'bairro', 'praça', 'praca', 'trincheira', 'rodovia', 'via', 'estrada']
		#keyWords = ['avenida', 'rua', 'bairro', 'rodovia', 'elevado', 'praca', 'praça']
		
		#keyWordsEncoded = []
		#for key in keyWords: keyWordsEncoded.append(key.decode('utf8'))
		#for key in keyWords: keyWordsEncoded.append(key)		
		#print (keyWordsEncoded)
		#print ("Original address: >" + address +"<")

		#Removing address category terms
		for index, token in enumerate(words):
			if (token != "" and token != " "): 
				if (token.lower() not in self.keyWordsEncoded): 
					newAddress = words[index: len(words)]
					break
					
		#print (	newAddress)
		#print ("New address without category terms: >"+" ".join(newAddress)+"<\n")
		return (" ".join(newAddress))

	#------------------------------------------------------
	# Function
	# @params:
	# @author: Monica
	#------------------------------------------------------
	def stopWordsFilter(self, addressToVerify):
		toContinue = True
		
		wordsTest = str(addressToVerify).split(" ")			
		stopwordsCount = 0
		lowercaseWords = 0
		
		#print("WordsTest: "+str(wordsTest) + "\n")
		#print ('de' in self.stopwords)
		#print ('DE' in self.stopwords)
		
		#condiçoes anteriores
		#for word in wordsTest:
		#	if (word.isnumeric()): toContinue = False	
		#	if (word in self.stopwords): stopwordsCount += 1	
		#	if (word.islower()): lowercaseWords += 1		
		#stopwordsCount = stopwordsCount/len(wordsTest)
		#lowercaseWords =lowercaseWords/len(wordsTest)
		
		#Conditions to avoid be evaluated
		if (wordsTest[0].isnumeric()): toContinue = False
		if (wordsTest[0] in self.stopwords): toContinue = False

		# Minímos requisitos para verificar no dicionário
		#if ((len(wordsTest) <= self.minimumTermsNumber) and (stopwordsCount >= self.minumumStopWordsTerms or lowercaseWords > self.minumumUppercaseTerms)): 	
		
		return toContinue

	#------------------------------------------------------
	# Function
	# @params:
	# @author: Monica
	#------------------------------------------------------
	def verifyAddress(self, address):
		addressesMatched = {}
		result = []
		indices = None
		matched = False
	
		#Impede que stopwords sejam avaliadas
		#toContinue = self.stopWordsFilter(address)
	
		#remove address category terms
		#address = [self.removeAddressCategory(address[0])]
		#address = self.removeAddressCategory(address)
		
		#remove spaces and accents
		#address[0] = self.remover_acentos(address[0])	
		#address = self.remover_acentos(address)	
		
		address = self.addressTermsFilter(address)
		
		#print ("Address to vrfy2:: "+str(address))
		if (address == "" or address == " "): return None
		#print ("\n")
		
		#Extraindo key
		addressToVerify = str(address)
		key = self.extractKey(addressToVerify)
		#print ('Key: ' + key)
		#print ('address original: ' + str(address))
		#print ('addressToVerify: ' + addressToVerify + "\n")
		#print ('addressToVerify: ' + addressToVerify + "\n")
		
		#Verificando endereço sem abreviações
		matched = self.verifyCompleteAddress(key, addressToVerify, addressesMatched)
		#print ("\nCompleto: ")
		#print (addressesMatched)
		
		#print("Array addres:: "+str(addressToVerify.split(" ")))
		#Endereços com apenas 1 termo (aproximado)
		#if (not matched and len(addressToVerify.split(" ")) == 1):
			#matched = self.verifyAddressPart(key, addressToVerify, addressesMatched)
			#print ("1 termo só, Address matched:: "+str(addressesMatched))
			
		#Verificando possíveis abreviações
		if (not matched and key in self.dicionario):
			matched = self.verifyAbbreviatedAddress(key, addressToVerify, addressesMatched)
			#print ("\nA>>>>>>>>>>>>>>>>>>>>>>>>>>>>>breviado: ")
			#print (addressesMatched)	
		
		#Verificando possíveis termos utilizando apenas o primeiro termo da expressão
		if (not matched and key[0] in self.dicionario):
			matched = self.verifyCompleteAddress(key[0], addressToVerify, addressesMatched)
			#print ("\nPrimeiro Termo: ")
			#print (addressesMatched)

		#Casamento aproximado
		if (not matched and key[0] in self.dicionario):
			#print ("\nAntes: "+str(addressesMatched))
			matched = self.verifyApproximatedAddress(key[0], addressToVerify, addressesMatched)
			#print ("\nAproximado: ")
			#print (addressesMatched)
	
		#Verificando entradas com qualquer parte do endereço
		#if (not matched):
			#matched = self.verifyTermsPresence(key, addressToVerify, addressesMatched)
			#print ("\Qualquer parte: ")
			#print (addressesMatched)
			
		#print ("\n-------------------\n")			
		distanceVector = sorted(addressesMatched)
		#print ("\nMatched::: " + str(addressesMatched)+"\n\n")

		# Resultado com mais de uma distancia de edicao
		if (len(addressesMatched) > 1):
			i = 0
			#Escolhendo o conj de menor distancia de edicao
			while (len(result) == 0 and i < len(addressesMatched)):
				if (i in addressesMatched): result.append(addressesMatched[distanceVector[i]])
				else : i = i + 1

			#if (len(result) >= 1): result = result[0][0]
			#print ("\nResult1::: " + str(addressesMatched)+"\n\n")
			
			#Resultados com menor distancia de edição
			if (len(result) > 1): result = result[0]
			elif (len(result)== 1): result = result[0][0]

		
		# Resultado apenas uma distancia de edicao
		elif (len(addressesMatched) > 0):			
			#result = list(addressesMatched.values())[0][0]
			#print ("Values::: " + str(list(addressesMatched.values()))+"\n\n")
			#print ("Values::: " + str(len(list(addressesMatched.values())[0]))+"\n\n")
			
			#Resultados com menor distancia de edição
			if (len(list(addressesMatched.values())[0]) == 1):
				result = list(addressesMatched.values())[0][0]
			else:
				result = list(addressesMatched.values())[0]

		# Sem resultados
		else:
			result = None
		
		#print (type(result))
		#if (result != None and type(result) is list): 
		#	print (type(result[0]))
		#	print (len(result[0]))
		#	print (result[0])
		#	print (len(result))
			
		if (result != None and type(result) is not list): print (">>>>> ERRO1")
		if (result != None and (type(result[0]) is list and len(result) <= 1)):print (">>>>>> ERRO2")
		if (result != None and (type(result[0]) is not list and type(result[0]) is not str)): print (">>>>>>> ERRO3")
		
		#if ( ( result != None and type(result) is not list) or ( result != None and (type(result[0]) is not list or len(result[0]) <= 1) or (type(result[0]) is not list and type(result[0]) is not str))): print (">>>>>>>>>>ERRO: Returned matched results format incorrect")
		#print("Result::: " + str(result))
		return result

	#------------------------------------------------------
	# Function
	# @params:
	# @author: http://wiki.python.org.br/RemovedorDeAcentos#CA-ee056743639a7a1f3c8dac71f09c13863c354b09_18
	#------------------------------------------------------
	def remover_acentos(self, txt, codif='utf-8'):
		result = []
		
		#Remove extra spaces
		#txt = " ".join(txt.split())
		
		words = str(txt).split(" ")
		for word in words:
			i = "".join(x for x in unicodedata.normalize('NFKD', word) if x in string.ascii_letters).upper()
			if (i != ''): result.append(i)
		return " ".join(result)
		
		try:
			txt = normalize('NFKD', txt.decode(codif)).encode('ASCII','ignore').upper()
		except Exception:
			txt = txt.encode('utf-8')
			txt = normalize('NFKD', txt.decode(codif)).encode('ASCII','ignore').upper()
		return txt

	#------------------------------------------------------
	# Function
	# @params:
	# @author: Monica
	#------------------------------------------------------
	#~ def getCoordinate(self):
		#~ self.dicionario = {}
		#~ address = 'Barão Homem de Melo'
		#~ addressItem = str(self.remover_acentos(address))
		#~ key = self.extractKey(addressItem)
		#~ self.addToDictionary(key, addressItem)
		#~ #print (self.dicionario)
		#~ #self.teste()
		#~ 
		#~ print ("Result: " + str(self.verifyAddress('Barão Homem de Melo')))

	#------------------------------------------------------
	# Function
	# @params:
	# @author: Monica
	#------------------------------------------------------
	def teste(self):
		print ("\n[Passa] na Rua da Bahia:: "    	+ str(self.verifyAddress("na Rua da Bahia")))
		print ("\n[Passa] Rua da Bahia:: "    		+ str(self.verifyAddress("Rua da Bahia")))
		print ("\n[Passa] R da Bahia:: "    		+ str(self.verifyAddress("R da Bahia")))
		print ("\n[Passa] da Bahia:: "    			+ str(self.verifyAddress("da Bahia")))
		print ("\n[Passa] da:: "    				+ str(self.verifyAddress("da")))
		print ("\n[Passa] na:: "    				+ str(self.verifyAddress("na")))
		print ("\n[Passa] de:: "    				+ str(self.verifyAddress("de")))
		print ("\n[Passa] a:: "    					+ str(self.verifyAddress("a")))
		return
		print ("\n[Passa] Barão:: "    				+ str(self.verifyAddress("Barão")))
		print ("\n[Passa] NS do Carmo:: "     		+ str(self.verifyAddress("NS do Carmo")))
		print ("\n[Passa] M Menicucci:: "     		+ str(self.verifyAddress("M Menicucci")))
		print ("\n[Passa] Barão Homem de Melo:: "   + str(self.verifyAddress("Barão Homem de Melo")))
		print ("\n[Passa] aBILIO mACHADO:: "    	+ str(self.verifyAddress("aBILIO mACHADO")))	
		print ("\n[Passa] REGIONeL:: "     			+ str(self.verifyAddress("REGIONeL")))
		print ("\n[Passa] C Machado:: "     		+ str(self.verifyAddress("C Machado")))
		print ("\n[Passa] C M:: "     				+ str(self.verifyAddress("C M")))
		print ("\n[Passa] M Menicucci:: "     		+ str(self.verifyAddress("M Menicucci")))
		print ("\n[Passa] NS do Carmo:: "     		+ str(self.verifyAddress("NS do Carmo")))
		print ("\n[Passa] aBILIO mACHADO:: "     	+ str(self.verifyAddress("aBILIO mACHADO")))	
		print ("\n[Passa] A Machado:: "     		+ str(self.verifyAddress("A Machado")))
		print ("\n[Passa] Abilio M:: "     			+ str(self.verifyAddress("Abilio M")))
		print ("\n[Passa] M Goes Menicucci:: "     	+ str(self.verifyAddress("M Goes Menicucci")))
		print ("\n[Passa] Marcelo Goes Menicucci:: "+ str(self.verifyAddress("Marcelo Goes Menicucci")))
		print ("\n[Passa] Marcelo Goes M:: "     	+ str(self.verifyAddress("Marcelo Goes M")))
		print ("\n[Passa] M G Menicucci:: "     	+ str(self.verifyAddress("M G Menicucci")))
		print ("\n[Passa] Marcelo G Menicucci:: "   + str(self.verifyAddress("Marcelo G Menicucci")))
		print ("\n[Passa] REGIONeL:: "     			+ str(self.verifyAddress("REGIONeL")))
		print ("\n[Nao Passa] REGIONeieL:: "     	+ str(self.verifyAddress("REGIONeieL")))
		print ("\n[Passa] Barão Homem de Melo:: "   + str(self.verifyAddress("Barão Homem de Melo")))
		print ("\n[Passa] Barão Homem de M:: "     	+ str(self.verifyAddress("Barão Homem de M")))
		print ("\n[Passa] Barão H de Melo:: "     	+ str(self.verifyAddress("Barão H de Melo")))
		print ("\n[Passa] B H de Melo:: "     		+ str(self.verifyAddress("B H de Melo")))
		print ("\n[Passa] B H de M:: "     			+ str(self.verifyAddress("B H de M")))
		print ("\n[Passa] Conceição do Mato Dentro:: "+ str(self.verifyAddress("Conceição do Mato Dentro")))
		print ("\n[Passa] Conceição do M D:: "     	  	+ str(self.verifyAddress("Conceição do M D")))
		print ("\n[Passa] C do Mato Dentro:: "     		+ str(self.verifyAddress("C do Mato Dentro")))
		print ("\n[Passa] C do M Dentro:: "     		+ str(self.verifyAddress("C do M Dentro")))
		print ("\n[Passa] Nossa Senhora do Carmo:: "    + str(self.verifyAddress("Nossa Senhora do Carmo")))
		print ("\n[Passa] Nossa Senhora do C:: "     	+ str(self.verifyAddress("Nossa Senhora do C")))
		print ("\n[Passa] Nossa S do C:: "     			+ str(self.verifyAddress("Nossa S do C")))
		print ("\n[Passa] N S do Carmo:: "     			+ str(self.verifyAddress("N S do Carmo")))
		print ("\n[Passa] Cristiano Machado:: "     	+ str(self.verifyAddress("Cristiano Machado")))
		print ("\n[Passa] C Machado:: "     			+ str(self.verifyAddress("C Machado")))
		print ("\n[Passa] C M:: "     					+ str(self.verifyAddress("C M")))	
		print ("\n[Passa] Avenida Nossa Senhora do Carmo:: "+ str(self.verifyAddress("Avenida Nossa Senhora do Carmo")))
		print ("\n[Passa] Avenida B H de Melo:: "     		+ str(self.verifyAddress("Avenida B H de Melo")))
		print ("\n[Passa] Rua aBILIO mACHADO:: "    		+ str(self.verifyAddress("Rua aBILIO mACHADO")))	
		print ("\n[Passa] AGUA MARINHA:: "     				+ str(self.verifyAddress("Água Marinha")))	
		print ("\n[Passa] AGUA:: "     						+ str(self.verifyAddress("Água")))	
		print ("\n[Passa] AGUANIL:: "     					+ str(self.verifyAddress("AGUANIL")))	
		print ("\n[Passa] TABOAO DA SERRA:: "     			+ str(self.verifyAddress("TABOAO DA SERRA")))	
		print ("\n[Passa] TABOãO DA SERRA::"     			+ str(self.verifyAddress("Taboão da serra")))
		#coordinate = self.geocoding("Barão Homem de Melo")
		#print (coordinate)
		
	#------------------------------------------------------
	# Function
	# @params:
	# @author: Monica
	#------------------------------------------------------
	#~ def verifyAddress0(self, address):
		#~ addressesMatched = {}
		#~ result = []
		#~ indices = None
		#~ 
		#~ #Impede que stopwords sejam avaliadas
		#~ wordsTest = str(address).split(" ")	
		#~ stopwordsCount = 0
		#~ lowercaseWords = 0
		#~ for word in wordsTest:
			#~ if (word.isnumeric()): return None
			#~ if (word in self.stopwords): stopwordsCount += 1	
			#~ if (word.islower()): lowercaseWords += 1		
		#~ stopwordsCount = stopwordsCount/len(wordsTest)
		#~ lowercaseWords =lowercaseWords/len(wordsTest)
#~ 
		#~ if ((len(wordsTest) <= 2) and (stopwordsCount >= 0.5 or lowercaseWords > 0.5)): 
			#~ return None
		#~ 
		#~ #Extraindo key
		#~ addressItem = str(self.remover_acentos(address))
		#~ key = self.extractKey(addressItem)
		#~ matched = False
		#~ 
		#~ #Verificando endereço sem abreviações
		#~ if (key in self.dicionario):
			#~ #casamento de cadeias
			#~ for item in self.dicionario[key]:
				#~ indices = self.getDistance(addressItem, item, addressesMatched)
				#~ if (len(indices)>0):
					#~ matched = True
					#~ if (len(addressesMatched) >= 3): break
#~ 
		#~ indices = []
#~ 
		#~ #Verificando possíveis abreviações
		#~ if (not matched and key in self.dicionario):
			#~ #casamento de cadeias
			#~ for item in self.dicionario[key]:
				#~ words = str(addressItem).split(" ")
				#~ dicionarioAddress = str(item).split(" ")
				#~ newAddress = item
#~ 
				#~ #Endereço abeviado de acordo com o padrão
				#~ i = 0
				#~ for word in words:
					#~ if (len(word) == 1): newAddress = str(newAddress).replace(dicionarioAddress[i], dicionarioAddress[i][0])
					#~ i = i + 1
				#~ indices = self.getDistance(addressItem, newAddress, addressesMatched)
				#~ if (len(indices)>0):
					#~ addressesMatched[indices[0]][indices[1]] = item
					#~ if (len(addressesMatched) >= 3): break
#~ 
		#~ indices = []
#~ 
		#~ if (not matched and key[0] in self.dicionario):
			#~ #casamento de cadeias
			#~ for item in self.dicionario[key[0]]:
				#~ indices = self.getDistance(addressItem, item, addressesMatched)
				#~ if (len(indices)>0):
					#~ matched = True
					#~ if (len(addressesMatched) >= 3): break
#~ 
		#~ distanceVector = sorted(addressesMatched)
		#~ print (addressesMatched)
#~ 
		#~ # Resultado com mais de uma distancia de edicao
		#~ if (len(addressesMatched) > 1):
			#~ i = 0
			#~ #Escolhendo o conj de menor distancia de edicao
			#~ while (len(result)==0 and i < len(addressesMatched)):
				#~ if (i in addressesMatched): result.append(addressesMatched[distanceVector[i]])
				#~ else : i = i + 1
#~ 
			#~ if (len(result) >= 1): result = result[0]
#~ 
		#~ 
		#~ # Resultado apenas uma distancia de edicao
		#~ elif (len(addressesMatched) > 0):			
			#~ result = list(addressesMatched.values())[0][0]
#~ 
		#~ # Sem resultados
		#~ else:
			#~ result = None
#~ 
		#~ return result

	