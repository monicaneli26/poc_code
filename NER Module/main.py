import	 sys
from 	dataset			import Dataset
#from 	topicClassification import TopicClassification
#from 	rules 			import Rules
#from 	testingModule 	import TestingModule
#from 	postagging 		import POStagging
#from 	dicionario 		import Dictionary
#from 	nlp 			import NLP

class Main:
	gazetteer 		= sys.argv[1]
	trainingFile 	= sys.argv[2]
	testingFile 	= sys.argv[3]
	totalInstances 	= sys.argv[4]
	
	#topicClassify = TopicClassification()
	#topicClassify.classify(trainingFile, testingFile, totalInstances)
	#exit()
	
	dataset = Dataset()
	dataset.crossValidation(totalInstances, 10, gazetteer)
	
	exit()
	
	# Teste de Dicionário
	#dicionario = Dictionary(gazetteer)
	#dicionario.teste()
	#exit()

	# Teste Dataset
	#dataset = Dataset()
	#dataset.testingFunctions(gazetteer)
	
	# Teste de NLP Module
	#nlp = NLP()
	#nlp.teste()