#!/usr/bin/python
# -*- coding: utf-8 -*-

import	nltk
import 	nltk.corpus, nltk.tag, itertools
from 	nltk.util 	import ngrams
from 	unicodedata import normalize

class NLP:
	senteceTest		= None
	conjunto 		= None
	posTagger 		= None
	stemmer 		= nltk.stem.RSLPStemmer()
	keyWordsEncoded = ['avenida', 'rua', 'bairro', 'praça', 'trevo', 'elevado', 'trincheira', 'rodovia', 'curva', 'via', 'círculo', 'circulo', 'parque', 'área', 'area', 'rotatória', 'rotatoria', 'estrada']
	
	#'em', 'na', 'altura', 'expressa', 'sentido','próximo', 'proximo', 'prox', 'próx', 'ao'

	#------------------------------------------------------
	# Function
	# @params:
	# @author: Monica
	#------------------------------------------------------
	def tokenization(self, sentence):
		#return nltk.word_tokenize(str(sentence).decode('utf8'))
		return nltk.word_tokenize(str(sentence))

	#------------------------------------------------------
	# Function
	# @params:
	# @author: http://wiki.python.org.br/RemovedorDeAcentos#CA-ee056743639a7a1f3c8dac71f09c13863c354b09_18
	#------------------------------------------------------
	def tokenEncodeDecode(self, txt, codif='utf-8'):
		try:
			txt = normalize('NFKD', txt.decode(codif)).encode('ASCII','ignore')
		except Exception:
			txt = txt.encode('utf-8')
			txt = normalize('NFKD', txt.decode(codif)).encode('ASCII','ignore')
		return txt


	#------------------------------------------------------
	# Function
	# @params:
	# @author: Monica
	#------------------------------------------------------
	def hasGeoEvidenceToken(self, token):
		result = False
		if (token.lower() in self.keyWordsEncoded): result = True
		return result


	#------------------------------------------------------
	# Function: extractGeoEvidenceTokens
	# Utilizada na geração do padrão de regras.
	# @params: nGram
	# @author: Monica
	#------------------------------------------------------
	def extractGeoEvidenceTokens(self, nGram):				
		result = []
		for token in nGram:
			if (token.lower() in self.keyWordsEncoded): result.append(token)
		return result

	#------------------------------------------------------
	# Function
	# @params:
	# @author: Monica
	#------------------------------------------------------
	def extractNGrams(self, n, sentenceTokens):
		return list(ngrams(sentenceTokens, n))


	#------------------------------------------------------
	# Function
	# @params:
	# @author: Monica
	#------------------------------------------------------
	def extractUpperCaseTerms(self, sentenceTokens):
		termList = []
		for token in sentenceTokens:
			if (token[0].isupper()):
				termList.append(token)
		return termList

	#------------------------------------------------------
	# Function
	# @params:
	# @author: Monica
	#------------------------------------------------------
	def teste(self):
		self.senteceTest = nltk.word_tokenize("15h59 Foliões estão acessando a Guaicurus e deixando o trânsito lento na Contorno e Andradas, sentido centro.")
		
		#self.conjunto = ["Expressa próx. ao Metro da Gameleira.", "Foliões estão acessando a Guaicurus e deixando o trânsito lento na Contorno e Andradas, sentido centro."]
		self.conjunto = ["Expressa próx. ao Metro da Gameleira."]

		#stemmer = nltk.stem.RSLPStemmer()
		#print stemmer.stem("copiando")
		#print nltk.pos_tag([stemmer.stem("copiando")])

		#stopwords = nltk.corpus.stopwords.words('portuguese')
		#print 'aquela' in stopwords

		for sentenceItem in self.conjunto:
			#print sentenceItem.decode('utf8')
			#tokens = nltk.word_tokenize(str(sentenceItem).decode('utf8'))
			tokens = self.tokenization(sentenceItem)
			print ("N-gram1:: " + str(tokens))
			print ("N-gram2:: "+str(self.extractNGrams(2, tokens)))
			print ("N-gram3:: "+str(self.extractNGrams(3, tokens)))
			print ("N-gram4:: "+str(self.extractNGrams(4, tokens)))
			print ("N-gram5:: "+str(self.extractNGrams(5, tokens)))

		print(self.extractUpperCaseTerms(self.senteceTest))

	def getStem(self, token):
		#stemmer = nltk.stem.RSLPStemmer()
	#	print (token +" STEMMER:: " + str(self.stemmer.stem(token)))
		#print nltk.pos_tag([stemmer.stem("copiando")])
		return (str(self.stemmer.stem(token)))

	#def extractNGramas(self, n, sentenceTokens):
	#~ def extractNGramas0(self, n, sentenceTokens):
		#~ ngramsGeoList = []
		#~ #ngramList = ngrams(sentenceTokens, n)
		#~ #print ngramList
		#~ for grams in ngrams(sentenceTokens, n):
			#~ if (self.hasGeoEvidence(grams)):
				#~ ngramsGeoList.append(" ".join(grams))
				#~ #print " ".join(grams)
#~ 
		#~ ngramList = [" ".join(i) for i in list(ngrams(sentenceTokens, n))]
		#~ return [ngramList, ngramsGeoList]

	#~ def extractNGramas2(self, sentence):
		#~ sentenceTokens = nltk.word_tokenize(sentence)
		#~ return sentenceTokens
		#~ 
	#~ def extractNGramas4(self, sentence, n):
		#~ sentenceTokens = nltk.word_tokenize(sentence)
		#~ return list(ngrams(sentenceTokens, n))

	#~ def hasGeoEvidence(self, nGram):
		#~ result = False
		#~ #Se houver evidencia geográfica (Avenida, Rua, Praça, em, na, Elevado, trincheira, altura, rodovia, curva, via, expressa, círculo, parque, area, rotatória, sentido, a, proximo)
		#~ keyWordsEncoded = ['avenida', 'rua', 'praça', 'em', 'na', 'elevado', 'trincheira', 'altura', 'rodovia', 'curva', 'via', 'expressa', 'círculo', 'circulo', 'parque', 'área', 'area', 'rotatória', 'rotatoria', 'sentido', 'próximo', 'proximo', 'prox', 'próx']
		#~ #print (keyWordsEncoded)
		#~ #keyWordsEncoded = []
		#~ #for key in keyWords: keyWordsEncoded.append(key.decode('utf8'))
		#~ if (nGram[0].lower() in keyWordsEncoded): result = True
		#~ return result
		
	#~ def tokenEncoding(self, txt, codif='utf-8'):
		#~ try:
			#~ txt = normalize('NFKD', txt.decode(codif))
		#~ except Exception:
			#~ txt = txt.encode('utf-8')
			#~ txt = normalize('NFKD', txt.decode(codif))
		#~ return txt		