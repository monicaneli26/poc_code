#!/usr/bin/python
# -*- coding: utf-8 -*-

import  nltk
import nltk.corpus, nltk.tag, itertools
#from nltk.corpus import machado
#from nltk.corpus import floresta


class POStagging:
	macMorpho = None
	floresta  = None
	posTagger = None
	
	def __init__(self):
		print ("\n---------POS-TAGGING TRAINING--------")
		self.macMorphoCorpus()		
		#self.florestaCorpus()	
		#self.florestaMacMorphoTraining()
		

	def macMorphoCorpus(self):
		self.macMorpho = nltk.corpus.mac_morpho.tagged_sents()
		
		tagger0 = nltk.tag.UnigramTagger(self.macMorpho)
		tagger1 = nltk.tag.BigramTagger(self.macMorpho, backoff=tagger0)
		self.posTagger = nltk.tag.TrigramTagger(self.macMorpho, backoff=tagger1)
		
		#size = int(len(self.macMorpho) * 0.9)
		#size2 = int(len(self.macMorpho) * 0.95)
		
		#train_sents0 = self.macMorpho[:size]
		#test_sents0 = self.macMorpho[size:size2]
		
		print ("MAC-MORPHO Corpus size: " + str(len(self.macMorpho)))
		#print ("Training: " + str(size) )	
		#print ("Testing: " + str(len(self.macMorpho)-size2))		
		#self.macMorphoTraining(train_sents0, test_sents0)
		
		
	def macMorphoCorpusEvaluation(self):
		self.macMorpho = nltk.corpus.mac_morpho.tagged_sents()
		
		size = int(len(self.macMorpho) * 0.9)
		size2 = int(len(self.macMorpho) * 0.95)
		
		train_sents0 = self.macMorpho[:size]
		test_sents0 = self.macMorpho[size:size2]
		
		print ("MAC-MORPHO Corpus: " + str(len(self.macMorpho)))
		print ("Training: " + str(size) )	
		print ("Testing: " + str(len(self.macMorpho)-size2))
		
		self.macMorphoTraining(train_sents0, test_sents0)

	def florestaCorpus(self):
		self.floresta = nltk.corpus.floresta.tagged_sents()
		
		size = int(len(self.floresta) * 0.9)
		size2 = int(len(self.floresta) * 0.95)
		
		train_sents1 = self.floresta[:size]
		test_sents1 = self.floresta[size:size2]
		
		print ("floresta: " + str(len(self.floresta)))
		print ("Trainning: " + str(size)+ " vs " + str(len(train_sents1)))		
		print ("Test: " + str(len(self.floresta)-size2)+ " vs " + str(len(test_sents1)))
		self.florestaTraining(train_sents1, test_sents1)

	def macMorphoTraining(self, train_sents0, test_sents0):
		print ("-------------")
		#one-grams
		#unigram_tagger = nltk.UnigramTagger(train_sents0)
		#print unigram_tagger.evaluate(test_sents0)

		#bi-grams
		#bigram_tagger = nltk.BigramTagger(train_sents0)
		#print bigram_tagger.evaluate(test_sents0)

		
		tagger0 = nltk.tag.UnigramTagger(train_sents0)
		tagger1 = nltk.tag.BigramTagger(train_sents0, backoff=tagger0)
		self.posTagger = nltk.tag.TrigramTagger(train_sents0, backoff=tagger1)
		print ("POS-tagging Accuracy: " + str(self.posTagger.evaluate(test_sents0)))
		#print "-------------"
		
	def florestaTraining(self, train_sents1, test_sents1):
		print ("-------------")
		#one-grams
		unigram_tagger = nltk.UnigramTagger(train_sents1)
		print (unigram_tagger.evaluate(test_sents1))

		#bi-grams
		bigram_tagger = nltk.BigramTagger(train_sents1)
		print (bigram_tagger.evaluate(test_sents1))

		print ("----UNITED---------")
		tagger0 = nltk.tag.UnigramTagger(train_sents1)
		tagger1 = nltk.tag.BigramTagger(train_sents1, backoff=tagger0)
		tagger2 = nltk.tag.TrigramTagger(train_sents1, backoff=tagger1)
		print (tagger2.evaluate(test_sents1))
		
	def florestaMacMorphoTraining(self):		
		size = int(len(self.macMorpho) * 0.9)
		size2 = int(len(self.macMorpho) * 0.95)
		
		train_sents0 = self.macMorpho[:size]
		test_sents0 = self.macMorpho[size:size2]
		
		self.floresta = nltk.corpus.floresta.tagged_sents()
		size = int(len(self.floresta) * 0.9)
		size2 = int(len(self.floresta) * 0.95)
		
		train_sents1 = self.floresta[:size]
		test_sents1 = self.floresta[size:size2]
		
		print ("-------FLORESTA + MAC-MORPHO------")
		tagger0 = nltk.tag.UnigramTagger(train_sents0+train_sents1)
		tagger1 = nltk.tag.BigramTagger(train_sents0+train_sents1, backoff=tagger0)
		tagger2 = nltk.tag.TrigramTagger(train_sents0+train_sents1, backoff=tagger1)
		print (tagger2.evaluate(test_sents0+test_sents1))

	def posTaggingNGram(self, nGram):
		#print self.posTagger.tag(nGram)
		return self.posTagger.tag(nGram)
		#print self.posTagger.pos_tag(nGram)
	