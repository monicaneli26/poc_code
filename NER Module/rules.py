from __future__ 	import division
from	nlp 		import NLP
from 	postagging 	import POStagging
import 	operator
import	re
import 	os
import 	numpy as np

class Rules:
	ruleSet			= {}
	nGramsSet 		= []
	nlp 			= None
	posTagging 		= None
	minSupportValue = 1
	
	def __init__(self , posTaggingInstance):
		self.posTagging = posTaggingInstance
		self.nlp = NLP()
		self.ruleSet = {}
		self.nGramsSet = []
		
	def addRule(self, rule, support):	
		if (not(rule in self.ruleSet)):
			self.ruleSet[rule] = support
		
	#------------------------------------------------------
	# Function addNGram
	# Adiciona as expressões geográficas extraídas do conjunto de treinamento.
	# Todos as expressões são adicionadas ao atributo nGramsSet.
	# @params: nGram, labelIOB
	# @author: Monica
	#------------------------------------------------------	
	def addNGram(self, nGram, label):
		self.nGramsSet.append([nGram, label])
		
	def selectRule(self, rule):
		support = None
		if (rule in self.ruleSet): support = self.ruleSet[rule]		
		return support

	#------------------------------------------------------
	# Function getNGramsSet
	# Retorna todas as expressões geográficas que foram adicionadas durante o treinamento.
	# @params: Nenhum
	# @author: Monica
	#------------------------------------------------------			
	def getNGramsSet(self):
		return self.nGramsSet;
		
	def rulesNum(self):
		return len(self.ruleSet)
		
	def rulesInfo(self):
		print ("\nRules Num: " + str(self.rulesNum()) + "\n\n")
		print ("Max: " + str(max(self.ruleSet.items(), key=operator.itemgetter(1))))
		print ("Min: " + str(min(self.ruleSet.items(), key=operator.itemgetter(1))))
		vectorValues = list(self.ruleSet.values())
		
		#Distribuição dos valores de support entre as regras - Par (Support , NumOcorrencias)
		result_dict = dict( [ (i, vectorValues.count(i)) for i in set(vectorValues) ] )
		r = sorted(result_dict.items(), key=operator.itemgetter(0), reverse=True)
		print (r)
		print ("Mean: "+ str(np.mean(vectorValues)))
		print ("Std: "+ str(np.std(vectorValues)))
		
		graficoFileName = ""
		for i in range(0, 10):
			if (not os.path.isfile('./grafico_rules_'+str(i))): 
				graficoFileName = 'grafico_rules_'+str(i)
				break
		#if (graficoFileName == ""): graficoFileName = 'grafico_rules_0'
		
		f = open(graficoFileName, 'w')
		for item in r:
			f.write(str(item[0]) + " " + str(item[1]) + "\n")
		f.close()
		
		f2 = open(graficoFileName+"_d", 'w')
		for index,item in enumerate(self.ruleSet.items()):
			f2.write(str(index) + " , " + str(item[1])+"\n")
		f2.close()
		
	def printRules(self):
		for rule in self.ruleSet: print (rule)
			
	def findPattern(self, nGram):
		#get uppercase pattern
		uppercasePattern = self.getUppercasePattern(nGram)

		#get geo evicence terms
		geoEvidencePattern = self.nlp.extractGeoEvidenceTokens(nGram)

		#get pos-tagging pattern
		postaggingPattern = self.posTagging.posTaggingNGram(nGram)
		
		uppercaseRule = {'u': uppercasePattern}			
		posTaggingRule = {'p': [item[1] for item in postaggingPattern]}
		if (len(geoEvidencePattern) > 0 ): geoEvidenceRule = {'g': geoEvidencePattern}
		else: geoEvidenceRule = None
		
		return (uppercaseRule, geoEvidenceRule, posTaggingRule)
		
	def getUppercasePattern(self, nGram):
		pattern = ""
		cont = 0
		for token in nGram:
			if (token[0].isupper()):
				pattern = pattern+'S'
				cont += 1
			else:
				pattern = pattern+'N'
		
		return cont/len(nGram)
	
	def generateRules(self):		
		#C1 candidates
		c1_uppercase = {}
		c1_geoEvidence = {}
		c1_posTagging = {}
		
		#All N-grams, i.e. annotated geographic expression, added during training period
		for ngram in self.getNGramsSet():			
			# Obtendo o padrão
			uppercaseRule, geoEvidenceRule, posTaggingRule = self.findPattern(ngram[0])
			
			#adding C1 candidates and counting support	value					
			if (not (str(uppercaseRule) in c1_uppercase)): c1_uppercase[str(uppercaseRule)] = 1
			else: c1_uppercase[str(uppercaseRule)] += 1				
			
			if ((geoEvidenceRule != None) and not (str(geoEvidenceRule) in c1_geoEvidence)): c1_geoEvidence[str(geoEvidenceRule)] = 1
			elif (geoEvidenceRule != None): c1_geoEvidence[str(geoEvidenceRule)] += 1				
			
			if (not (str(posTaggingRule) in c1_posTagging)): c1_posTagging[str(posTaggingRule)] = 1
			else: c1_posTagging[str(posTaggingRule)] += 1
			

		sortedC1_uppercase = sorted(c1_uppercase.items(), key=operator.itemgetter(1), reverse=True)
		sortedC1_geoEvidence = sorted(c1_geoEvidence.items(), key=operator.itemgetter(1), reverse=True)
		sortedC1_posTagging = sorted(c1_posTagging.items(), key=operator.itemgetter(1), reverse=True)
			
		#Podando atributos com suporte menor que o mínimo e combinando atributos cujos valores de suporte são maiores
		l1_frequent = {}
		#Uppercase Candidates
		for uppercaseCandidateRule in sortedC1_uppercase:
			
			#Is support value greater than the minimal			
			if (uppercaseCandidateRule[1] > self.minSupportValue):
				
				#PosTagging Candidates						
				for posTaggingCandidateRule in sortedC1_posTagging:
					if (posTaggingCandidateRule[1] > self.minSupportValue):
						
						#Uppercase + POS-tagging						
						rulePattern = str([eval(uppercaseCandidateRule[0]), eval(posTaggingCandidateRule[0])])
						l1_frequent[rulePattern] = 0
						
						#GeoEvidence Candidates
						for geoEvidenceCandidateRule in sortedC1_geoEvidence:
							if (geoEvidenceCandidateRule[1] > self.minSupportValue):
								
								#Uppercase + POS-tagging + GeoEvidence terms					
								rulePattern = str([eval(uppercaseCandidateRule[0]), eval(posTaggingCandidateRule[0]), eval(geoEvidenceCandidateRule[0])])
								l1_frequent[rulePattern] = 0
							
							else: break#vetor ordenado descendente chegou ao valor de suporte mínimo					
					else: break	#vetor ordenado descendente chegou ao valor de suporte mínimo			
			else: break #vetor ordenado descendente chegou ao valor de suporte mínimo


		#Scanning the rules generated before and counting support
		for ngram in self.getNGramsSet():
			uppercaseRule, geoEvidenceRule, posTaggingRule = self.findPattern(ngram[0])
						
			if (geoEvidenceRule != None): 
				rulePattern = str([uppercaseRule, posTaggingRule, geoEvidenceRule])
			else:
				rulePattern = str([uppercaseRule, posTaggingRule])
			
			if (rulePattern in l1_frequent): l1_frequent[rulePattern] += 1
			
		sortedL1 = sorted(l1_frequent.items(), key=operator.itemgetter(1), reverse=False)
		
		#Seleção das regras finais com supporte mínimo
		for rule in sortedL1:
			if (rule[1] > self.minSupportValue): self.addRule(rule[0], rule[1])
		
		self.rulesInfo()

	def isThereRule(self, nGram):
		#Obtendo o padrão do n-grama
		uppercaseRule, geoEvidenceRule, posTaggingRule = self.findPattern(nGram)
		
		#Regra
		if (geoEvidenceRule != None): 
			rulePattern = str([uppercaseRule, posTaggingRule, geoEvidenceRule])
		else:
			rulePattern = str([uppercaseRule, posTaggingRule])
		
		support = self.selectRule(rulePattern)
		return support
	
	
	#~ def testingRules(self):
		#~ nGram = "na Avenida do Contorno".split(" ")
		#~ print("Is there rule:: " + str(self.isThereRule(nGram)))
		