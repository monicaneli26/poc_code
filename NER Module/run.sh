#!/bin/sh

#Running the program: <DICIONARIO>  <TRAINING_ANNOTATED_INSTANCES>  <TESTING_INSTANCES> <TOTAL_INSTANCES>
#python3 main.py base/Gazetteer.csv base/datasetPreprocessed1_210_Trainning.csv base/datasetPreprocessed1_140_Testing.csv base/datasetPreprocessed_350.csv

#70% 30%
python3 main.py base/Gazetteer.csv base/Annotated_dataset_2003_Training.csv base/Annotated_dataset_858_Testing.csv base/Annotated_dataset_2861.csv

#Deleting the databases created if they exists
rm -rf *.pyc

#Moving output files
mv *.csv Output/

sh scripts/plot.sh

mv *.csv *.npy *.png *.pkl grafico_rules_0 grafico_rules_1 grafico_rules_2 grafico_rules_3 grafico_rules_4 Output/


exit 0