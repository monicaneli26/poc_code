
gnuplot -persist << EOF
reset
#set title 'Distribuição da Frequência dos Suportes Encontrados'
set auto x
set auto y
unset key
#set yrange [0:15]
#set xrange [0:200]
set xlabel 'Suporte'
set ylabel 'Frequência'
set grid
plot "grafico_rules_0" using 1:2 with points lc rgb '#0060ad' lt 1 lw 2 pt 7 pi -1 ps 1.5
#plot "grafico" using 1:2 with points lc rgb '#0060ad' lt 1 lw 2 pt 7 pi -1 ps 1.5
#replot "Ataques_ER1_grau.txt" with lines lw 2
#replot "Ataques_BA1_grau.txt" with lines lw 2
set terminal png
set output 'grafico_rules_0.png'
replot
EOF

gnuplot -persist << EOF
reset
#set title 'Distribuição da Frequência dos Suportes Encontrados'
set auto x
set auto y
unset key
#set yrange [0:15]
#set xrange [0:200]
set xlabel 'Suporte'
set ylabel 'Frequência'
set grid
plot "grafico_rules_1" using 1:2 with points lc rgb '#0060ad' lt 1 lw 2 pt 7 pi -1 ps 1.5
#plot "grafico" using 1:2 with points lc rgb '#0060ad' lt 1 lw 2 pt 7 pi -1 ps 1.5
#replot "Ataques_ER1_grau.txt" with lines lw 2
#replot "Ataques_BA1_grau.txt" with lines lw 2
set terminal png
set output 'grafico_rules_1.png'
replot
EOF

gnuplot -persist << EOF
reset
#set title 'Distribuição da Frequência dos Suportes Encontrados'
set auto x
set auto y
unset key
#set yrange [0:15]
#set xrange [0:200]
set xlabel 'Suporte'
set ylabel 'Frequência'
set grid
plot "grafico_rules_2" using 1:2 with points lc rgb '#0060ad' lt 1 lw 2 pt 7 pi -1 ps 1.5
#plot "grafico" using 1:2 with points lc rgb '#0060ad' lt 1 lw 2 pt 7 pi -1 ps 1.5
#replot "Ataques_ER1_grau.txt" with lines lw 2
#replot "Ataques_BA1_grau.txt" with lines lw 2
set terminal png
set output 'grafico_rules_2.png'
replot
EOF


gnuplot -persist << EOF
reset
#set title 'Distribuição da Frequência dos Suportes Encontrados'
set auto x
set auto y
unset key
#set yrange [0:15]
#set xrange [0:200]
set xlabel 'Suporte'
set ylabel 'Frequência'
set grid
plot "grafico_rules_3" using 1:2 with points lc rgb '#0060ad' lt 1 lw 2 pt 7 pi -1 ps 1.5
#plot "grafico" using 1:2 with points lc rgb '#0060ad' lt 1 lw 2 pt 7 pi -1 ps 1.5
#replot "Ataques_ER1_grau.txt" with lines lw 2
#replot "Ataques_BA1_grau.txt" with lines lw 2
set terminal png
set output 'grafico_rules_3.png'
replot
EOF

gnuplot -persist << EOF
reset
#set title 'Distribuição da Frequência dos Suportes Encontrados'
set auto x
set auto y
unset key
#set yrange [0:15]
#set xrange [0:200]
set xlabel 'Suporte'
set ylabel 'Frequência'
set grid
plot "grafico_rules_4" using 1:2 with points lc rgb '#0060ad' lt 1 lw 2 pt 7 pi -1 ps 1.5
#plot "grafico" using 1:2 with points lc rgb '#0060ad' lt 1 lw 2 pt 7 pi -1 ps 1.5
#replot "Ataques_ER1_grau.txt" with lines lw 2
#replot "Ataques_BA1_grau.txt" with lines lw 2
set terminal png
set output 'grafico_rules_4.png'
replot
EOF

gnuplot -persist << EOF
reset
#set title 'Distribuição da Frequência dos Suportes Encontrados'
set auto x
set auto y
unset key
#set yrange [0:15]
#set xrange [0:200]
set xlabel 'Suporte'
set ylabel 'Frequência'
set grid
plot "grafico_rules_5" using 1:2 with points lc rgb '#0060ad' lt 1 lw 2 pt 7 pi -1 ps 1.5
#plot "grafico" using 1:2 with points lc rgb '#0060ad' lt 1 lw 2 pt 7 pi -1 ps 1.5
#replot "Ataques_ER1_grau.txt" with lines lw 2
#replot "Ataques_BA1_grau.txt" with lines lw 2
set terminal png
set output 'grafico_rules_5.png'
replot
EOF

gnuplot -persist << EOF
reset
#set title 'Distribuição da Frequência dos Suportes Encontrados'
set auto x
set auto y
unset key
#set yrange [0:15]
#set xrange [0:200]
set xlabel 'Suporte'
set ylabel 'Frequência'
set grid
plot "grafico_rules_6" using 1:2 with points lc rgb '#0060ad' lt 1 lw 2 pt 7 pi -1 ps 1.5
#plot "grafico" using 1:2 with points lc rgb '#0060ad' lt 1 lw 2 pt 7 pi -1 ps 1.5
#replot "Ataques_ER1_grau.txt" with lines lw 2
#replot "Ataques_BA1_grau.txt" with lines lw 2
set terminal png
set output 'grafico_rules_6.png'
replot
EOF

gnuplot -persist << EOF
reset
#set title 'Distribuição da Frequência dos Suportes Encontrados'
set auto x
set auto y
unset key
#set yrange [0:15]
#set xrange [0:200]
set xlabel 'Suporte'
set ylabel 'Frequência'
set grid
plot "grafico_rules_7" using 1:2 with points lc rgb '#0060ad' lt 1 lw 2 pt 7 pi -1 ps 1.5
#plot "grafico" using 1:2 with points lc rgb '#0060ad' lt 1 lw 2 pt 7 pi -1 ps 1.5
#replot "Ataques_ER1_grau.txt" with lines lw 2
#replot "Ataques_BA1_grau.txt" with lines lw 2
set terminal png
set output 'grafico_rules_7.png'
replot
EOF


gnuplot -persist << EOF
reset
#set title 'Distribuição da Frequência dos Suportes Encontrados'
set auto x
set auto y
unset key
#set yrange [0:15]
#set xrange [0:200]
set xlabel 'Suporte'
set ylabel 'Frequência'
set grid
plot "grafico_rules_8" using 1:2 with points lc rgb '#0060ad' lt 1 lw 2 pt 7 pi -1 ps 1.5
#plot "grafico" using 1:2 with points lc rgb '#0060ad' lt 1 lw 2 pt 7 pi -1 ps 1.5
#replot "Ataques_ER1_grau.txt" with lines lw 2
#replot "Ataques_BA1_grau.txt" with lines lw 2
set terminal png
set output 'grafico_rules_8.png'
replot
EOF

gnuplot -persist << EOF
reset
#set title 'Distribuição da Frequência dos Suportes Encontrados'
set auto x
set auto y
unset key
#set yrange [0:15]
#set xrange [0:200]
set xlabel 'Suporte'
set ylabel 'Frequência'
set grid
plot "grafico_rules_9" using 1:2 with points lc rgb '#0060ad' lt 1 lw 2 pt 7 pi -1 ps 1.5
#plot "grafico" using 1:2 with points lc rgb '#0060ad' lt 1 lw 2 pt 7 pi -1 ps 1.5
#replot "Ataques_ER1_grau.txt" with lines lw 2
#replot "Ataques_BA1_grau.txt" with lines lw 2
set terminal png
set output 'grafico_rules_9.png'
replot
EOF
