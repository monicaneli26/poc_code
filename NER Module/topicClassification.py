#!/usr/bin/python
# -*- coding: utf-8 -*-

from __future__ import division, unicode_literals
import math
import nltk
import nltk.corpus
import csv
import re
import numpy
from sklearn import svm
from sklearn.externals import joblib
from sklearn.ensemble import BaggingClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.cross_validation import cross_val_score
from sklearn.ensemble import AdaBoostClassifier
from sklearn.ensemble import ExtraTreesClassifier
from sklearn.datasets import make_hastie_10_2
from sklearn.ensemble import GradientBoostingClassifier
import matplotlib.pyplot as plt
from sklearn.metrics import confusion_matrix
from sklearn import cross_validation
from sklearn.linear_model import LogisticRegression
from sklearn.naive_bayes import GaussianNB
from sklearn.ensemble import RandomForestClassifier
from sklearn.ensemble import VotingClassifier
from sklearn.metrics import accuracy_score
from sklearn.metrics import f1_score
from scipy import interp
from sklearn import metrics
from sklearn.metrics import roc_curve, auc, roc_auc_score
from sklearn.cross_validation import StratifiedKFold
from sklearn.cross_validation import train_test_split
from sklearn.metrics import precision_score
from sklearn.metrics import recall_score
from sklearn.preprocessing import label_binarize
from sklearn.multiclass import OneVsRestClassifier
from sklearn.feature_selection import SelectKBest
from sklearn.feature_selection import chi2

#from postagging import POStagging
from nlp import NLP
#from dataset import Dataset

class TopicClassification:
	posTagging = None
	nlp = NLP()
	stopwords = None
	vocabulary = []
	labelsTraining = []
	trainingInstances = []
	labelsTesting = []
	testingInstances = []
	totalInstances = []
	totalLabels = []
	datasetSentences = []
	datasetLabels = []
	totalNumInstances = 2861
	totalPredictedLabels = []
	totalTrueLabels = []
	
	"""
	" Classify
	"""
	def classify(self, fileNameTrainning, fileNameTesting, totalData):
		self.stopwords = nltk.corpus.stopwords.words('portuguese')
		
		#Tetse
		#~ self.readTrainingFile(totalData)
		#~ self.totalInstances = self.trainingInstances
		#~ self.totalLabels = self.labelsTraining
		#~ self.evaluateClassifier()
		#~ return 
		#self.crossValidation(totalData, 10)
		#return
		
		self.readTrainingFile(fileNameTrainning)		
		self.readTestingFile(fileNameTesting)

		print('=' * 80)
		print("INSTANCES")
		self.totalInstances = numpy.concatenate((self.trainingInstances, self.testingInstances), axis=0)		
		self.totalLabels = numpy.array(self.labelsTraining+self.labelsTesting)
		
		print (self.totalInstances.shape)
		print (self.totalLabels.shape)
		print ("\nTraining Instances number::: " + str(len(self.trainingInstances)))
		print ("Testing Instances number::::" + str(len(self.testingInstances)))

		classifier = self.generateSVMClassifier()
		self.evaluateClassifier()
		
	def classifyAndEvaluate(self, totalData):
		self.stopwords = nltk.corpus.stopwords.words('portuguese')
		self.crossValidation(totalData, 10)
		
	"""
	computes term frequency: which is the number of times a word appears in a document, 
	normalized by dividing by the total number of words in the document
	"""
	def tf(self, word, sentenceTokens):
		#print (word + " [count] " + str(sentenceTokens.count(word)) + "[len] " + str(len(sentenceTokens)) + " = " + str(sentenceTokens.count(word) / len(sentenceTokens)))
		return sentenceTokens.count(word) / len(sentenceTokens)

	"""
	returns the number of documents containing word
	"""
	def n_containing(self, sentenceList):
		return sum(1 for sentence in sentenceList if sentence > 0)

	"""
	computes inverse document frequency: which measures how common a word 
	is among all documents. The more common a word is, the lower its idf.
	We take the ratio of the total number of documents to the number of 
	documents containing word, then take the log of that. 
	Add 1 to the divisor to prevent division by zero.
	"""
	def idf(self, sentenceList):
		return math.log(len(sentenceList) / (1.0  + self.n_containing(sentenceList)))

	"""
	 computes the TF-IDF score
	"""
	def tfidf(self, TF, sentenceList):
		return TF * self.idf(sentenceList)
		
	#~ def tfidf2(self, TF, index):
		#~ sentenceList = self.trainingInstances[:,index]
		#~ return TF * (math.log(len(sentenceList) / (1.0  + sum(1 for sentence in sentenceList if sentence > 0))))

	def tokenization(self, sentence):
		return nltk.word_tokenize(str(sentence))

	def plot_confusion_matrix(self, cm, title='Matriz de Confusão', cmap=plt.cm.Blues):
		target_names = ["ALERT", "NOTIFICATION", "INCIDENT"]
		plt.imshow(cm, interpolation='nearest', cmap=cmap)
		plt.title(title)
		plt.colorbar()
		tick_marks = numpy.arange(len(target_names))
		plt.xticks(tick_marks, target_names, rotation=45)
		plt.yticks(tick_marks, target_names)
		plt.tight_layout()
		plt.ylabel('Label Correto')
		plt.xlabel('Label Predito')
	
	def plotROC(self, y_test, pred):
		false_positive_rate, true_positive_rate, thresholds = roc_curve(y_test, pred)
		roc_auc = auc(false_positive_rate, true_positive_rate)
		
		plt.title('ROC (Receiver Operating Characteristic)')
		plt.plot(false_positive_rate, true_positive_rate, 'b',
		label='AUC = %0.2f'% roc_auc)
		plt.legend(loc='lower right')
		plt.plot([0,1],[0,1],'r--')
		plt.xlim([-0.1,1.2])
		plt.ylim([-0.1,1.2])
		plt.ylabel('True Positive Rate')
		plt.xlabel('False Positive Rate')
		plt.savefig('ROC.png')
	
	def confusionMatrix(self, testingLabels, y_pred):
		# Compute confusion matrix
		cm = confusion_matrix(testingLabels, y_pred)
		numpy.set_printoptions(precision=2)
		print('Matriz de Confusão')
		print(cm)
		plt.figure()
		self.plot_confusion_matrix(cm)
		
		# Normalize the confusion matrix by row (i.e by the number of samples in each class)
		cm_normalized = cm.astype('float') / cm.sum(axis=1)[:, numpy.newaxis]
		plt.savefig('CM_1.png')
		
		print('\nMatriz de Confusão Normalizada')
		print(cm_normalized)
		plt.figure()
		self.plot_confusion_matrix(cm_normalized, title='Matriz de Confusão Normalizada')
		plt.savefig('CM_2.png')
	
	def generateImageConfusionMatrix(self, matrix, name='CM_image.png'):
		plt.figure()
		self.plot_confusion_matrix(numpy.array(matrix))
		plt.savefig(name)
	
	def reporting(self, y_test, pred):
		print("classification report:")
		print(metrics.classification_report(y_test, pred,target_names=["ALERT", "NOTIFICATION", "INCIDENT"]))
	
	def featureSelection(self):
		ch2 = SelectKBest(chi2, k="all")
		
		# keep selected feature names
		X_new = ch2.fit_transform(numpy.array(self.trainingInstances), numpy.array(self.testingInstances))
		print ("New Shape::: " + str(X_new.shape))
		feature_names = [self.vocabulary[i] for i in ch2.get_support(indices=True)]
		print ("Feature names:: " + str(feature_names))
		print (feature_names)
	
	def generateSVMClassifier(self):
		classifier = svm.LinearSVC()
		classifier.fit(self.trainingInstances, self.labelsTraining)
		joblib.dump(classifier, 'svmModel.pkl')
		return classifier
	
	def evaluateClassifier(self):
		#~ classifier = svm.LinearSVC()
		#~ results = cross_val_score(classifier, self.totalInstances, self.totalLabels, cv=10)	
		#~ print('=' * 80)
		#~ print ("10F-CROSS-VALIDATION 1\n")
		#~ print (results.mean())
		#~ print (results.std())
		#~ print (results)
		
		print('=' * 80)
		print ("RESULTADOS \n")
		#classifier = svm.SVC()
		#classifier = svm.NuSVC()
		classifier = svm.LinearSVC()
		predited = cross_validation.cross_val_predict(classifier, self.totalInstances, self.totalLabels, cv=10)	
		print("Results size predicted:: " + str(len(predited)))
		self.confusionMatrix(self.totalLabels, predited)
		print('=' * 80)
		print ("REPORT")
		self.reporting(self.totalLabels, predited)
		print()	
		
		# Normalize the confusion matrix by row (i.e by the number of samples in each class)
		scores = accuracy_score(self.totalLabels, predited, normalize=True)
		print ("Normalized accuracy %s" % str(scores))
			
		scores = accuracy_score(self.totalLabels, predited, normalize=False)
		print ("No Normalized accuracy %s" % str(scores))
		print()
		
		macro = precision_score(self.totalLabels, predited, average='macro')  
		micro = precision_score(self.totalLabels, predited, average='micro')
		print ("Precision Macro %s" %str(macro)) 
		print ("Precision Micro %s" %str(micro)) 
		print ()
		
		macro = recall_score(self.totalLabels, predited, average='macro')  
		micro = recall_score(self.totalLabels, predited, average='micro')
		print ("Recall Macro %s" %str(macro)) 
		print ("Recall Micro %s" %str(micro))
		
		macro = f1_score(self.totalLabels, predited, average='macro')
		micro = f1_score(self.totalLabels, predited, average='micro')
		noneT = f1_score(self.totalLabels, predited, average=None)
		print ("F1 Macro %s" %str(macro)) 
		print ("F1 Micro %s" %str(micro))
		print ("F1  %s" %str(noneT))		
		
	def extractAnnotatedEntities(self, patternLabel, sentenceTokens):
		occurrences = re.findall('(BI*)', patternLabel)
		entities = []
		newPattern = patternLabel
		indices = []

		for indx, occurrence in enumerate(occurrences):
			indexStart = newPattern.find(occurrences[indx])
			indices.append([indexStart, indexStart+len(occurrences[indx])])
			subs= ""
			for i in range(len(occurrences[indx])): subs= subs+'X'
			newPattern = newPattern.replace(occurrences[indx], subs, 1)

		termo = []
		for i,idx in enumerate(indices):
			for position in range(idx[0], idx[1]):
				termo.append(sentenceTokens[position])

			#print (" ".join(termo))
			entities.append(" ".join(termo))
			termo = []
		return entities	


	def avaliacaoResult(self):
			
		print('=' * 80)
		print ("10F-CROSS-VALIDATION RESULT\n")
		print("Results size predicted:: " + str(len(self.totalPredictedLabels)))
		self.confusionMatrix(self.totalTrueLabels, self.totalPredictedLabels)
		print('=' * 80)
		print ("REPORT")
		self.reporting(self.totalTrueLabels, self.totalPredictedLabels)
		print()	
		
		# Normalize the confusion matrix by row (i.e by the number of samples in each class)
		scores = accuracy_score(self.totalTrueLabels, self.totalPredictedLabels, normalize=True)
		print ("Normalized accuracy %s" % str(scores))
			
		scores = accuracy_score(self.totalTrueLabels, self.totalPredictedLabels, normalize=False)
		print ("No Normalized accuracy %s" % str(scores))
		print()
		
		macro = precision_score(self.totalTrueLabels, self.totalPredictedLabels, average='macro')  
		micro = precision_score(self.totalTrueLabels, self.totalPredictedLabels, average='micro')
		print ("Precision Macro %s" %str(macro)) 
		print ("Precision Micro %s" %str(micro)) 
		print ()
		
		macro = recall_score(self.totalTrueLabels, self.totalPredictedLabels, average='macro')  
		micro = recall_score(self.totalTrueLabels, self.totalPredictedLabels, average='micro')
		print ("Recall Macro %s" %str(macro)) 
		print ("Recall Micro %s" %str(micro))
		
		macro = f1_score(self.totalTrueLabels, self.totalPredictedLabels, average='macro')
		micro = f1_score(self.totalTrueLabels, self.totalPredictedLabels, average='micro')
		noneT = f1_score(self.totalTrueLabels, self.totalPredictedLabels, average=None)
		print ("F1 Macro %s" %str(macro)) 
		print ("F1 Micro %s" %str(micro))
		print ("F1  %s" %str(noneT))

	def getTestingInstances(self, foldsSet, nFolds, testingFoldIndex):
		print("FoldTesting:: " + str(len(foldsSet[testingFoldIndex])))
		self.labelsTesting = []
		instances = []
		vocabularySize = len(self.vocabulary)
		
		for row in foldsSet[testingFoldIndex]:
			sentence = row[0]				
			sentenceTokens = self.tokenization(sentence)				
			tokensCount = {}
			self.testingInstances = []
			self.labelsTesting.append(row[1])
			
			for token in sentenceTokens: 		
				#Removendo stop-words
				#if ((token not in self.stopwords)and (len(token) > 1) and (not token.isnumeric())):
				
				#token = self.nlp.getStem(token)
				
				#Vocabulário
				if (token in self.vocabulary): 
					indexVocabulary = self.vocabulary.index(token)										
									
					#Contagem
					tokensCount[indexVocabulary] = self.tf(token, sentenceTokens)
		
			#Matrix
			instance = [0]*vocabularySize
			for tupla in tokensCount.items(): instance[tupla[0]] = tupla[1]
			instances.append(instance)
	
		self.testingInstances = numpy.array(instances)
	
		
		#Instances
		for i,line in enumerate(self.testingInstances): 	
			#Terms
			for j,column in enumerate(line):
				#print ("(" + self.vocabulary[j] + ") [IF] " + str(column) + " [column] " + str(j))
				#print ("(" + self.vocabulary[j] + ") [IF] " + str(column) + " [IDF] " + str(self.idf(self.testingInstances[:,j])) + " = " + str(self.tfidf(column, self.testingInstances[:,j])))				
				self.testingInstances[i][j] = self.tfidf(column, self.testingInstances[:,j])

	def getTrainingInstances(self, foldsSet, nFolds, exceptFoldIndex):
		vocabularyFreq = []
		matrix = []
		instances = []	
		self.trainingInstances = []
		self.labelsTraining = []
		
		for index,fold in enumerate(foldsSet):
			if (index != exceptFoldIndex):
				for row in fold:
					sentence = row[0]				
					sentenceTokens = self.tokenization(sentence)				
					tokensCount = {}
					self.labelsTraining.append(row[1])
					
					for token in sentenceTokens: 	
						#Removendo stop-words
						if ((token not in self.stopwords)and (len(token) > 1) and (not token.isnumeric())):
							
							#token = self.nlp.getStem(token)
						
							#Vocabulário
							if (token not in self.vocabulary): self.vocabulary.append(token)	
							indexVocabulary = self.vocabulary.index(token)					
											
							#Contagem
							tokensCount[indexVocabulary] = self.tf(token, sentenceTokens)

					#Matrix
					matrix.append(tokensCount)
							
		vocabularySize = len(self.vocabulary)
		print ('=' *80)
		print ("Vocabulary: %s terms" % str(vocabularySize))
		print (self.vocabulary)
		print ()
	
		#Matrix info
		#~ count = 0
		#~ totalArrayValues = numpy.array([])
		#~ for line in matrix:
			#~ valuesItens = list(line.values())	
			#~ totalArrayValues = numpy.concatenate((totalArrayValues, numpy.array(valuesItens)))
	#~ 
		#print("Num > 0.1:: " + str(count))
		#~ print("Total Array:: " + str(len(totalArrayValues)))
		#~ print("MIn:: " + str(numpy.amin(totalArrayValues)))
		#~ print("Max:: " + str(numpy.amax(totalArrayValues)))
		#~ print("AVG:: " + str(numpy.mean(totalArrayValues)))
		#~ print("STD:: " + str(numpy.std(totalArrayValues)))

		
		#Generating Matrix of terms
		for line in matrix:
			instance = [0]*vocabularySize
			for tupla in line.items(): 
				instance[tupla[0]] = tupla[1]
				#print ("Tupla:: " + str(tupla))
				#print ("Instancia:: " + str(instance[tupla[0]]))
			instances.append(instance)
		
		self.trainingInstances = numpy.array(instances)
		
		print("Calculatong TF-IDF... ")
		
		#Instances
		print ("\n\Instances:: "+str(len(self.trainingInstances))) 
		for i,line in enumerate(self.trainingInstances):
			print (str(i)+" Linha:: "+str(len(line))) 	
			#Terms
			for j,column in enumerate(line):
				#print ("(" + self.vocabulary[j] + ") [IF] " + str(column) + " [IDF] " + str(self.idf(self.trainingInstances[:,j])) + " = " + str(self.tfidf(column, self.trainingInstances[:,j])))
				#print(self.trainingInstances[:,j])
				
				self.trainingInstances[i][j] = self.tfidf(column, self.trainingInstances[:,j])
				#self.trainingInstances[i][j] = self.tfidf2(column, j)
				#print ("Coluna:: ") 
			#fout.writerow(self.trainingInstances[i,:])	
		print("FIM Training:: ")	
		

	def initiateModules(self):
		self.vocabulary = []
				
	def crossValidation(self, totalData, nFolds):
		foldsSize 	= self.totalNumInstances//nFolds
		lastFold 	= 0
		foldNum 	= 0
		foldsSet 	= []
		#foldsSet 	= numpy.array([])	
		fold 		= []
		#fold 		= numpy.array([])
		self.totalPredictedLabels = []
		self.totalTrueLabels = []
		
		#Dividindo as instancias em folds
		with open(totalData, 'r', encoding='utf-8') as csvfile:
			spamreader = csv.reader(csvfile, delimiter=',', quotechar='|')
			for i,row in enumerate(spamreader):
				if (foldNum != nFolds and (lastFold == 0 or lastFold >= foldsSize)):
					if (len(fold) > 0):	
						foldsSet.append(fold)
						#foldsSet = numpy.append(foldsSet, numpy.array([fold]))
						#print(foldsSet)
					#fold = numpy.array([])
					fold = []
					lastFold = 0
					foldNum += 1
					
				sentence = row[1]				
				sentenceTokens = self.tokenization(sentence)
				
				#Removing entities
				entities = self.extractAnnotatedEntities(row[0], sentenceTokens)
				for entity in entities: sentence = sentence.replace(str(entity), '')
											
				fold.append([str(sentence.lower()), row[2]])
				#fold = numpy.append(fold, numpy.array([[str(sentence.lower()), row[2]]]))
				lastFold += 1
				#print(fold)
				
			csvfile.close()		
		#Last fold
		foldsSet.append(fold)
		#foldsSet = numpy.append(foldsSet, numpy.array([fold]))
		#fold = numpy.array([])
		fold = []
		
		print ("\n\n> NÚMERO DE FOLDS:: " + str(len(foldsSet)))
		for index,item in enumerate(foldsSet): print (" %d Fold: %d instâncias" % (index, len(item)))	
		print()
					
		#CrossValidation tests for each fold
		for foldIndex in range(0, nFolds): 
			print("="*80)
			print("FOLD "+str(foldIndex))
			print("="*80)
			
			#Reset structure to its initial state	
			self.initiateModules()
			classifier = svm.LinearSVC()
			
			#Training with nFolds-1 folds except foldIndex (extractong vocabulary)
			self.getTrainingInstances(foldsSet, nFolds, foldIndex)
					
			#Testing with one of the folds			
			self.getTestingInstances(foldsSet, nFolds, foldIndex)
			
			print("Training Model...\n")
			
			#Training model
			classifier.fit(self.trainingInstances, self.labelsTraining)
				
			#predictedLabels = classifier.predict(self.testingInstances, self.labelsTesting)
			predictedLabels = classifier.predict(self.testingInstances)
			#print (predictedLabels)
			self.totalPredictedLabels = self.totalPredictedLabels + list(predictedLabels)
			self.totalTrueLabels = self.totalTrueLabels + self.labelsTesting
			print ("Predicted labels:: " + str(len(self.totalPredictedLabels)))
			print ("Testing labels:: " + str(len(self.labelsTesting)))
		self.avaliacaoResult()
	
	def readTrainingFile0(self, fileNameTrainning):
		vocabularyFreq = []
		matrix = []
		instances = []		
		#self.vocabulary = ['acessando', 'lento', 'bom', 'trechos', 'maior', 'volume', 'boa', 'fluidez', 'defeito', 'liberada', 'fluindo', 'risco',  'raios', 'ventos', 'problema', 'mecânico', 'reboque', 'retenção', 'acidente', 'prejudica', 'queda', 'árvore', 'bloqueia', 'intenso', 'intensos', 'fluxo', 'manifestação', 'manifestantes', 'deslocam', 'faixas', 'ocorrência', 'reflexos', 'lentidão', 'caída', 'obstruída', 'colisões', 'viatura', 'causa', 'bem', 'refletindo', 'parados', 'faixa', 'operação', 'devido', 'obra', 'alerta', 'rajadas', 'vento', 'invadiram', 'impedido', 'desviado', 'liberação', 'retido', 'retida', 'atravessando', 'ocupam', 'atípico', 'carreta', 'concentrações', 'ocupa', 'vítimas', 'interditada', 'evento', 'caminhão', 'ocupando', 'fecharam', 'cruza', 'alteração', 'circulação', 'obras', 'recapeamento', 'nivelamento', 'interdição', 'liberadas', 'fechadas', 'liberado', 'desvio']	
		
		with open(fileNameTrainning, 'r') as csvfile:
			spamreader = csv.reader(csvfile, delimiter=',', quotechar='|')
			
			for row in spamreader:
				#print (row[1])
				sentence = row[1]				
				sentenceTokens = self.tokenization(sentence)
				
				#Removing entities
				entities = self.extractAnnotatedEntities(row[0], sentenceTokens)
				for entity in entities: sentence = sentence.replace(str(entity), '')
				sentenceTokens = self.tokenization(sentence.lower())
				
				tokensCount = {}
				self.labelsTraining.append(str(row[2]))
				
				for token in sentenceTokens: 
					#~ indexVocabulary = 0	
					#~ token = self.nlp.getStem(token)
					#~ if (token in self.vocabulary): 
						#~ indexVocabulary = self.vocabulary.index(token)
						#~ tokensCount[indexVocabulary] = self.tf(token, sentenceTokens)
						
					#Removendo stop-words
					if ((token not in self.stopwords)and (len(token) > 1) and (not token.isnumeric())):
						
						#token = self.nlp.getStem(token)
					
						#Vocabulário
						if (token not in self.vocabulary): self.vocabulary.append(token)	
						indexVocabulary = self.vocabulary.index(token)					
										
						#Contagem
						tokensCount[indexVocabulary] = self.tf(token, sentenceTokens)

				#Matrix
				matrix.append(tokensCount)
							
		vocabularySize = len(self.vocabulary)
		print ('=' *80)
		print ("Vocabulary: %s terms" % str(vocabularySize))
		print (self.vocabulary)
		print ()
	
		#Matrix info
		count = 0
		totalArrayValues = numpy.array([])
		for line in matrix:
			valuesItens = list(line.values())	
			totalArrayValues = numpy.concatenate((totalArrayValues, numpy.array(valuesItens)))
			#~ for item in valuesItens: 
				#~ if (item > 0.1):
					#~ count = count + 1
		#~ 
		#~ print("Num > 0.1:: " + str(count))
		print("Total Array:: " + str(len(totalArrayValues)))
		print("MIn:: " + str(numpy.amin(totalArrayValues)))
		print("Max:: " + str(numpy.amax(totalArrayValues)))
		print("AVG:: " + str(numpy.mean(totalArrayValues)))
		print("STD:: " + str(numpy.std(totalArrayValues)))

		
		#Generating Matrix of terms
		for line in matrix:
			instance = [0]*vocabularySize
			for tupla in line.items(): instance[tupla[0]] = tupla[1]
			instances.append(instance)
		
		self.trainingInstances = numpy.array(instances)
		
		#fout = csv.writer(open("svm_dataset.csv", 'w', newline=''))		
		# Write CSV Headers
		#fout.writerow(vocabulary)
		
		#Instances
		for i,line in enumerate(self.trainingInstances): 	
			#Terms
			for j,column in enumerate(line):
				#print ("(" + self.vocabulary[j] + ") [IF] " + str(column) + " [IDF] " + str(self.idf(self.trainingInstances[:,j])) + " = " + str(self.tfidf(column, self.trainingInstances[:,j])))
				self.trainingInstances[i][j] = self.tfidf(column, self.trainingInstances[:,j])
			#fout.writerow(self.trainingInstances[i,:])	
	
	def readTestingFile(self, fileNameTesting):
		matrix = []
		instances = []
		vocabularySize = len(self.vocabulary)
		
		with open(fileNameTesting, 'r') as csvfile:
			spamreader = csv.reader(csvfile, delimiter=',', quotechar='|')
			
			for row in spamreader:
				sentence = row[1]
				sentenceTokens = self.tokenization(sentence)
				tokensCount = {}
				self.labelsTesting.append(str(row[2]))				
				
				for token in sentenceTokens: 		
					#Removendo stop-words
					#if ((token not in self.stopwords)and (len(token) > 1) and (not token.isnumeric())):
					
					#token = self.nlp.getStem(token)
					
					#Vocabulário
					if (token in self.vocabulary): 
						indexVocabulary = self.vocabulary.index(token)										
										
						#Contagem
						tokensCount[indexVocabulary] = self.tf(token, sentenceTokens)
			
				#Matrix
				instance = [0]*vocabularySize
				for tupla in tokensCount.items(): instance[tupla[0]] = tupla[1]
				instances.append(instance)
	
		self.testingInstances = numpy.array(instances)
		
		#fout = csv.writer(open("svm_dataset.csv", 'w', newline=''))		
		# Write CSV Headers
		#fout.writerow(vocabulary)
		
		#Instances
		for i,line in enumerate(self.testingInstances): 	
			#Terms
			for j,column in enumerate(line):
				#print ("(" + self.vocabulary[j] + ") [IF] " + str(column) + " [column] " + str(j))
				#print ("(" + self.vocabulary[j] + ") [IF] " + str(column) + " [IDF] " + str(self.idf(self.testingInstances[:,j])) + " = " + str(self.tfidf(column, self.testingInstances[:,j])))
				
				self.testingInstances[i][j] = self.tfidf(column, self.testingInstances[:,j])
			#fout.writerow(self.testingInstances[i,:])	
			
			

	#~ def svmClassifier2(self, trainingData, trainingLabels, testingData, testingLabels):
		#~ clf = OneVsRestClassifier(svm.SVC(kernel='linear', probability=True))
		#~ results = cross_val_score(clf, trainingData, trainingLabels, cv=10)	
		#~ print (results.mean())
		#~ print (results)
		#~ 
		#~ clf = OneVsRestClassifier(svm.SVC(kernel='linear', probability=True))
		#~ y_pred = clf.fit(trainingData, trainingLabels).predict(testingData)
		#~ self.confusionMatrix(testingLabels, y_pred)
#~ 
		#~ scores = accuracy_score(testingLabels, y_pred)
		#~ print ("Normalized")
		#~ print (scores)
		#~ scores = accuracy_score(testingLabels, y_pred, normalize=False)
		#~ print ("No Normalized")
		#~ print (scores)
		#~ 
	#~ def svmClassifierROC1(self, trainingData, trainingLabels, testingData, testingLabels):
		#~ # Import some data to play with
		#~ X = numpy.array(trainingData)
		#~ y = numpy.array(trainingLabels)
		#~ 
		#~ # Binarize the output
		#~ y = label_binarize(y, classes=["ALERT", "NOTIFICATION", "INCIDENT"])
		#~ n_classes = 3
	#~ 
		#~ # shuffle and split training and test sets
		#~ X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=.1)
	#~ 
		#~ # Learn to predict each class against the other
		#~ #classifier = OneVsRestClassifier(svm.SVC(kernel='linear', probability=True))
		#~ classifier = OneVsRestClassifier(svm.LinearSVC())
		#~ #classifier = svm.LinearSVC()
		#~ y_score = classifier.fit(X_train, y_train).decision_function(X_test)
#~ 
		#~ # Compute ROC curve and ROC area for each class
		#~ fpr = dict()
		#~ tpr = dict()
		#~ roc_auc = dict()
		#~ for i in range(n_classes):
			#~ fpr[i], tpr[i], _ = roc_curve(y_test[:, i], y_score[:, i])
			#~ roc_auc[i] = auc(fpr[i], tpr[i])
#~ 
		#~ # Compute micro-average ROC curve and ROC area
		#~ fpr["micro"], tpr["micro"], _ = roc_curve(y_test.ravel(), y_score.ravel())
		#~ roc_auc["micro"] = auc(fpr["micro"], tpr["micro"])
#~ 
		#~ ##############################################################################
		#~ # Plot ROC curves for the multiclass problem
#~ 
		#~ # Compute macro-average ROC curve and ROC area
#~ 
		#~ # First aggregate all false positive rates
		#~ all_fpr = numpy.unique(numpy.concatenate([fpr[i] for i in range(n_classes)]))
#~ 
		#~ # Then interpolate all ROC curves at this points
		#~ mean_tpr = numpy.zeros_like(all_fpr)
		#~ for i in range(n_classes):
			#~ mean_tpr += interp(all_fpr, fpr[i], tpr[i])
#~ 
		#~ # Finally average it and compute AUC
		#~ mean_tpr /= n_classes
#~ 
		#~ fpr["macro"] = all_fpr
		#~ tpr["macro"] = mean_tpr
		#~ roc_auc["macro"] = auc(fpr["macro"], tpr["macro"])
#~ 
		#~ # Plot all ROC curves
		#~ plt.figure()
		#~ plt.plot(fpr["micro"], tpr["micro"],
				 #~ label='micro-average ROC curve (area = {0:0.2f})'
					   #~ ''.format(roc_auc["micro"]),
				 #~ linewidth=2)
#~ 
		#~ plt.plot(fpr["macro"], tpr["macro"],
				 #~ label='macro-average ROC curve (area = {0:0.2f})'
					   #~ ''.format(roc_auc["macro"]),
				 #~ linewidth=2)
#~ 
		#~ for i in range(n_classes):
			#~ plt.plot(fpr[i], tpr[i], label='ROC curve of class {0} (area = {1:0.2f})'
										   #~ ''.format(i, roc_auc[i]))
#~ 
		#~ #plt.plot([0, 1], [0, 1], 'k--')
		#~ plt.plot([0, 1], [0, 1], 'k--', color=(0.6, 0.6, 0.6), label='Random guess')
		#~ plt.xlim([0.0, 1.0])
		#~ plt.ylim([0.0, 1.05])
		#~ plt.xlabel('False Positive Rate')
		#~ plt.ylabel('True Positive Rate')
		#~ plt.title('Some extension of Receiver operating characteristic to multi-class')
		#~ plt.legend(loc="lower right")
		#~ plt.savefig('ROC_0.png')
#~ 
	#~ def svmClassifierROC(self, trainingData, trainingLabels, testingData, testingLabels):
		#~ ###############################################################################
		#~ # Data IO and generation
#~ 
		#~ #print (testingData)
		#~ #print("--------")
		#~ #print (testingLabels)
		#~ #print("--------\n")
		#~ # import some data to play with
		#~ #iris = datasets.load_iris()
		#~ X = numpy.array(testingData)
		#~ y = numpy.array(testingLabels)
		#~ y = label_binarize(y, classes=["ALERT", "NOTIFICATION", "INCIDENT"])
		#~ n_classes = 3
		#~ print (X.shape)
		#~ print (y.shape)		
		#~ #n_samples, n_features = X.shape
		#~ 
#~ 
		#~ # Add noisy features
		#~ #random_state = numpy.random.RandomState(0)
		#~ #X = numpy.c_[X, random_state.randn(n_samples, 200 * n_features)]
#~ 
		#~ ###############################################################################
		#~ # Classification and ROC analysis
#~ 
		#~ # Run classifier with cross-validation and plot ROC curves
		#~ cv = StratifiedKFold(y, n_folds=10)
		#~ #classifier = svm.LinearSVC()
		#~ classifier = svm.SVC(kernel='linear', probability=True,random_state=0)
		#~ mean_tpr = 0.0
		#~ mean_fpr = numpy.linspace(0, 1, 100)
		#~ all_tpr = []
#~ 
		#~ for i, (train, test) in enumerate(cv):
			#~ probas_ = classifier.fit(X[train], y[train]).predict_proba(X[test])
			#~ # Compute ROC curve and area the curve
			#~ fpr, tpr, thresholds = roc_curve(y[test], probas_[:, 1])
			#~ mean_tpr += interp(mean_fpr, fpr, tpr)
			#~ mean_tpr[0] = 0.0
			#~ roc_auc = auc(fpr, tpr)
			#~ plt.plot(fpr, tpr, lw=1, label='ROC fold %d (area = %0.2f)' % (i, roc_auc))
#~ 
		#~ mean_tpr /= len(cv)
		#~ mean_tpr[-1] = 1.0
		#~ mean_auc = auc(mean_fpr, mean_tpr)
			#~ 
		#~ plt.plot([0, 1], [0, 1], '--', color=(0.6, 0.6, 0.6), label='Luck')
		#~ plt.plot(mean_fpr, mean_tpr, 'k--', label='Mean ROC (area = %0.2f)' % mean_auc, lw=2)
#~ 
		#~ plt.xlim([-0.05, 1.05])
		#~ plt.ylim([-0.05, 1.05])
		#~ plt.xlabel('False Positive Rate')
		#~ plt.ylabel('True Positive Rate')
		#~ plt.title('Example')
		#~ plt.legend(loc="lower right")
		#~ plt.show()
	#~ 
	#~ def adaBoost(self, trainingData, trainingLabels, testingData, testingLabels):
		#~ #iris = load_iris()
		#~ clf = AdaBoostClassifier(n_estimators=300)
		#~ #clf.fit(trainingData, trainingLabels)
		#~ #scores = cross_val_score(clf, testingData, testingLabels)
		#~ scores = cross_val_score(clf, trainingData, trainingLabels, cv=10)
		#~ print ("AdaBoost")
		#~ print (scores.mean())
		#~ print (scores)
	#~ 
	#~ def bagging(self, trainingData, trainingLabels, testingData, testingLabels):	
		#~ bagging = BaggingClassifier(KNeighborsClassifier(), max_samples=0.5, max_features=0.5)
		#~ #bagging.fit(trainingData, trainingLabels)
		#~ #results = cross_val_score(bagging, testingData, testingLabels)
		#~ results = cross_val_score(bagging, trainingData, trainingLabels, cv=10)
		#~ print ("Bagging")
		#~ print (results.mean())
		#~ print ("Mean Accuracy: " + str(results))
		#~ 
	#~ def randomTrees(self, trainingData, trainingLabels, testingData, testingLabels):
		#~ clf = ExtraTreesClassifier(n_estimators=10, max_depth=None, min_samples_split=1, random_state=0)
		#~ #clf.fit(trainingData, trainingLabels)
		#~ #scores = cross_val_score(clf, testingData, testingLabels)
		#~ scores = cross_val_score(clf, trainingData, trainingLabels, cv=10)
		#~ print ("Random Trees")
		#~ print (scores.mean())
		#~ print (scores)
	#~ 
	#~ def gradient(self, trainingData, trainingLabels, testingData, testingLabels):
		#~ clf = GradientBoostingClassifier(n_estimators=100, learning_rate=1.0, max_depth=1, random_state=0)
		#~ #clf.fit(trainingData, trainingLabels)		
		#~ #scores = cross_val_score(clf, testingData, testingLabels)
		#~ scores = cross_val_score(clf, trainingData, trainingLabels, cv=10)
		#~ print ("Gradient")
		#~ print (scores.mean())
		#~ print (scores)
		#~ 
		#~ y_pred = clf.fit(trainingData, trainingLabels).predict(testingData)
		#~ self.confusionMatrix(testingLabels, y_pred)
#~ 
		#~ scores = accuracy_score(testingLabels, y_pred)
		#~ print ("Normalized")
		#~ print (scores)
		#~ scores = accuracy_score(testingLabels, y_pred, normalize=False)
		#~ print ("No Normalized")
		#~ print (scores)
			#~ 
	#~ def votingClassifier(self, trainingData, trainingLabels, testingData, testingLabels):		
		#~ #iris = datasets.load_iris()
		#~ #X, y = iris.data[:, 1:3], iris.target
		#~ 
		#~ clf1 = LogisticRegression(random_state=1)
		#~ clf2 = RandomForestClassifier(random_state=1)
		#~ clf3 = GaussianNB()
		#~ 
		#~ eclf = VotingClassifier(estimators=[('lr', clf1), ('rf', clf2), ('gnb', clf3)], voting='hard')
		#~ print ("VotingClassifier")
		#~ for clf, label in zip([clf1, clf2, clf3, eclf], ['Logistic Regression', 'Random Forest', 'naive Bayes', 'Ensemble']):
			#~ #scores = cross_validation.cross_val_score(clf, testingData, testingLabels, cv=5, scoring='accuracy')
			#~ scores = cross_validation.cross_val_score(clf, trainingData, trainingLabels, cv=10, scoring='accuracy')
			#~ print("Accuracy: %0.2f (+/- %0.2f) [%s]" % (scores.mean(), scores.std(), label))
	
	#~ def testClassifier(self, classifier):
		#~ print('=' * 80)
		#~ print ("REPORT")
					#~ 
		#~ y_pred = classifier.predict(self.testingInstances)
		#~ #predictFunct = classifier.decision_function(self.testingInstances)
		#~ #print("Decision Function")
		#~ #print(predictFunct)
		#~ #print()
		#~ 
		#~ self.reporting(self.labelsTesting, y_pred)
		#~ print()
		#~ 
		#~ self.confusionMatrix(self.labelsTesting, y_pred)
		#~ print()
		#~ 
		#~ # Normalize the confusion matrix by row (i.e by the number of samples in each class)
		#~ scores = accuracy_score(self.labelsTesting, y_pred, normalize=True)
		#~ print ("Normalized accuracy %s" % str(scores))
			#~ 
		#~ scores = accuracy_score(self.labelsTesting, y_pred, normalize=False)
		#~ print ("No Normalized accuracy %s" % str(scores))
		#~ print()
		#~ 
		#~ macro = precision_score(self.labelsTesting, y_pred, average='macro')  
		#~ micro = precision_score(self.labelsTesting, y_pred, average='micro')
		#~ print ("Precision Macro %s" %str(macro)) 
		#~ print ("Precision Micro %s" %str(micro)) 
		#~ print ()
		#~ 
		#~ macro = recall_score(self.labelsTesting, y_pred, average='macro')  
		#~ micro = recall_score(self.labelsTesting, y_pred, average='micro')
		#~ print ("Recall Macro %s" %str(macro)) 
		#~ print ("Recall Micro %s" %str(micro))
		#~ 
		#~ macro = f1_score(self.labelsTesting, y_pred, average='macro')
		#~ micro = f1_score(self.labelsTesting, y_pred, average='micro')
		#~ noneT = f1_score(self.labelsTesting, y_pred, average=None)
		#~ print ("F1 Macro %s" %str(macro)) 
		#~ print ("F1 Micro %s" %str(micro))
		#~ print ("F1  %s" %str(noneT))
								
								
	#~ def svmClassifier(self, trainingData, trainingLabels, testingData, testingLabels):
		#~ #clf = svm.SVC()
		#~ clf = svm.LinearSVC()
		#~ 
		#~ #clf = svm.SVC(C=1.0, cache_size=200, class_weight=None, coef0=0.0, decision_function_shape=None, degree=3, gamma='auto', kernel='rbf', max_iter=-1, probability=False, random_state=None, shrinking=True, tol=0.001, verbose=False)
		#~ #clf.fit(trainingData, trainingLabels)
		#~ joblib.dump(clf, 'svmModel.pkl')
		#~ #clf = joblib.load('svmModel.pkl') 		
		#~ print ("SVM")
		#~ #results = cross_val_score(clf, testingData, testingLabels)		
		#~ results = cross_val_score(clf, trainingData, trainingLabels, cv=10)	
		#~ print (results.mean())
		#~ print (results)
		#~ #print (testingData[3,:])
		#~ #for i in range(len(testingData)):
			#~ #print (str(clf.predict([testingData[i,:]])) +" vs [correct] " + str(testingLabels[i]))
		#~ 
		#~ clf = svm.LinearSVC()
		#~ y_pred = clf.fit(trainingData, trainingLabels).predict(testingData)
		#~ self.confusionMatrix(testingLabels, y_pred)
#~ 
		#~ scores = accuracy_score(testingLabels, y_pred)
		#~ print ("Normalized")
		#~ print (scores)
		#~ scores = accuracy_score(testingLabels, y_pred, normalize=False)
		#~ print ("No Normalized")
		#~ print (scores)
							